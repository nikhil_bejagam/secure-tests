package com.utils;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.Status;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.utils.ConfigFileReader;

public class DriverInstance {
	TestLogger logger = new TestLogger();
	public static WebDriver driver;
	public Connection conn;
	DesiredCapabilities capabilities;
	ConfigFileReader configFileReader = new ConfigFileReader();
	public String environment = configFileReader.getEnvironment();
	public String browserType = configFileReader.getBrowserType();
	public static String path = System.getProperty("user.dir");
	String baseURL;

	static {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		System.setProperty("current.date.time", path + "/Logs/PS_" + dateFormat.format(new Date()));
	}

	/****
	 * Method: launchURL method loads and reads from configuration property file
	 * Parameters: No Author: Dhivya Gunasekaran
	 ****/
	public WebDriver launchBrowser() {
		logger.info("DriverInstance - launchURL :BrowserType is: " + browserType);
		setCapabilities();
		browserDriver();
		logger.info("DriverInstance - launchURL : Driver object from method LaunchURL is: " + driver);
		try {
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			// driver.navigate().to(configFileReader.getApplicationUrl());
		}

		catch (Exception e) {
			logger.fail("DriverInstance - launchURL : error occured--" + e.getMessage() + e.getCause());
		}
		return driver;
	}

	public String launchURL() {
		try {
			if (environment.equalsIgnoreCase("Staging")) {
				baseURL = configFileReader.getStagingUrl();
				driver.navigate().to(baseURL);

			} else if (environment.equalsIgnoreCase("Beta")) {
				baseURL = configFileReader.getBetaUrl();
				driver.navigate().to(baseURL);
			} else if (environment.equalsIgnoreCase("Gamma")) {
				baseURL = configFileReader.getOnboardingGammaUrl();
				driver.navigate().to(baseURL);
			}else if (environment.equalsIgnoreCase("OnboardingGamma")) {
				baseURL = configFileReader.getOnboardingGammaUrl();
				driver.navigate().to(baseURL);
			}else if (environment.equalsIgnoreCase("cnbeta")) {
					baseURL = configFileReader.getchinaBetaUrl();
					driver.navigate().to(baseURL);
			}else {
				baseURL = configFileReader.getStagingUrl();
				driver.navigate().to(baseURL);

			}
		} catch (Exception e) {
			logger.fail("launchURL : error occured--" + e.getMessage() + e.getCause());
		}
		return baseURL;
	}

	/****
	 * Method: launchURL method to get the browser type from xml or json and set
	 * the driver Parameters: No Author: Dhivya Gunasekaran
	 ****/
	public void browserDriver() {
		switch (browserType) {
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", configFileReader.getGeckoWindows());
			driver = new FirefoxDriver();
			break;

		case "InternetExplorer":
			System.setProperty("webdriver.ie.driver", configFileReader.getIEWindows());
			driver = new InternetExplorerDriver();
			break;

		case "Chrome":
			String osName = System.getProperty("os.name");
			String chromeDriverStr = "webdriver.chrome.driver";
			logger.info("Operating System > " + osName);

			if (osName.contains("Linux")) {
				System.setProperty(chromeDriverStr, configFileReader.getChromeLinux());
				logger.info("Operating System inside Linux Switch Case> " + configFileReader.getChromeLinux());
			} else if (osName.contains("Mac OS X"))
				System.setProperty(chromeDriverStr, configFileReader.getChromeMac());
			else
				System.setProperty(chromeDriverStr, configFileReader.getChromeWindows());
			ChromeOptions options = new ChromeOptions();
			// driver = new ChromeDriver();
			// case "Headless":
			// System.setProperty("webdriver.chrome.driver",
			// configFileReader.getChromeDriverPath());
			setCapabilities();
			String headless = configFileReader.getHeadless();
			if (headless != null && Boolean.parseBoolean(headless)) {
				options.addArguments("--headless");
				options.addArguments("--disable-gpu");
				// System.setProperty("webdriver.chrome.driver",
				// configFileReader.getChromeDriverPath());
			}
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(options);
			driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitWait(), TimeUnit.SECONDS);
			break;
		default:
			System.setProperty("webdriver.gecko.driver", configFileReader.getGeckoWindows());
			driver = new FirefoxDriver();
			break;

		}
		logger.info("DriverInstance - browserDriver : browser type is " + browserType + "driver is " + driver);

	}

	/****
	 * Method: launchURL method to set capabilities Parameters: NO Author:
	 * Dhivya Gunasekaran
	 ****/
	public void setCapabilities() {
		switch (browserType) {

		case "Firefox":
			capabilities = DesiredCapabilities.firefox();
			break;

		case "InternetExplorer":
			capabilities = DesiredCapabilities.internetExplorer();
			break;

		case "Chrome":
			capabilities = DesiredCapabilities.chrome();
			break;

		default:
			capabilities = DesiredCapabilities.firefox();
			break;

		}

	}
	public Connection JDBCconnection() {
		try {
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setUser("avi.shah");
		ExtentReportGeneration.test.log(Status.INFO , "Set user" );
		dataSource.setPassword("1wfD1dd2rtCas38c");
		ExtentReportGeneration.test.log(Status.INFO , "Set password" );
		dataSource.setServerName("staging-secure-api.cbwleyov3k2q.us-east-1.rds.amazonaws.com");
		ExtentReportGeneration.test.log(Status.INFO , "Set servername" );
		if (environment.equalsIgnoreCase("cnbeta"))
		dataSource.setDatabaseName("beta_secure_api");
		else
		dataSource.setDatabaseName(environment.toLowerCase()+"_secure_api");
		conn = dataSource.getConnection();
		ExtentReportGeneration.test.log(Status.INFO , "Set connection" );
		}
		catch (Exception e) {
			logger.fail("launchURL : error occured--" + e.getMessage() + e.getCause());
			ExtentReportGeneration.test.fail("Unable to make JDBC connection" );
		}
		return conn;
	}

	}

