package com.utils;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.pageDefinitions.AdminPage;
import com.pageDefinitions.CysecPage;
import com.pageDefinitions.HomePage;
import com.pageDefinitions.Payments;
import com.pageDefinitions.RegistrationPage;
import com.pageDefinitions.SVGPage;
import com.pageDefinitions.VerificationPage;
import com.pageDefinitions.Onboarding;


public abstract class TestConfig extends DriverInstance {

	ExtentReportGeneration extreport = new ExtentReportGeneration();
	protected HomePage homePage;
	protected RegistrationPage registrationPage;
	protected VerificationPage verificationPage;
	protected Payments payments;
	protected CysecPage cysecPage;
	protected Onboarding onboarding;
	protected AdminPage admin;
	protected SVGPage svg;
	

    TestLogger log = new TestLogger();
    
    @BeforeSuite(alwaysRun=true)
    public void Report(){
    	extreport.reportGeneration();
    	
    }
    @BeforeClass(alwaysRun=true)
	public void Browser() {
		launchBrowser();
	}

	@BeforeMethod(alwaysRun=true)
	public void Url(Method method) {
		driver.manage().deleteAllCookies();
		homePage = PageFactory.initElements(driver, HomePage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
		verificationPage = PageFactory.initElements(driver, VerificationPage.class);
		payments = PageFactory.initElements(driver, Payments.class);
		cysecPage=PageFactory.initElements(driver, CysecPage.class);
		onboarding = PageFactory.initElements(driver, Onboarding.class);
		admin = PageFactory.initElements(driver, AdminPage.class);
		svg = PageFactory.initElements(driver, SVGPage.class);
	}
	
	@AfterSuite(alwaysRun=true)
	public void closeReport(){
		extreport.tear();
	}
	@AfterClass(alwaysRun=true)
	public void closeBrowser() {
		driver.manage().deleteAllCookies();
		driver.close();
	}

	@AfterMethod(alwaysRun=true)
	public void tearDown(ITestResult result) throws IOException {
		extreport.reportLogger(result);

	}
	public void ExtentReportTestCaseName(Method method, String options, String country) {
		launchURL();
		extreport.report(method, options,country);
	}
	public void ExtentReportTestCaseName(Method method, String options) {
		launchURL();
		extreport.report(method, options);
	}
	public void ExtentReportTestCaseName(Method method) {
		launchURL();
		extreport.report(method);
	}
}
