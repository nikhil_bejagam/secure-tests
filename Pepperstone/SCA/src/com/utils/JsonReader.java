package com.utils;

import java.io.File;
import java.util.Properties;

import com.jayway.jsonpath.*;
import com.utils.ConfigFileReader;



public class JsonReader 
{
	File jsonFile;
	String str;
	String fs=File.separator;
//	 private Properties properties= new Properties();
	static String path = System.getProperty("user.dir");
	
	/**** 
	 * Method: JsonReader method loads JSON file
	 * Parameters: NO
	 * Author: Dhivya Gunashekaran
	****/
	public JsonReader(String filepath) {
		ConfigFileReader configFileReader = new ConfigFileReader();
		String json=path+fs+configFileReader.getInputFiles()+fs+configFileReader.getJsonPath(filepath);
		jsonFile = new File(json);
	}
	
	/**** 
	 * Method: launchURL method reads from JSON file for test data
	 * Parameters: String
	 * Author: Dhivya Gunasekaran
	****/
	public String GetElementValue(String element)
	{
		try
		{		
		str =JsonPath.read(jsonFile, "$."+ element);	
		}
		catch(Exception e)
		{
			System.out.println("Exception is: "+ e);
		}
		return str;
	}
}

