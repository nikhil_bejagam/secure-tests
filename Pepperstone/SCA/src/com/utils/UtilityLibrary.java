package com.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.gargoylesoftware.htmlunit.javascript.host.Set;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
//import java.util.Set;

public class UtilityLibrary extends DriverInstance {
	static ExtentTest test;
	static TestLogger logger = new TestLogger();
	static ConfigFileReader configFileReader = new ConfigFileReader();
	public String environment = configFileReader.getEnvironment();
	static String fileName;
	String baseURl;
	public static WebDriverWait wait;
	static String fs = File.separator;
	ZonedDateTime currentDate;

	/****
	 * Method: pass method logs in pass result Parameters: No Author: Bharathi
	 * Bhogavarapu
	 ****/
	public static void getScreenShot(ITestResult result) {
		try {
			
			File fileSrc = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String methodName = result.getName();
			fileName = configFileReader.getScreenshotPath() + fs + methodName + "_" + timeStamp() + ".png";
			File fileDest = new File(fileName);
			FileUtils.copyFile(fileSrc, fileDest);
			System.out.println("***Placed screen shot in " + fileName + " ***");

			// adds screenshot to the report
			Reporter.log("<a href='" + fileDest.getAbsolutePath() + "'> <img src='" + fileDest.getAbsolutePath()
					+ "' height='100' width='100'/> </a>");
		} catch (IOException e) {
			logger.fail(e.getMessage());
			e.printStackTrace();

		}
	}

	public static String timeStamp() {
		String stamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
		return stamp;
	}

	public void enterKeys(WebElement element, String message) {
		if (element.isEnabled()) {
			element.clear();
			element.sendKeys(message);
		} else {
			logger.fail("element not enabled" + element);
		}

	}
	public boolean isElementPresent(WebElement locatorKey) {
	    try {
	        locatorKey.isDisplayed();
	        return true;
	    } catch (org.openqa.selenium.NoSuchElementException e) {
	        return false;
	    }
	}
	public void validateURL(String expectedURL, String msg) {
		String currentURL = driver.getCurrentUrl();
		if (currentURL.equals(expectedURL)) {
			logger.pass(msg + "Success");
		} else {
			ExtentReportGeneration.test.log(Status.FAIL, "unable to validate URL expectedURL");
			test.fail(msg + "Failed");
			logger.fail(msg + "Failed");
		}

	}

	public void validatePartialURL(String expectedURL, String msg) {
		String currentURL = driver.getCurrentUrl();
		if (currentURL.contains(expectedURL)) {
			logger.pass(msg + "Success");
		} else {
			logger.fail(msg + "Failed");
		}

	}

	public void isDisplayed(WebElement element, String msg) {
		boolean bln = element.isDisplayed();
		if (bln) {
			logger.pass(msg + "Success");
		} else {
			logger.fail(msg + "Failed");
		}
	}

	public void SwitchAndCloseWindow(String partialUrl) {
		int flag = 0;
		String firstWindow = driver.getWindowHandle();
		ExtentReportGeneration.test.log(Status.INFO , "Main window is"+ firstWindow);
		// Pass a window handle to the other window
		for (String handle1 : driver.getWindowHandles()) {
			driver.switchTo().window(handle1);
			ExtentReportGeneration.test.log(Status.INFO , "Switching to child window"+ handle1);
			if (driver.getCurrentUrl().contains(partialUrl)) {
				ExtentReportGeneration.test.log(Status.INFO , "window opened and verified partial url"+partialUrl);
				flag = 1;
				driver.close();
				driver.switchTo().window(firstWindow);
				ExtentReportGeneration.test.log(Status.INFO , "Switched to main window"+ firstWindow);
				break;
			}
		}
		if (flag == 0) {
			logger.fail(" window not displayed");
		}
	}

	public void pageWait() {
		new WebDriverWait(driver, configFileReader.getImplicitWait())
				.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState")
						.equals("complete"));
	}

	public void waitForPageLoaded() {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.fail("Timeout waiting for Page Load Request to complete.");
		}
	}

	public void getDropDownByValue(WebElement element, String value) {
		Select drp = new Select(element);
		drp.selectByValue(value);
	}

	public void getDropDownByIndex(WebElement element, int index) {
		Select drp = new Select(element);
		drp.selectByIndex(index);
	}

	public void getDropDownByVisibleText(WebElement element, String text) {
		Select drp = new Select(element);
		drp.selectByVisibleText(text);
	}

	public String getFirstSelectedOption(WebElement element) {
		Select drp = new Select(element);
		return drp.getFirstSelectedOption().getText();
	}

	public int getRandomNumber(int range) {
		Random rand = new Random();
		return rand.nextInt(range);
	}

	public void scrollIntoView(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void waitForElementToBeClickable(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitForElementPresent(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated((By) element));
	}

	public String getMethodName() {
		String nameofCurrMethod = new Throwable().getStackTrace()[1].getMethodName();
		return nameofCurrMethod;
	}

	public void jsClick(WebElement element) {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (StaleElementReferenceException e) {
			e.printStackTrace();
			jsClick(element);
		} catch (Exception e) {
			e.printStackTrace();
			element.click();
		}
	}

	public static boolean waitForPageWithUrl(String title, int waitTimeInSec) {

		Boolean isOnPage;

		try {
			wait = new WebDriverWait(driver, waitTimeInSec);
			return wait.until(ExpectedConditions.urlContains(title));

		} catch (Exception e) {
			e.printStackTrace();
			isOnPage = false;
			// return false;
		}
		return isOnPage;
	}

	public void tabFromElement(WebElement webElement) {
		webElement.sendKeys(Keys.TAB);
		webElement.sendKeys(Keys.ENTER);
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	public void windowhandle() {
		java.util.Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> itr = allWindowHandles.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}

	public String getRandomAlphaNumericString(int n) {

		// length is bounded by 256 Character
		byte[] array = new byte[256];
		new Random().nextBytes(array);

		String randomString = new String(array, Charset.forName("UTF-8"));

		// Create a StringBuffer to store the result
		StringBuffer r = new StringBuffer();

		// Append first 20 alphanumeric characters
		// from the generated random String into the result
		for (int k = 0; k < randomString.length(); k++) {

			char ch = randomString.charAt(k);

			if (((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) && (n > 0)) {

				r.append(ch);
				n--;
			}
		}

		// return the resultant string
		return r.toString();
	}

	public String environmentURl() {
		try {
			if (environment.equalsIgnoreCase("Staging")) {
				baseURl = configFileReader.getStagingUrl();
			} else if (environment.equalsIgnoreCase("Beta")) {
				baseURl = configFileReader.getBetaUrl();
			} else if (environment.equalsIgnoreCase("cnbeta")) {
				baseURl = configFileReader.getchinaBetaUrl();
			}else if (environment.equalsIgnoreCase("Gamma")) {
				baseURl = configFileReader.getOnboardingGammaUrl();
			}else if (environment.equalsIgnoreCase("OnboardingGamma")) {
				baseURl = configFileReader.getOnboardingGammaUrl();
			}else 
				baseURl = configFileReader.getStagingUrl();
		} catch (Exception e) {
			logger.fail("unable to get base URL for" + environment + "from configuration.properties file");
			test.fail("unable to get base URL for" + environment + "from configuration.properties file");
		}
		return baseURl;
	}

	static DateTimeFormatter globalFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mma z");
	static DateTimeFormatter etFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy 'at' hh:mma 'ET'");

	static ZoneId istZoneId = ZoneId.of("Asia/Kolkata");
	static ZoneId etZoneId = ZoneId.of("America/New_York");

	public ZonedDateTime currentDate() {
		LocalDateTime LocalDate = LocalDateTime.now();
		ZonedDateTime currentISTime = LocalDate.atZone(istZoneId); // India Time
		ZonedDateTime currentETime = currentISTime.withZoneSameInstant(etZoneId); // ET
																					// Time
		return currentETime;
	}

	public ZonedDateTime ageEighteen() {
		ZonedDateTime ageEighteen = currentDate().minusYears(18);
		return ageEighteen;
	}

	public ZonedDateTime agebelowEighteen() {
		ZonedDateTime agebelowEighteen = currentDate().minusYears(17).minusMonths(11).minusDays(29);
		return agebelowEighteen;
	}

	public ZonedDateTime ageaboveEighteen() {
		ZonedDateTime ageaboveEighteen = currentDate().minusYears(18).minusDays(1);
		return ageaboveEighteen;
	}

	public boolean overEighteen(int year, int month, int day) {

		currentDate = currentDate().minusYears(year).minusMonths(month).minusDays(day);
		int yr = currentDate.getYear();
		int mon = currentDate.getMonthValue();
		int days = currentDate.getDayOfMonth();
		boolean flag = false;
		if ((yr > 17) || ((yr == 17) && (mon > 11)) || ((yr == 17) && (mon == 11) && (days > 29))) {
			System.out.println("Age is over Eighteen");
			flag = true;
		} else
			System.out.println("Age is not Eighteen");
		return flag;
	}
	
	public String executeQueryOnDB(String query) {
		String columnValue = null;
		try {
		ExtentReportGeneration.test.log(Status.INFO , "Making JDBC connection");
		Connection conn = JDBCconnection();
		 ExtentReportGeneration.test.log(Status.INFO , "Completed JDBC connection");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		while (rs.next()) {
		    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
		        if (i > 1) System.out.print(",  ");
		         columnValue = rs.getString(i);
		        System.out.print(columnValue + " " + rs.getMetaData().getColumnName(i));
		    }
		}
		    System.out.println("");
		rs.close();
		stmt.close();
		conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return columnValue;
	}
	
	public String getuserIDfromDatabase(String emailId){
		String id=null;
		try{
		String userid = "SELECT id FROM User where email='"+emailId+"'";
		id = executeQueryOnDB(userid);
		ExtentReportGeneration.test.log(Status.INFO , "UserID for email id "+emailId+" is "+id);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	public String getuserlicencefromDB(String emailId){
		String licence=null;
		try{
		String userid = "SELECT licences FROM User where email='"+emailId+"'";
		licence = executeQueryOnDB(userid);
		ExtentReportGeneration.test.log(Status.INFO , "licence for email id "+emailId+" is "+licence);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return licence;
	}
	public WebElement getDynamicbalance(String account) {
		return driver.findElement(By.xpath(
				"//div[contains(text(),'" + account + "')]/../../..//span[contains(text(),'balance')]/../../div[1]"));
	}

	public WebElement getDynamicFundNow(String account) {
		return driver.findElement(By.xpath("//div[contains(text(),'" + account
				+ "')]/ancestor::div[contains(@class,'trading')]//span[contains(text(),'fund now')]/.."));
	}
	
	public WebElement getDynamicAccountNumber(String account) {
		return driver.findElement(By.xpath("//div[contains(text(),'" + account + "')]/ancestor::div[contains(@class,'trading')]//div[contains(@class,'tooltip-content')]/span"));
	}
}
