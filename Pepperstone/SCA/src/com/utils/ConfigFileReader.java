package com.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

public class ConfigFileReader 
{
	 private Properties properties= new Properties();
	 String fs=File.separator;
	 static String path = System.getProperty("user.dir");
	 private final String propertyFilePath= path + fs+"Configuration.properties";
	 public Logger log;
	
	 
	 /**** 
	 * Method: ConfigFileReader method loads and reads from configuration property file
	 * Parameters: NO
	 * Author: Bharathi Bhogavarapu
	 ****/
	 
	 public ConfigFileReader()
	 {		 
		 BufferedReader reader;
		
		 try 
		 {
			 reader = new BufferedReader(new FileReader(propertyFilePath));
			 try 
			 {
				 properties.load(reader);
				 reader.close();
			 } 
			 catch (IOException e) 
			 {
				 e.printStackTrace();
			 }
		 } 
		 catch (FileNotFoundException e)
		 {
			 e.printStackTrace();
			 throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		 } 
	 }
	 
		 
	 public long getImplicitWait() 
	 { 
		 String implicitWait = properties.getProperty("implicitWait");
		 if(implicitWait != null) return Long.parseLong(implicitWait);
		 else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file."); 
	 }
	 public String getHeadless() 
	 {
		 String isHeadless = properties.getProperty("isHeadless");
		 if(isHeadless != null) return isHeadless;
		 else throw new RuntimeException("isHeadless not specified in the Configuration.properties file.");
	 }
	 public String getInputFiles() 
	 {
		 String inputFiles = properties.getProperty("inputFiles");
		 if(inputFiles != null) return inputFiles;
		 else throw new RuntimeException("inputFiles not specified in the Configuration.properties file.");
	 }
	 public String getDrivers() 
	 {
		 String drivers = properties.getProperty("drivers");
		 if(drivers != null) return drivers;
		 else throw new RuntimeException("drivers not specified in the Configuration.properties file.");
	 }
	 public String getJsonPath(String jsonPath) 
	 {
		 String json = properties.getProperty(jsonPath);
		 if(json != null) return json;
		 else throw new RuntimeException("jsonPath not specified in the Configuration.properties file.");
	 }
	 public String getEnvironment() 
	 {
		 String env = properties.getProperty("environment");
		 if(env != null) return env;
		 else throw new RuntimeException("Environment is not specified in the Configuration.properties file.");
	 }
	 public String getStagingUrl() 
	 {
		 String url = properties.getProperty("staging.url");
		 if(url != null) return url;
		 else throw new RuntimeException("Staging url not specified in the Configuration.properties file.");
	 }
	 
	 public String getBetaUrl() 
	 {
		 String url = properties.getProperty("beta.url");
		 if(url != null) return url;
		 else throw new RuntimeException("Beta url not specified in the Configuration.properties file.");
	 }
	 
	 public String getGammaUrl() 
	 {
		 String url = properties.getProperty("gamma.url");
		 if(url != null) return url;
		 else throw new RuntimeException("Gamma url not specified in the Configuration.properties file.");
	 }
	 
	 public String getOnboardingGammaUrl() 
	 {
		 String url = properties.getProperty("onboardingGamma.url");
		 if(url != null) return url;
		 else throw new RuntimeException("OnboardingGamma url not specified in the Configuration.properties file.");
	 }
	 
	 
	 public String getchinaBetaUrl() 
	 {
		 String url = properties.getProperty("cnbeta.url");
		 if(url != null) return url;
		 else throw new RuntimeException("cnbeta url not specified in the Configuration.properties file.");
	 }
	 
	 public String getScreenshotPath() 
	 {
		 String screenshotPath = properties.getProperty("screenshotsPath");
		 if(properties.getProperty("screenshotsPath") != null) return screenshotPath;
		 else throw new RuntimeException("Screenshots path not specified in the Configuration.properties file.");
	 }
	 
	 public String getReportPath() 
	 {
		 String reportPath = properties.getProperty("reportPath");
		 if(properties.getProperty("reportPath") != null) return reportPath;
		 else throw new RuntimeException("ExtentReport path not specified in the Configuration.properties file.");
	 }

	 public String getBrowserType() 
	 {
		 String browserType = properties.getProperty("browserType");
		 if(browserType != null) return browserType;
		 else throw new RuntimeException("browserType not specified in the Configuration.properties file.");
	 }
	 
	 public String getDevice() 
	 {
		 String device = properties.getProperty("device");
		 if(device != null) return device;
		 else throw new RuntimeException("device is not specified in the Configuration.properties file.");
	 }
	 
	 public String getGeckoWindows() 
	 {
		 String geekodriverPath = path +fs+ properties.getProperty("drivers")+fs+ properties.getProperty("windows.gecko ");
		 if(properties.getProperty("windows.gecko ") != null) return geekodriverPath;
		 else throw new RuntimeException("geekodriverPath not specified in the Configuration.properties file.");
	 }
	 public String getIEWindows() 
	 {
		 String iedriverPath = path + fs+ properties.getProperty("drivers")+fs+properties.getProperty("windows.ie");
		 return iedriverPath;
	 }
	 public String getChromeMac() 
	 {
		 String chromedriverPath = path +fs+ properties.getProperty("drivers")+fs+ properties.getProperty("mac.chrome");
		 System.out.println(chromedriverPath);
		 if(properties.getProperty("mac.chrome") != null) return chromedriverPath;
		 else throw new RuntimeException("chromedriverPath not specified in the Configuration.properties file.");
	 }
	 public String getChromeWindows() 
	 {
		 String chromedriverPath = path +fs+ properties.getProperty("drivers")+fs+ properties.getProperty("windows.chrome");
		 System.out.println(chromedriverPath);
		 if(properties.getProperty("windows.chrome") != null) return chromedriverPath;
		 else throw new RuntimeException("chromedriverPath not specified in the Configuration.properties file.");
	 }
	 public String getChromeLinux() 
	 {
		 String chromedriverPath = path + fs+ properties.getProperty("drivers")+fs+properties.getProperty("linux.chrome");
		 System.out.println(chromedriverPath);
		 if(properties.getProperty("linux.chrome") != null) return chromedriverPath;
		 else throw new RuntimeException("chromedriverPath not specified in the Configuration.properties file.");
	 }
	 public String getJsonFilePath(String jsonPath) 
	 {
		 String jsonFilePath = path + properties.getProperty(jsonPath);
		 if(properties.getProperty(jsonPath) != null) return jsonFilePath;
		 else throw new RuntimeException("jsonFilePath not specified in the Configuration.properties file.");
	 }
}
