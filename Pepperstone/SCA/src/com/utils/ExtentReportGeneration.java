package com.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportGeneration {

	public static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;
	static ConfigFileReader configFileReader = new ConfigFileReader();
	UtilityLibrary util = new UtilityLibrary();
	String fs=File.separator;

	@BeforeSuite
	public void reportGeneration() {
		htmlReporter = new ExtentHtmlReporter(configFileReader.getReportPath()+fs + "Report" + util.timeStamp() + ".html");
		report = new ExtentReports();
		report.attachReporter(htmlReporter);
		report.setSystemInfo("Environment ", configFileReader.getEnvironment());
		report.setSystemInfo("Browser ", configFileReader.getBrowserType());
		report.setSystemInfo("isHeadless ", configFileReader.getHeadless());
		report.setSystemInfo("OS ", System.getProperty("os.name"));
		report.setSystemInfo("Device ", configFileReader.getDevice());
		
	}

	public void report(Method method , String options, String country) {
		String descriptiveTestName = method.getAnnotation(Test.class).testName() +" "+options+" "+country;
		test = report.createTest(descriptiveTestName);

	}
	public void report(Method method , String options) {
		String descriptiveTestName = method.getAnnotation(Test.class).testName() +" "+options;
		test = report.createTest(descriptiveTestName);

	}
	public void report(Method method) {
		String descriptiveTestName = method.getAnnotation(Test.class).testName();
		test = report.createTest(descriptiveTestName);

	}

	@AfterSuite
	public void tear() {
		report.flush();
	}

	@AfterMethod
	public void reportLogger(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.fail(MarkupHelper.createLabel(result.getName() + " Test Case Failed", ExtentColor.RED));
			// To add it in the extent report
			test.fail("Snapshot below: " + test.addScreenCaptureFromPath(util.fileName));
		} else if (result.getStatus() == ITestResult.SUCCESS)
			test.pass(MarkupHelper.createLabel(result.getName() + " Test Case Passed", ExtentColor.GREEN));
		else if (test.getStatus()==Status.WARNING)
			test.warning(MarkupHelper.createLabel(result.getName() + " Test Case Passed", ExtentColor.ORANGE));
		else
			test.skip(MarkupHelper.createLabel(result.getName() + " Test Case Skipped", ExtentColor.BLUE));

	}

}
