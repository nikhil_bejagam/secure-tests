package com.utils;

import org.apache.log4j.*;
import org.testng.Assert;

import com.utils.TestLogger;

public class TestLogger 
{
	public static Logger logger = Logger.getLogger(TestLogger.class);

	/**** 
	 * Method: pass method logs in pass result
	 * Parameters: String
	 * Author: Bharathi Bhogavarapu
	****/
	public void pass(String message)
	{
		System.out.println(message);
		Assert.assertTrue(true, message);
		logger.info(message);
	}
	
	/**** 
	 * Method: fail method logs in fail result
	 * Parameters: String
	 * Author: Bharathi Bhogavarapu
	****/
	public void fail(String message) 
	{
		System.out.println(message);
		Assert.fail(message);
		logger.error(message);
			
	}
	
	/**** 
	 * Method: info method logs in info message
	 * Parameters: String
	 * Author: Bharathi Bhogavarapu
	****/
	public void info(String message)
	{
		System.out.println(message);
		logger.info(message);
	}
	
	
}
