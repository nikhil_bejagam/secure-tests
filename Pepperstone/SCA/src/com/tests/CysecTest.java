package com.tests;

import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.pageDefinitions.CysecPage;
import com.pageDefinitions.VerificationPage;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;


public class CysecTest extends TestConfig{
	
	
	
	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;

	TestLogger log = new TestLogger();
	JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
	UtilityLibrary util = new UtilityLibrary();
	
	
	@Test(groups = { "Cysectest"},dataProvider = "Cysec_License",testName = "Verify Important,Footer notes on Registration page with Cysec Licensed.", description = "verifying Important and footer on Registration page with Risk warning message.")
	public void verifyFooterNotesOnRegistrationPage(Method method,String accountName )
	throws InterruptedException {
		try {
		ExtentReportTestCaseName(method);
		Thread.sleep(2000);
		homePage.ValidateExistingPageIsLoginPage();
		homePage.clickSignUpLink(util.environmentURl() + "/register");
		Thread.sleep(2000);
		cysecPage.verifyImportantAndFooterNoteOnRegistrationPage(accountName);
		
		ExtentReportGeneration.test.log(Status.PASS, "Validated Important and Footer Note on Registration Page.");
		} catch (Exception e) {
		ExtentReportGeneration.test.fail(e);
		log.fail("exception : " + e.getMessage());
		}
	}
	
	
	@Test(groups = { "Cysectest"},dataProvider = "Cysec_License",testName = "Verify Footer Links on Registration page with Cysec Licensed.", description = "verifying footer on Registration page with Risk warning message.")
	public void verifyFooterLinksOnRegistrationPage(Method method,String accountName )
	throws InterruptedException {
		try {
		ExtentReportTestCaseName(method);
		Thread.sleep(2000);
		homePage.ValidateExistingPageIsLoginPage();
		homePage.clickSignUpLink(util.environmentURl() + "/register");
		Thread.sleep(2000);
		cysecPage.verifyFooterLinkOnRegistrationPage(accountName);
		
		ExtentReportGeneration.test.log(Status.PASS, "Validated Footer Links on Registration Page.");
		} catch (Exception e) {
		ExtentReportGeneration.test.fail(e);
		log.fail("exception : " + e.getMessage());
		}
	}
	
	@Test(groups = { "Cysectest"},dataProvider = "Cyprus",testName = "Verify Cyprus country URL by clicking 'here' link on model popup on Registration page with Cysec Licensed.", description = "verifying Cyprus Country URL by clicking 'here' link on model popup.")
	public void verifyCyprusCountryURLOnRegistrationPage(Method method,String accountName )
	throws InterruptedException {
		try {
		ExtentReportTestCaseName(method);
		Thread.sleep(2000);
		homePage.ValidateExistingPageIsLoginPage();
		homePage.clickSignUpLink(util.environmentURl() + "/register");
		Thread.sleep(2000);
		cysecPage.verifyCountryURLOnRegistrationPage(accountName);
		
		ExtentReportGeneration.test.log(Status.PASS, "Validated Cyprus country URL by clicking 'here' link on model popup on Registration page.");
		} catch (Exception e) {
		ExtentReportGeneration.test.fail(e);
		log.fail("exception : " + e.getMessage());
		}
	}
	
	 @Test(groups = { "Cysectest"},dataProvider = "Cysec_Reg",testName = "Verify 'Plus' button is changed to 'request an account' button under Account tab for Live and Demo tab.", description = "Verify 'Plus' button is changed to 'request an account' button under Account tab for Live and Demo tab.")
     public void verifyRequestAnAccountButtonOnAccountTab(Method method,String accountName )
     throws InterruptedException {
         try {
             ExtentReportTestCaseName(method, accountName);
             Thread.sleep(2000);
             homePage.ValidateExistingPageIsLoginPage();
             homePage.clickSignUpLink(util.environmentURl() + "/register");
             Thread.sleep(2000);
             cysecPage.registrationWithEEACountry(accountName);
             Thread.sleep(5000);
             cysecPage.verifyRequestAccountButtonOnAccountTabForLiveAndDemoIsDisplayed();
             
             ExtentReportGeneration.test.log(Status.INFO, "Validated Request Account Button isdisplayed on both Live and Demo Link.");
         } catch (Exception e) {
             ExtentReportGeneration.test.fail(e);
             log.fail("exception : " + e.getMessage());
         }
     }
	 
	 @Test(groups = { "Cysectest"},dataProvider = "Cyprus_Reg",testName = "Verify footer note as Risk Warning/Disclaimer message in SCA.", description = "Verify footer note as Risk Warning/Disclaimer message in SCA.")
     public void verifyFooterNoteOnVerificationPage(Method method,String accountName )
     throws InterruptedException {
         try {
             ExtentReportTestCaseName(method, accountName);
             Thread.sleep(2000);
             homePage.ValidateExistingPageIsLoginPage();
             homePage.clickSignUpLink(util.environmentURl() + "/register");
             Thread.sleep(2000);
             cysecPage.registrationWithEEACountry(accountName);
             Thread.sleep(5000);
             
             String expectedFooterNote="Risk Warning: CFDs are complex instruments and come with a high risk of losing money rapidly due to leverage. The vast majority of retail client accounts lose money when trading in CFDs. You should consider whether you understand how CFDs work and whether you can afford to take the high risk of losing your money.";
             String actualFooterNote=cysecPage.getVerificationPageFooterNote().getText();
             log.info("Actual Footer note :::"+actualFooterNote);
             Assert.assertTrue(expectedFooterNote.contains(actualFooterNote), "Failed:- Mismatch of footer note between actual and expected.");
             
             ExtentReportGeneration.test.log(Status.PASS, "Validated footer note as Risk Warning/Disclaimer message in SCA.");
         } catch (Exception e) {
             ExtentReportGeneration.test.fail(e);
             log.fail("exception : " + e.getMessage());
         }
     }
	 
	 @Test(groups = { "Cysectest"},testName = "Verify Legal Documentation under Declaration Tab in SCA.", description = "Verify Legal Documentation under Declaration Tab in SCA.")
     public void verifyLegalDocumentation(Method method )
     throws InterruptedException {
         try {
        	 log.info("Validating Declaration test with all six links.");
 			ExtentReportTestCaseName(method);
 			ExtentReportGeneration.test.log(Status.INFO, "Validating Declaration test with all six links");
 			homePage.login("LoginDeclaration");
 			cysecPage.veifyDeclarationLinks();
 			verificationPage.logout();
 		} catch (Exception e) {
 			ExtentReportGeneration.test.fail(e);
 			log.fail("exception : " + e.getMessage());
 			log.fail("Validation failed for Declaration Test with all six links.");
 			verificationPage.logout();
 		}
     }
	  
	@DataProvider(name ="Cysec_Reg")
	public static Object[][] Cysec_Reg() {
		return new Object[][] { { "Cysec" } };
	}
		
	@DataProvider(name ="Cysec_License")
	public static Object[][] Cysec_License() {
		return new Object[][] { { "Cysec_Footer" } };
	}
	
	@DataProvider(name ="Cyprus")
	public static Object[][] Cyprus() {
		return new Object[][] { { "Cyprus" } };
	}
	
	@DataProvider(name ="Cyprus_Reg")
	public static Object[][] Cyprus_Reg() {
		return new Object[][] { { "Cyprus_Registration" } };
	}
	
	@DataProvider(name ="Spain_Reg")
	public static Object[][] Spain_Reg() {
		return new Object[][] { { "Spain_Declaration" } };
	}
	
	
}

