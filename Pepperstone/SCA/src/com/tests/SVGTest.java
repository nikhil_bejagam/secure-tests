package com.tests;

import static org.testng.Assert.assertTrue;

import java.lang.reflect.Method;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.utils.ConfigFileReader;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

import org.junit.Assert;

@Listeners(com.utils.TestListener.class)

public class SVGTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;

	TestLogger log = new TestLogger();
	// JsonReader jr = new JsonReader();
	UtilityLibrary util = new UtilityLibrary();
	ConfigFileReader configFileReader = new ConfigFileReader();
	ExtentReportGeneration extreport = new ExtentReportGeneration();

	@Test(groups = { "SVG" }, testName = "Verify svg terms and conditiona and submittion of Ib account", enabled = false)
	public void verifySvgTermsAndConditionIBAccount(Method method) {
		try {
			util.pageWait();
			Thread.sleep(2000);
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			registrationPage.Registration("ChinaIntroducingBroker");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			svg.fillIBDetails("China");
			svg.validateSVGtermsandConditionNavigation();
			svg.fillprogramInfo();
			svg.fillChineseName("China");
			verificationPage.fillPersonalDetailsWithNationalID("China");
			svg.submitPage();
			svg.validateIBSubmission();
			homePage.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to login with correct credentials");
			homePage.logout();
		}
	}

	@Test(groups = { "SVG" }, testName = "Verify China Lands to svg licence", enabled = false)
	public void verifyChinaInsvglicence(Method method) {
		String emailID = null;
		try {
			util.pageWait();
			Thread.sleep(2000);
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("ChinaIntroducingBroker");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			String licence = util.getuserlicencefromDB(emailID);
			ExtentReportGeneration.test.log(Status.INFO, "user id is " + licence);
			Assert.assertEquals("svg", licence);
			ExtentReportGeneration.test.log(Status.PASS, "user lands to SVG for china");
			homePage.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to register a user under svg");
			homePage.logout();
		}
	}

	@Test(groups = { "SVG" }, testName = "Verify China Lands to svg licence")
	public void verifyAccountsforChina(Method method) {
		try {
			util.pageWait();
			Thread.sleep(2000);
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.login("chinaUser");
			svg.validateAddfundsoption();
			homePage.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to login with correct credentials");
			homePage.logout();
		}
	}

	/**
	 * verifySVGAddressInContactPage() Test case is commented as contact us Link
	 * is removed from the Home page.
	 * 
	 * @param method
	 * @throws InterruptedException
	 */

	/*
	  @Test(groups = { "SVGTest"},testName = "verifySVGAddressInContactPage()",
	  description = "Verify SVG Address card in contact us page.") public void
	  verifySVGAddressInContactPage(Method method ) throws InterruptedException
	  { 
		  try { log.info("Verify Cyprus Address card in contact us page.");
	  ExtentReportTestCaseName(method);
	  ExtentReportGeneration.test.log(Status.INFO,"Validating SVG Address card in contact us page.");
	  homePage.login("LoginSVG"); svg.verifyAddressCard();
	  verificationPage.logout(); } catch (Exception e) {
	  ExtentReportGeneration.test.fail(e); log.fail("exception : " +
	  e.getMessage());
	  log.fail("Validation failed for SVG Address card in contact us page.");
	  verificationPage.logout();
	 }
 }*/
	 

	@Test(groups = {"SVG" }, testName = "verifyLeverageUnderDemoAccount()", description = "Verify Leverage option is removed from Request an Individual Demo Account model popup..")
	public void verifyLeverageUnderDemoAccount(Method method) throws InterruptedException {
		try {
			log.info("Verify Leverage option is removed from Request an Individual Demo Account model popup..");
			ExtentReportTestCaseName(method);
			ExtentReportGeneration.test.log(Status.INFO,
					"Validating Leverage option is removed from Request an Individual Demo Account model popup.");
			homePage.login("LoginSVG_Trading");
			svg.verifyRequestAnIndividualDemoAccount();
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail(
					"Validation failed :: Leverage option is removed from Request an Individual Demo Account model popup..");
			verificationPage.logout();
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyRiskWarningMessageOnRegistrationPage()", description = "verifying Risk warning message on Registration page with both Language.")
	public void verifyRiskWarningMessageOnRegistrationPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			svg.verifyRiskWarningMessageInBothLanguage("SVG");

			ExtentReportGeneration.test.log(Status.PASS,
					"Validated Risk warning message on Registration Page with Chinese and English Language.");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyAddressOnRegistrationPage()", description = "Verify Address and phone number on Account Registration Page with SVG Country.")
	public void verifyAddressOnRegistrationPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			svg.verifyAddressAndPhoneNumberOnRegistrationPage("SVG");

			ExtentReportGeneration.test.log(Status.PASS,
					"Verify Address and Phone number on Account Registration Page with SVG Country.");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyFooterLinkOnRegistrationPage()",enabled=false,description = "Verify Footer links on Registration Page with SVG Country.")
	public void verifyFooterLinkOnRegistrationPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(3000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			util.waitForPageLoaded();

			ExtentReportGeneration.test.log(Status.INFO, "Starts Verifying Footer Links");
			svg.verifyFooterLink("SVG");
			ExtentReportGeneration.test.log(Status.INFO, "Footer links is successfully verified.");
			ExtentReportGeneration.test.log(Status.PASS, "Verify Footer Links on Registration Page with SVG Country.");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyRiskWarningMessageOnVerificationPage()", description = "verifying Risk warning message on Verification page.")
	public void verifyRiskWarningMessageOnVerificationPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			Thread.sleep(2000);
			homePage.login("LoginSVG_Trading");
			Thread.sleep(2000);
			svg.verifyRiskWarningMessage("SVG");

			ExtentReportGeneration.test.log(Status.PASS, "Validated Risk warning message on Verification Page.");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyPrivacyPolicyAndLoginLinkOnRegistrationPage()",enabled=false,description = "verifying Privacy policy and Login here link on Registration page with both Language.")
	public void verifyPrivacyPolicyAndLoginLinkOnRegistrationPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			svg.verifyLinksInBothLanguage("SVG");

			ExtentReportGeneration.test.log(Status.PASS,
					"Validated Risk warning message on Registration Page with Chinese and English Language.");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyEmploymentAndIncomeWithYesOption()", description = "Verify Employment & Income flow with New Questionnaire with Yes option,Passes quiz and move to Declaration Section.")
	public void verifyEmploymentAndIncomeWithYesOption(Method method) throws InterruptedException {
		String emailID = null;
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("China");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal",
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("ChinaSVG");
			verificationPage.fillChineseName("ChinaSVG");
			verificationPage.fillDOBPersonaldetails("ChinaSVG");
			verificationPage.fillAddressDetails("ChinaSVG");
			verificationPage.fillNationalityPersonalDetails("ChinaSVG");
			verificationPage.fillSecretQuestionPersonalDetails("ChinaSVG");
			verificationPage.fillMobileNumber("ChinaSVG");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingAccountPassword("MetaTrader4_STD");
			util.scrollIntoView(verificationPage.getNext());
			verificationPage.navigateToNextTab();
			verificationPage.FillemploymentandIncomeSVGWithYesOption("EmploymentAndIncomeWithYesOption", true);

			assertTrue(verificationPage.getDeclarationPage().isDisplayed(),
					"Failed:-Declaration section is not displayed.");
			log.info(
					"VerificationPage - Employment & Income flow with New Questionnaire with Yes option,Passes quiz and move to Declaration Section.");
			ExtentReportGeneration.test.log(Status.PASS,
					"Verify Employment & Income flow with New Questionnaire with Yes option,Passes quiz and move to Declaration Section.");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyEmploymentAndIncomeWithNoOption()", description = "Verify Employment & Income flow with New Questionnaire with No option,Passes quiz and move to Declaration Section.")
	public void verifyEmploymentAndIncomeWithNoOption(Method method) throws InterruptedException {
		String emailID = null;
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("China");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal",
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("ChinaSVG");
			verificationPage.fillChineseName("ChinaSVG");
			verificationPage.fillDOBPersonaldetails("ChinaSVG");
			verificationPage.fillAddressDetails("ChinaSVG");
			verificationPage.fillNationalityPersonalDetails("ChinaSVG");
			verificationPage.fillSecretQuestionPersonalDetails("ChinaSVG");
			verificationPage.fillMobileNumber("ChinaSVG");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingAccountPassword("MetaTrader4_STD");
			util.scrollIntoView(verificationPage.getNext());
			verificationPage.navigateToNextTab();
			verificationPage.FillemploymentandIncomeSVGWithYesOption("EmploymentAndIncomeWithNoOption", false);

			assertTrue(verificationPage.getDeclarationPage().isDisplayed(),
					"Failed:-Declaration section is not displayed.");
			log.info(
					"VerificationPage - Employment & Income flow with New Questionnaire with No option,Passes quiz and move to Declaration Section.");
			ExtentReportGeneration.test.log(Status.PASS,
					"Verify Employment & Income flow with New Questionnaire with No option,Passes quiz and move to Declaration Section.");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyEmploymentAndIncomeWithAppropriatenessTest()", description = "Verify Employment & Income flow with Appropriateness test.")
	public void verifyEmploymentAndIncomeWithAppropriatenessTest(Method method) throws InterruptedException {
		String emailID = null;
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("China");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal",
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("ChinaSVG");
			verificationPage.fillChineseName("ChinaSVG");
			verificationPage.fillDOBPersonaldetails("ChinaSVG");
			verificationPage.fillAddressDetails("ChinaSVG");
			verificationPage.fillNationalityPersonalDetails("ChinaSVG");
			verificationPage.fillSecretQuestionPersonalDetails("ChinaSVG");
			verificationPage.fillMobileNumber("ChinaSVG");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingAccountPassword("MetaTrader4_STD");
			util.scrollIntoView(verificationPage.getNext());
			verificationPage.navigateToNextTab();
			verificationPage.FillemploymentandIncomeSVG("EmploymentAndIncomeAppropriateness");

			assertTrue(verificationPage.getAppropriatenessTestButton().isDisplayed(),
					"Failed:-Appropriateness section is not displayed.");
			log.info("VerificationPage - Verified Employment & Income flow and move to Appropriateness Section.");
			ExtentReportGeneration.test.log(Status.PASS,
					"Verified Employment & Income flow and move to Appropriateness Section.");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyAppropriatenessTestWithSuitabilityTest()", description = "Verify Appropriateness test by passing Suitability test.")
	public void verifyAppropriatenessTestWithSuitabilityTest(Method method) throws InterruptedException {
		String emailID = null;
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("China");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal",
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("ChinaSVG");
			verificationPage.fillChineseName("ChinaSVG");
			verificationPage.fillDOBPersonaldetails("ChinaSVG");
			verificationPage.fillAddressDetails("ChinaSVG");
			verificationPage.fillNationalityPersonalDetails("ChinaSVG");
			verificationPage.fillSecretQuestionPersonalDetails("ChinaSVG");
			verificationPage.fillMobileNumber("ChinaSVG");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingAccountPassword("MetaTrader4_STD");
			util.scrollIntoView(verificationPage.getNext());
			verificationPage.navigateToNextTab();
			verificationPage.FillemploymentandIncomeSVG("EmploymentAndIncomeAppropriateness");
			verificationPage.beginAppropriatenessTest();
			Thread.sleep(5000);
			verificationPage.verifySuitabilityTest_SVG("PassWithSuitabilityTest");

			assertTrue(verificationPage.getDeclarationPage().isDisplayed(),
					"Failed:-Declaration section is not displayed.");
			log.info("Pass:: - Declaration Section landed successfully.");
			ExtentReportGeneration.test.log(Status.PASS,
					"Verified Suitability test of Appropriateness Test and moved to Declaration section.");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = {"SVG" }, testName = "verifyDeclaration() ",enabled=false,description = "verify Links under Declaration section.")
	public void verifyDeclaration(Method method) throws InterruptedException {
		try {

			log.info("Validating Declaration test with Four links.");
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			ExtentReportGeneration.test.log(Status.INFO, "Validating Declaration test with Four links");
			homePage.login("LoginSVG_Declaration");
			svg.veifyDeclarationLinks();
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for Declaration Test with all eight links.");
			verificationPage.logout();
		}
	}
	
	@Test(groups = {"SVG" }, testName = "verifyAppropriatenessTestWithKnowledgeTest()",description = "Verify Appropriateness test by failing Suitability test and moving to knowledge test.")
	public void verifyAppropriatenessTestWithKnowledgeTest(Method method) throws InterruptedException {
		String emailID = null;
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("China");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal","Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("ChinaSVG");
			verificationPage.fillChineseName("ChinaSVG");
			verificationPage.fillDOBPersonaldetails("ChinaSVG");
			verificationPage.fillAddressDetails("ChinaSVG");
			verificationPage.fillNationalityPersonalDetails("ChinaSVG");
			verificationPage.fillSecretQuestionPersonalDetails("ChinaSVG");
			verificationPage.fillMobileNumber("ChinaSVG");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingAccountPassword("MetaTrader4_STD");
			util.scrollIntoView(verificationPage.getNext());
			verificationPage.navigateToNextTab();
			verificationPage.FillemploymentandIncomeSVG("EmploymentAndIncomeAppropriateness");
			verificationPage.beginAppropriatenessTest();
			Thread.sleep(5000);
			verificationPage.verifyFailSuitabilityTest_SVG("FailWithSuitabilityTest");
			ExtentReportGeneration.test.log(Status.INFO, "Suitability Test scored score is less than 10 so proceeding to Knowledge test with Set 2 Question.");
			Thread.sleep(3000);
			String actualPaging=verificationPage.getPaging_Set2_SVG().getText();
			log.info("Actual Paging::"+actualPaging);
			assertTrue(actualPaging.equals("1 of 10"), "Failed:- Wrong paging is displayed.");
			verificationPage.verifyKnowledgeTestWithSet2_SVG();
			assertTrue(verificationPage.getDeclarationPage().isDisplayed(),"Failed:-Declaration section is not displayed.");
			log.info("Pass:: - Declaration Section landed successfully.");
			ExtentReportGeneration.test.log(Status.PASS,"Cleared Knowledge test of Appropriateness Test and moved to Declaration section.");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}
	
	@Test(groups = {"SVG" }, testName = "verifyAppropriatenessTestWithFailScenario()",description = "Verify Appropriateness test by failing Knowledge test.")
	public void verifyAppropriatenessTestWithFailScenario(Method method) throws InterruptedException {
		String emailID = null;
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("China");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal","Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("ChinaSVG");
			verificationPage.fillChineseName("ChinaSVG");
			verificationPage.fillDOBPersonaldetails("ChinaSVG");
			verificationPage.fillAddressDetails("ChinaSVG");
			verificationPage.fillNationalityPersonalDetails("ChinaSVG");
			verificationPage.fillSecretQuestionPersonalDetails("ChinaSVG");
			verificationPage.fillMobileNumber("ChinaSVG");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingAccountPassword("MetaTrader4_STD");
			util.scrollIntoView(verificationPage.getNext());
			verificationPage.navigateToNextTab();
			verificationPage.FillemploymentandIncomeSVG("EmploymentAndIncomeAppropriateness");
			verificationPage.beginAppropriatenessTest();
			Thread.sleep(5000);
			verificationPage.verifyFailSuitabilityTest_SVG("FailWithSuitabilityTest");
			ExtentReportGeneration.test.log(Status.INFO, "Suitability Test scored score is less than 10 so proceeding to Knowledge test with Set 2 Question.");
			Thread.sleep(3000);
			String actualPaging=verificationPage.getPaging_Set2_SVG().getText();
			log.info("Actual Paging::"+actualPaging);
			assertTrue(actualPaging.equals("1 of 10"), "Failed:- Wrong paging is displayed.");
			verificationPage.knowledgeTestWithFailSet2_SVG();
			
			assertTrue(verificationPage.getUnsuccesfulMessage_SVG().isDisplayed(),"Failed:-unSuccessful message is not displayed .");
			log.info("Pass:: - Model pop up is displayed with unsuccessful message about Account is blocked for seven days.");
			verificationPage.getCloseButton_SVG().click();
			Thread.sleep(3000);
			driver.navigate().back();
			Thread.sleep(2000);
			verificationPage.getNegativeCloseButton_SVG().click();
			verificationPage.logout();
			ExtentReportGeneration.test.log(Status.PASS,"Model pop up is displayed with unsuccessful message about Account is blocked for seven days.");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

}
