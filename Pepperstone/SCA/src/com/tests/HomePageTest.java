package com.tests;

import java.lang.reflect.Method;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.utils.ConfigFileReader;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

@Listeners(com.utils.TestListener.class)

public class HomePageTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;

	TestLogger log = new TestLogger();
	//JsonReader jr = new JsonReader();
	UtilityLibrary util = new UtilityLibrary();
	ConfigFileReader configFileReader = new ConfigFileReader();
	ExtentReportGeneration extreport = new ExtentReportGeneration();

	@Test(groups = { "functest",
			"Login" }, testName = "RTSFS-21: Verify Login with Correct username and correct Password")
	public void verifyLogin_CorrectLoginCredentials(Method method) {
		try {
			ExtentReportTestCaseName(method);
			if (environment.equalsIgnoreCase("beta")){
			homePage.login("LoginBeta");
			}
			else{
			homePage.login("Login");
			}
			Thread.sleep(5000);
			homePage.validateURL(util.environmentURl() +"/accounts/live",
					"Verify Login with Correct username and correct Password");
			ExtentReportGeneration.test.log(Status.PASS, "Successfully logged in with correct credentials");
			homePage.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to login with correct credentials");
			homePage.logout();
		}
	}

	@Test(groups = { "functest",
			"Login" }, testName = "RTSFS-43: Verify Login with incorrect username andincorrect Password")
	public void verifyLogin_IncorrectLoginPwd(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.login("Login1");
			homePage.validateURL(util.environmentURl()+"/login",
					"Verify Login with incorrect username and incorrect Password");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}

	}

	@Test(groups = { "functest",
			"Login" }, testName = "RTSFS-20: Verify Login with correct username andincorrect Password")
	public void verifyLogin_IncorrectPwd(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.login("Login2");
			homePage.validateURL(util.environmentURl()+"/login",
					"Verify Login with correct username and incorrect Password");

		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}

	}

	@Test(groups = { "functest",
			"Login" }, testName = "RTSFS-17: Verify Link \"Contact us\" points to correct link")
	public void verifyContactUsLink(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.clickContactUsLink("contact-us");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}

	}

	@Test(groups = { "functest",
			"Login" }, testName = "RTSFS-12: Verify Link text \"Sign up here\" pointsto correct link")

	public void verifySignUpLink(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl()+"/register");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}

	}

	@Test(groups = { "functest", "Login" }, testName = "RTSFS-14: VerifyLogin via Linkedin link")

	public void verifyLinkedinBtn(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickLinkedinBtn();
			homePage.SwitchAndCloseWindow("linkedin.com");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}

	}

	@Test(groups = { "functest", "Login" }, testName = "RTSFS-16: VerifyLogin via Google+link")

	public void verifyGoogleBtn(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickGoogleBtn();
			homePage.SwitchAndCloseWindow("accounts.google.com");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}

	}

	@Test(groups = { "functest",
			"Login" }, testName = "RTSFS-17: Verify Link \"Forgotten Password\"points to correct link")

	public void verifyForgotPwdLink(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickForgotPwdLink(util.environmentURl()+"/reset-password");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = { "functest", "Login" }, testName = "RTSFS-22: Verify\"Remember me\" checkbox works")
	public void verifyRememberMeChkbox(Method method) {
		
		try {
			ExtentReportTestCaseName(method);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.SelectRememberMeChkbox();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}
	}
	
	
	@Test(groups = { "translations", "Login" }, testName = "Check translations with different languages on homepage")
  
	public void ztranslationsOnHomePage(Method method) {
		
		try {
			ExtentReportTestCaseName(method);
			
			homePage.translationsWithDifferentLanguages();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}
	}
	
	
	

	/*@Test(groups = { "functest","Login" }, testName = "RTSFS-19: Verify Chat widget pops up when clickedupon", priority = 4)
	public void VerifyChatPopup(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.CheckChatNow();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
		}
	}*/

}
