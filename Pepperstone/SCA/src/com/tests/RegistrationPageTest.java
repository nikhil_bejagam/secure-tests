package com.tests;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.pageDefinitions.VerificationPage;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

@Listeners(com.utils.TestListener.class)

public class RegistrationPageTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;

	TestLogger log = new TestLogger();
	JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
	UtilityLibrary util = new UtilityLibrary();
	VerificationPage verificationpage = new VerificationPage();

	@Test(groups = { "functest",
			"registration" }, dataProvider = "Individual&IB_Accounts", testName = "Verify Registration with correct details", description = "verifying correct registration")
	public void verifyRegistration_Individual_IBCorrectDetails(Method method, String account)
			throws InterruptedException {
		try {
			ExtentReportTestCaseName(method, account);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			registrationPage.Registration(account);
			String acc = jr.GetElementValue(account + ".AccountType");
			ExtentReportGeneration.test.log(Status.INFO,
					"SignUp with Correct details(" + account + ") for " + acc + " Account is done");
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + jr.GetElementValue(account + ".Url"),
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = { "functest",
			"registration" }, dataProvider = "JointAccounts", testName = "Verify Registration with correct details", description = "verifying correct registration")
	public void verifyRegistration_JointCorrectDetails(Method method, String account) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method, account);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			registrationPage.Registration(account);
			String acc = jr.GetElementValue(account + ".AccountType");
			ExtentReportGeneration.test.log(Status.INFO,
					"SignUp with Correct details(" + account + ")for" + acc + " Account is done");
			registrationPage.AccountValidation(acc);
			Thread.sleep(3000);
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = { "functest",
			"registration" }, dataProvider = "negativeScenarios", testName = "Verify Registration with Incorrect details", description = "RTSFS:39 to 42 - verifying registration with Incorrect details")
	public void verifyRegistration_InCorrectDetails(Method method, String account) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method, account);
			Thread.sleep(2000);
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			registrationPage.Registration(account);
			ExtentReportGeneration.test.log(Status.INFO, "SignUp with Incorrect details(" + account + ") is done");
			Thread.sleep(2000);
			registrationPage.validateURL(util.environmentURl() + "/register", "Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");

		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = { "functest",
			"registration" }, testName = "Verify Registration FromGetStartedPage", description = "RTSFS:27 -Verify If User is able to register as an Introducing Broker")
	public void verifyRegistration_FromGetStartedPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			registrationPage.SignUpAsIntroducingBrokerFromGetStartedPage("IntroducingBroker");
			ExtentReportGeneration.test.log(Status.INFO, "SignUp as Introducing broker is Successful");
			UtilityLibrary.waitForPageWithUrl(jr.GetElementValue("IntroducingBroker" + ".UrlToVerify"), 15);
			registrationPage.validateURL(jr.GetElementValue("IntroducingBroker" + ".UrlToVerify"),
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Navigated to the specified Page");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@Test(groups = { "translations",
			"registration" }, testName = "Translations on Registration Page With different languages", description = "Verify Translations on Registration Page With different languages")
	public void VerifyTranslationsForRegistrationPage(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			Thread.sleep(2000);
			homePage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			registrationPage.translationsWithDifferentLanguages();
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
		}
	}

	@DataProvider(name = "Individual&IB_Accounts")
	public static Object[][] individualAndIB() {
		return new Object[][] { { "Individual" }, { "IntroducingBroker" } };
	}

	@DataProvider(name = "JointAccounts")
	public static Object[][] jointAccounts() {
		return new Object[][] { { "Joint" }, { "Company" }, { "Trust" }, { "TrustCorporate" }, { "Demo" } };
	}

	@DataProvider(name = "negativeScenarios")
	public static Object[][] negativeScenarios() {
		return new Object[][] { { "CompanyInvalid" }, { "NameInvalid" }, { "PasswordMismatch" },
				{ "NilEmailAndPassword" } };
	}
}
