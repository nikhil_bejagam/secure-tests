package com.tests;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.utils.ConfigFileReader;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

@Listeners(com.utils.TestListener.class)

public class PaymentsTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;

	TestLogger log = new TestLogger();
	JsonReader jr = new JsonReader("jsonFilePathpaymentsPage");
	UtilityLibrary util = new UtilityLibrary();
	ConfigFileReader configFileReader = new ConfigFileReader();
	ExtentReportGeneration extreport = new ExtentReportGeneration();

	
	@Test(groups = { "functest", "payments" }, dataProvider = "depositeAccounts", testName = "ADD funds Live Account")
	public void addFundsLive(Method method, String account) {
		try {
			ExtentReportTestCaseName(method, account);
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.login(jr.GetElementValue(account + ".login"));
			payments.depositeWithAccount(account);
			ExtentReportGeneration.test.log(Status.PASS, "Successfully added funds" + account);
			payments.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to add funds");
			payments.logout();
		}
	}

	@Test(groups = { "functest", "payments" }, testName = "ADD funds Demo Account")
	public void addFundsDemo(Method method) {
		try {
			ExtentReportTestCaseName(method);
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.login("Payments");
			payments.addfundsDemo();
			ExtentReportGeneration.test.log(Status.PASS, "Successfully added funds");
			payments.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to add funds");
			payments.logout();
		}
	}

	@Test(groups = { "functest", "payments" }, dataProvider = "transferFunds", testName = "Transfer funds ")
	public void transferFunds(Method method, String transfer) {
		try {
			ExtentReportTestCaseName(method, transfer);
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.login("Payments");
			payments.transferfunds(transfer);
			ExtentReportGeneration.test.log(Status.PASS, "Successfully transfered funds");
			payments.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to transfer funds");
			payments.logout();
		}
	}

	@Test(groups = { "functest",
			"payments" }, dataProvider = "AccountType", testName = "Change Password for account -  ")
	public void changePassword(Method method, String account) {
		try {
			ExtentReportTestCaseName(method, account);
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.login("Payments");
			payments.changepassword(account);
			ExtentReportGeneration.test.log(Status.PASS,
					"Successfully validated password change for " + account + " account");
			payments.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to change password for" + account + "account");
			payments.logout();
		}
	}

	@Test(groups = { "functest", "payments" }, dataProvider = "AccountType", testName = "Account Edit Nick name - ")
	public void verifyEditNickName(Method method, String account) {
		try {
			ExtentReportTestCaseName(method, account);
			homePage.login("LoginFCA");
			payments.verifyEditNickName(account);
			ExtentReportGeneration.test.log(Status.PASS, "Nick name edited successfully.");
			payments.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to Edit Account Nick name.");
			payments.logout();
		}
	}
	
	@Test(groups = { "functest", "payments" }, testName = "Neteller and skrill are removed from FCA accounts")
	public void removeNetellerSkrillForFCA(Method method) {
		try {
			ExtentReportTestCaseName(method);
			homePage.login("LoginFCA");
			payments.noNetellerAndSkrill();
			ExtentReportGeneration.test.log(Status.PASS, "Neteller and skrill accounts are removed from FCA accounts");
			payments.logout();
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Neteller and skrill accounts still exist for FCA");
			payments.logout();
		}
	}

	@DataProvider(name = "depositeAccounts")
	public static Object[][] depositeAccounts() {
		return new Object[][] {  { "NetellerAUD" }, { "NetellerEUR" }, { "NetellerUSD" },
				};
		// { "Visa" }, {"NetellerCHF"}, {"NetellerGBP"},{ "POLi" },{ "Paypal" } 
	}

	@DataProvider(name = "transferFunds")
	public static Object[][] TransferFunds() {
		return new Object[][] { { "inDifferentCurrency" }, { "inSameCurrency" } };
	}

	@DataProvider(name = "AccountType")
	public static Object[][] AccountType() {
		return new Object[][] { { "Demo" }, { "Live" } };
	}

}
