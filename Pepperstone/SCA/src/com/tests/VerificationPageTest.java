package com.tests;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

@Listeners(com.utils.TestListener.class)

public class VerificationPageTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;
	TestLogger log = new TestLogger();
	JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
	JsonReader jrV = new JsonReader("jsonFilePathverificationPage");
	UtilityLibrary util = new UtilityLibrary();

	@Test(groups = { "Verification", "DFSA",
			"functest" }, dataProvider = "country", testName = "NationalIDPositiveScenarios", description = "Verify NationalID with correct inputs")
	public void verifyNationalIDPositiveScenarios(Method method, String country) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method, country);
			log.info("Validating National ID for " + country);
			ExtentReportGeneration.test.log(Status.INFO, "Validating National ID for " + country);
			completeRegistration("Individual");
			verificationPage.fillPersonalDetails(country);
			verificationPage.ValidatePersonalDetailSubmission();
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation of National ID failed for " + country);
			verificationPage.logout();
		}

	}

	@Test(enabled = false, groups = { "functest", "Verification",
			"DFSA" }, dataProvider = "countryNegative", testName = "NationalIDNegativeScenarios", description = "Verify NationalID with incorrect inputs")
	public void verifyNationalIDNegativeScenarios(Method method, String country) throws InterruptedException {
		try {

			log.info("Validating National ID for " + country);
			ExtentReportTestCaseName(method, country);
			completeRegistration("UnitedArabEmirates");
			verificationPage.fillCountryInPersonalDetails(country);
			verificationPage.clickSubmit();
			verificationPage.fillDOBPersonaldetails(country);
			verificationPage.fillNationalityPersonalDetails(country);
			verificationPage.ValidateNationalIDErrorMessage();
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation of National ID failed for " + country);
			verificationPage.logout();
		}
	}

	@Test(groups = { "Verification",
			"SVG" }, dataProvider = "leverage", testName = "LeverageOption", description = "Verify availability of Leverage Option")
	public void verifyLeverageOptionDisable(Method method, String country) throws InterruptedException {
		try {

			log.info("Validating Leverage Option for " + country);
			ExtentReportTestCaseName(method, country);
			completeRegistration(country);
			if(country.equalsIgnoreCase("china")){
			svg.fillChineseName(country);
			verificationPage.fillNationalityPersonalDetails(country);
			}
			verificationPage.fillName(country);
			verificationPage.fillDOBPersonaldetails(country);
			verificationPage.fillAddressDetails(country);
			verificationPage.fillSecretQuestionPersonalDetails(country);
			verificationPage.fillMobileNumber(country);
			verificationPage.navigateToNextTab();
			verificationPage.checkLeverageOptionDisabled(country);
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation of National ID failed for " + country);
			verificationPage.logout();
		}

	}

	@Test(groups = { "functest",
			"Verification" }, dataProvider = "over18", testName = "Validate blocking user below 18 years - ", description = "verifyDateOfBirthBelow18")
	public void verifyDateOfBirth(Method method, String age) throws InterruptedException {
		try {
			log.info("Validating DateOfBirth for ");
			ExtentReportTestCaseName(method, age);
			ExtentReportGeneration.test.log(Status.INFO, "Validating DateOfBirth for " + age);
			completeRegistration("Individual");
			verificationPage.getDOB(age);
			String day = String.format("%02d", verificationPage.getDy());
			String month = String.format("%02d", verificationPage.getMon());
			ExtentReportGeneration.test.log(Status.INFO,
					"DOB entered " + verificationPage.getYr() + "-" + month + "-" + day);
			util.getDropDownByVisibleText(verificationPage.day, day);
			util.getDropDownByVisibleText(verificationPage.month, month);
			util.getDropDownByVisibleText(verificationPage.year, Integer.toString(verificationPage.getYr()));
			verificationPage.fillName("AustraliaOver18");
			verificationPage.fillAddressDetails("AustraliaOver18");
			verificationPage.fillNationalityPersonalDetails("AustraliaOver18");
			verificationPage.fillSecretQuestionPersonalDetails("AustraliaOver18");
			verificationPage.navigateToNextTab();
			if (age.contains("Below18"))
				Assert.assertTrue(verificationPage.dobErrorMsg.isDisplayed(),
						"Age is below eighteen so Trading Preferences not enabled");
			else
				Assert.assertTrue(verificationPage.tradingPreferencesExpanded.isDisplayed(), "Age is over eighteen");
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation of DateOfBirth failed for " + age);
		}

	}

	@Test(groups = { "DFSA",
			"Verification" }, testName = "Verify-DFSA ", dataProvider = "DFSAAppropriatenessKnowledgeTest", description = "verify Appropriateness Test for DFSA")
	public void AppropriateTest(Method method, String test) throws InterruptedException {
		try {

			log.info(
					"Validating Appropriateness test when User Pass in Appropriateness and does not attend Knowledge test");
			ExtentReportTestCaseName(method, test);
			ExtentReportGeneration.test.log(Status.INFO, "Validate Appropriateness test for DFSA");
			completeRegistration("UnitedArabEmirates");
			verificationPage.fillPersonalDetailsWithoutNationalityAndTitle("UnitedArabEmirates");
			verificationPage.ValidatePersonalDetailSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Submitted personal details");
			verificationPage.fillTradingPreferences(test);
			verificationPage.ValidateTradingPreferenceSubmission();
			verificationPage.fillEmploymentAndIncomeDFSA("EmploymentAndIncome-Positive");
			verificationPage.ValidateEmploymentAndIncomeSubmission();
			verificationPage.startAppropriatenessTest();
			verificationPage.AppropriatenesstestFirstset(test);
			verificationPage.navigateToNextTab();
			if (test.equalsIgnoreCase("PassWithKnowledgeTest1") || test.equalsIgnoreCase("PassWithKnowledgeTest2"))
				verificationPage.knowledgeTest(test, 1);
			if (test.equalsIgnoreCase("PassWithKnowledgeTest2") || test.equalsIgnoreCase("FailWithKnowledgeTest2"))
				verificationPage.knowledgeTest(test, 2);
			verificationPage.validateAppropriatenessTestSubmission();
			// verificationPage.declaration();
			// verificationPage.validateDeclarationSubmission();
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for Appropriateness Test");
			verificationPage.logout();
		}
	}

	@Test(groups = { "DFSA",
			"Verification" }, testName = "Verify-DFSA ", dataProvider = "DFSAAppropriatenessKnowledgeTestNegative", description = "verify Appropriateness Test for DFSA")
	public void AppropriateTest_NegativeScenarios(Method method, String test) throws InterruptedException {
		try {

			log.info(
					"Validating Appropriateness test when User Pass in Appropriateness and does not attend Knowledge test");
			ExtentReportTestCaseName(method, test);
			ExtentReportGeneration.test.log(Status.INFO, "Validate Appropriateness test for DFSA");
			completeRegistration("UnitedArabEmirates");
			verificationPage.fillPersonalDetailsWithoutNationalityAndTitle("UnitedArabEmirates");
			verificationPage.ValidatePersonalDetailSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Submitted personal details");
			verificationPage.fillTradingPreferences(test);
			verificationPage.ValidateTradingPreferenceSubmission();
			verificationPage.fillEmploymentAndIncomeDFSA("EmploymentAndIncome-Positive");
			verificationPage.ValidateEmploymentAndIncomeSubmission();
			verificationPage.startAppropriatenessTest();
			if (test.equalsIgnoreCase("AppropriatenessTest1_AllNever")) {
				verificationPage.fillAppropriatenessTestSet1Q1(test);
				Assert.assertTrue(verificationPage.getExperienceOfQualification().isDisplayed(),
						"Experience of Qualification qtn is not displayed");
				ExtentReportGeneration.test.info("Skipped 2nd qtn & Experience of Qualification qtn is displayed");

			} else if (test.equalsIgnoreCase("AppropriatenessTest1_OTCPurposeOfTradingWarningMsg")) {
				verificationPage.AppropriatenesstestFirstset(test);
				Assert.assertTrue(verificationPage.getPurposeOfTradingWarning().isDisplayed(),
						"OTC_PurposeOfTrading Warning message is not displayed");
				ExtentReportGeneration.test.info("OTC_PurposeOfTrading Warning message is displayed");
			} else {
				verificationPage.AppropriatenesstestFirstset(test);
				verificationPage.navigateToNextTab();
				verificationPage.knowledgeTest(test, 1);
				if (test.equalsIgnoreCase("FailWithKnowledgeTest2")) {
					verificationPage.knowledgeTest(test, 2);
					verificationPage.validateAndCloseUnsuccessfulPopup();
				}
			}
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("AllNeverValidation failed for Appropriateness Test");
			verificationPage.logout();
		}

	}

	@Test(groups = { "ASIC", "Verification",
			"functest" }, dataProvider = "license", testName = "Verify Employment and Income", description = "verifyEmployementAndIncome-DFSA")
	public void verifyEmployementAndIncome(Method method, String license) throws InterruptedException {
		try {

			String country = jrV.GetElementValue((license + ".country"));
			log.info("Validating Employment and income is blocked for lowest income" + country);
			ExtentReportTestCaseName(method, country);
			completeRegistration(country);
			verificationPage.fillName(country);
			verificationPage.fillDOBPersonaldetails(country);
			verificationPage.fillAddressDetails(country);
			if (country.equalsIgnoreCase("UK"))
				verificationPage.fillNationalityPersonalDetails(country);
			verificationPage.fillSecretQuestionPersonalDetails(country);
			verificationPage.fillMobileNumber(country);
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingPreferences(license);
			verificationPage.FillemploymentandIncomeAsicFca(license);
			verificationPage.validateEmploymentAndIncome(country);
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = { "DFSA",
			"Verification" }, dataProvider = "EmploymentAndIncome", testName = "Verify-DFSA ", description = "verifyEmployementAndIncome-DFSA")
	public void verifyEmployementAndIncomeDFSA(Method method, String employmentAndIncome) throws InterruptedException {
		try {

			log.info("Validating EmployementAndIncome section");
			ExtentReportTestCaseName(method, employmentAndIncome);
			ExtentReportGeneration.test.log(Status.INFO,
					"Validating EmployementAndIncomesection " + employmentAndIncome);
			completeRegistration("UnitedArabEmirates");
			verificationPage.fillPersonalDetails(employmentAndIncome);
			verificationPage.ValidatePersonalDetailSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Submitted personal details");
			verificationPage.fillTradingPreferences(employmentAndIncome);
			verificationPage.ValidateTradingPreferenceSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Trading Preference submission is done");
			verificationPage.fillEmploymentAndIncomeDFSA(employmentAndIncome);
			if (employmentAndIncome.equalsIgnoreCase("EmploymentAndIncome-Positive"))
				verificationPage.ValidateEmploymentAndIncomeSubmission();
			else if (employmentAndIncome.equalsIgnoreCase("EmploymentAndIncome-RequiredMsg"))
				verificationPage.ValidateRequiredMsgInEmploymentAndIncome();
			else
				verificationPage.ValidateWarningMsgInEmploymentAndIncome();
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for " + employmentAndIncome);
			verificationPage.logout();
		}
	}

	@Test(groups = { "ASIC",
			"Verification" }, testName = "ValidateTradingPreference ", dataProvider = "TradingPreference", description = "ValidateTradingPreference")
	public void TradingPreference(Method method, String platform, String country) throws InterruptedException {
		try {

			log.info("Validating TradingPreference");
			ExtentReportTestCaseName(method, platform, country);
			ExtentReportGeneration.test.log(Status.INFO, "Validate TradingPreference");
			completeRegistration(country);
			verificationPage.fillPersonalDetailsWithoutNationalityAndTitle(country);
			verificationPage.ValidatePersonalDetailSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Submitted personal details");
			verificationPage.fillTradingPreferences(platform);
			verificationPage.validateTradingPreference(platform);
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for TradingPreference");
			verificationPage.logout();
		}
	}

	@Test(groups = { "ASIC",
			"Verification" }, testName = "ValidatePlatform&Accounts_TradingPreference ", dataProvider = "TradingPreference_platform", description = "ValidatePlatform&Accounts_TradingPreference")
	public void ValidatePlatform_Accounts_TradingPreference(Method method, String country) throws InterruptedException {
		try {

			log.info("Validating Platform and Account types in TradingPreference");
			ExtentReportTestCaseName(method, country);
			ExtentReportGeneration.test.log(Status.INFO, "Validating Platform and Account types in TradingPreference");
			completeRegistration(country);
			verificationPage.fillPersonalDetails(country);
			verificationPage.ValidatePersonalDetailSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Submitted personal details");
			verificationPage.checkPlatform_AccType(country);
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for TradingPreference");
			verificationPage.logout();
		}
	}

	@Test(groups = { "EV_Gamma",
			"Verification" }, testName = "ElectronicVerifcation ", dataProvider = "ElectronicVerifcation", description = "ElectronicVerifcation")
	public void ElectronicVerifcation(Method method, String document) throws InterruptedException {
		try {

			log.info("Validating ElectronicVerifcation");
			ExtentReportTestCaseName(method, document);
			ExtentReportGeneration.test.log(Status.INFO, "Validate ElectronicVerifcation");
			completeRegistration("Australia");
			verificationPage.fillPersonalDetailsWithoutNationalityAndTitle("Australia_EV");
			verificationPage.ValidatePersonalDetailSubmission();
			ExtentReportGeneration.test.log(Status.INFO, "Submitted personal details");
			verificationPage.fillTradingPreferences(document);
			verificationPage.ValidateTradingPreferenceSubmission();
			verificationPage.FillemploymentandIncomeAsicFca("Australia_EV");
			util.jsClick(verificationPage.getHaveYouTradedYES());
			util.jsClick(verificationPage.getiConfirmCheckbox());
			verificationPage.navigateToNextTab();
			verificationPage.declaration();
			// verificationPage.validateDeclarationSubmission()
			if (document.equalsIgnoreCase("InvalidDataValidation"))
				verificationPage.invalidDataValidationEV(document);
			else if (document.equalsIgnoreCase("MandatoryFieldValidation")) {
				verificationPage.mandatoryFieldEV(document);
				verificationPage.addressfieldPopulationEV(document);
			} else
				verificationPage.fillEVerification(document);
			verificationPage.logout();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for ElectronicVerifcation");
			verificationPage.logout();
		}
	}

	

	@Test(testName = "Onboarding an Asic user", description = "Complete Onboarding an Asic user")
	public void CompleteOnboardingAsic(Method method) {
		try {
			util.pageWait();
			ExtentReportTestCaseName(method);
			completeRegistration("Australia");
			verificationPage.fillName("Australia");
			verificationPage.fillDOBPersonaldetails("Australia");
			verificationPage.fillAddressDetails("Australia");
			verificationPage.fillSecretQuestionPersonalDetails("Australia");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingPreferences("AsicEmploymentAndIncomePass");
			verificationPage.FillemploymentandIncomeAsicFca("AsicEmploymentAndIncomePass");
			verificationPage.declaration();
			Thread.sleep(2000);
			verificationPage.sikuliDocumentUpload();
			verificationPage.verifyDocumentUploadedandUserSubmitted();
			verificationPage.logout();
		} catch (InterruptedException e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}

	@Test(groups = {
			"translations" }, testName = "Translations on verification Page with different languages", description = "Translations for verification Page Page with different languages")
	public void zTranslationsWithDifferentLanguages(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			homePage.login("Translations");
			verificationPage.translationsWithDifferentLanguages();
			log.info("Validating ElectronicVerifcation");

		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			log.fail("Validation failed for ElectronicVerifcation");
			verificationPage.logout();
		}
	}
	
	@Test(groups = {"SecureGamma" }, testName = "Validate banned countries for IB", description = "Validate banned countries for IB")
	public void validateBannedCountriesForIB(Method method) throws InterruptedException {
		try {
			ExtentReportTestCaseName(method);
			log.info("Validate banned countries for IB ");
			ExtentReportGeneration.test.log(Status.INFO, "Validate banned countries for IB ");
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			verificationPage.validateIBBannedCountries("BannedCountries");
		} catch (InterruptedException e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
	}
	
	public String completeRegistration(String country) {
		String emailID = null;
		try {
			util.pageWait();
			Thread.sleep(2000);
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration(country);
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal",
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
		} catch (InterruptedException e) {
			ExtentReportGeneration.test.fail(e);
			log.fail("exception : " + e.getMessage());
			verificationPage.logout();
		}
		return emailID;
	}

	@DataProvider(name = "country")
	public static Object[][] countries() {
		return new Object[][] { { "SouthAfrica" }, { "Brazil" }, { "UK" }, { "Spain" } };
		/*
		 * { "UK" }, { "Brazil" }, { "Spain" },{"SouthAfrica"}, { "Italy" }, {
		 * "Norway" }, { "Poland" }, { "Portugal" }, { "Greece" }, { "Iceland"
		 * }, { "Malta" }, { "Czechia" }, { "Netherlands" }, { "Sweden" }, {
		 * "Slovakia" }, { "Latvia" }, { "Cyprus" }, { "Estonia" }, { "Belgium"
		 * }
		 */
	}

	@DataProvider(name = "countryNegative")
	public static Object[][] countriesNegative() {
		return new Object[][] { { "SouthAfrica-Invalid" }, { "UK-Invalid" } };
		// {"Lichtenstein-Invalid"}
	}

	@DataProvider(name = "leverage")
	public static Object[][] leverage() {
		return new Object[][] { {"China"} };
		//{ "UnitedArabEmirates" }, {"Lichtenstein-Invalid"} , { "UK" }
	}

	@DataProvider(name = "over18")
	public static Object[][] over18() {
		return new Object[][] { { "Given age Above18" }, { "Given age Below18" }, { "given age 18" } };
	}

	@DataProvider(name = "EmploymentAndIncome")
	public static Object[][] employmentAndIncome() {
		return new Object[][] { { "EmploymentAndIncome-Positive" }, { "EmploymentAndIncome-RequiredMsg" },
				{ "EmploymentAndIncome-WarningMsg" } };

	}

	@DataProvider(name = "license")
	public static Object[][] license() {
		return new Object[][] { { "AsicLowestIncome" }, { "FCALowestIncome" } };

	}

	@DataProvider(name = "DFSAAppropriatenessKnowledgeTest")
	public static Object[][] DFSAAppropriatenessKnowledgeTest() {
		return new Object[][] { { "PassWithAppropriatenessTest" }, { "PassWithKnowledgeTest1" },
				{ "PassWithKnowledgeTest2" } };
	}

	@DataProvider(name = "DFSAAppropriatenessKnowledgeTestNegative")
	public static Object[][] DFSAAppropriatenessKnowledgeTestNegative() {
		return new Object[][] { { "AppropriatenessTest1_OTCPurposeOfTradingWarningMsg" },
				{ "AppropriatenessTest1_AllNever" }, { "FailWithKnowledgeTest2" } };
	}

	@DataProvider(name = "ElectronicVerifcation")
	public static Object[][] eVerification() {
		return new Object[][] { { "AustralianPassport" }, { "ForeignPassport" }, { "DriverLicence" },
				{ "MandatoryFieldValidation" }, { "InvalidDataValidation" } };
	}

	@DataProvider(name = "TradingPreference")
	public static Object[][] tradingPreference() {
		return new Object[][] { { "MT5_STD", "Australia" }, { "MT5_Razor", "Australia" },
				{ "MT5_MngAccount", "Australia" }, { "MT4_STD", "Australia" }, { "MT4_Razor", "Australia" },
				{ "MT4_MngAccount", "Australia" }, { "cTrader_STD", "Australia" }, { "cTrader_Razor", "Australia" } };
	}

	@DataProvider(name = "TradingPreference_platform")
	public static Object[][] tradingPreference_platform() {
		return new Object[][] { { "Australia" }, { "UK" }, { "UnitedArabEmirates" } };
	}
}
