package com.tests;

import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.pageDefinitions.Onboarding;
import com.utils.ConfigFileReader;
import com.utils.ExtentReportGeneration;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class OnboardingTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;

	TestLogger log = new TestLogger();
	// JsonReader jr = new JsonReader();
	UtilityLibrary util = new UtilityLibrary();
	ConfigFileReader configFileReader = new ConfigFileReader();
	ExtentReportGeneration extreport = new ExtentReportGeneration();

	@Test(groups = { "functest", "Onboarding" }, testName = "Onboarding Login",
	 priority = 0)
	 public void verifyOnboardingLogin_CorrectLoginCredentials(Method method) {
	 try {
	 ExtentReportTestCaseName(method);
	 if (environment.equalsIgnoreCase("Gamma")) {
	 onboarding.login("Onboardinglogin");
	 }
	 Thread.sleep(6000);
	 homePage.validateURL(util.environmentURl() +
	 "/#/pre-qualify?section=services",
	 "Verify Login with Correct username and correct Password");
	 ExtentReportGeneration.test.log(Status.PASS, "Successfully logged in with correct credentials");
	 onboarding.logout();
	 } catch (Exception e) {
	 onboarding.logout();
	 log.fail("exception : " + e.getMessage());
	 ExtentReportGeneration.test.fail(e.getMessage());
	 ExtentReportGeneration.test.log(Status.FAIL, "Failed to login with correct credentials");
	 }
	 }

	@Test(groups = { "functest", "Onboarding" }, testName = "Verify Forgot Password in Login Page", priority = 0)

	public void verifyForgotPwd(Method method) {
		try {
			ExtentReportTestCaseName(method);
			onboarding.clickOnForgotPwd(util.environmentURl() + "/#/reset-password");
			ExtentReportGeneration.test.log(Status.INFO, "Clicked on Forgot Password");
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.fail(e.getMessage());
		}
	}

	@Test(groups = { "functest", "Onboarding" }
	,testName="Verify LoginHereLinkin Registration page", priority = 0)

	public void verifyLoginHereLink(Method method) {
	 try {
	 ExtentReportTestCaseName(method);
	 onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
	 util.jsClick(onboarding.getLoginHereLink());
	 ExtentReportGeneration.test.log(Status.INFO, "Clicked on LoginHere link");
	 util.validateURL("https://secure.pepperstone.com/login", "Verify LoginHere link landed on correct page");
	 ExtentReportGeneration.test.log(Status.INFO, "LoginHere link landed on correct page");
	 } catch (Exception e) {
	 onboarding.logout();
	 log.fail("exception : " + e.getMessage());
	 ExtentReportGeneration.test.fail(e.getMessage());
	 }
	
	 }

	@Test(groups = { "functest", "Onboarding" }, testName = "OnboardingRegistration", priority = 1)
	public void verifyOnboardingRegistration(Method method) {
		try {
			ExtentReportTestCaseName(method);
			onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
			onboarding.onboardingRegistration("OnboardingRegistration");
			Thread.sleep(5000);
			homePage.validateURL(util.environmentURl() + "/#/pre-qualify?section=services", "Verify Registration");
			ExtentReportGeneration.test.log(Status.PASS, "Registered Successfully ");
			onboarding.logout();
		} catch (Exception e) {
			onboarding.logout();
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.fail(e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to register");
		}
	}

	@Test(groups = { "functest",
	 "Onboarding" }, dataProvider = "negativeScenarios", testName = "Onboarding Registration", priority = 1)
	 public void verifyOnboardingRegistration_NegativeScenarios(Method method,
	 String registration) {
	 try {
	 ExtentReportTestCaseName(method, registration);
	 onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
	 onboarding.onboardingRegistration(registration);
	 Thread.sleep(5000);
	 homePage.validateURL(util.environmentURl() + "/#/register", "Verify Registration");
	 ExtentReportGeneration.test.log(Status.PASS, "Validated the URL after registration ");
	 } catch (Exception e) {
	 onboarding.logout();
	 log.fail("exception : " + e.getMessage());
	 ExtentReportGeneration.test.fail(e.getMessage());
	 }
	 }

	@Test(groups = { "functest", "Onboarding" }, testName = "Validate About YourBusiness", priority = 2)
	public void validateAboutYourBusiness(Method method) {
		try {
			ExtentReportTestCaseName(method);
			onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
			onboarding.onboardingRegistration("OnboardingRegistration");
			Thread.sleep(6000);
			homePage.validateURL(util.environmentURl() + "/#/pre-qualify?section=services", "Verify Registration");
			ExtentReportGeneration.test.log(Status.PASS, "Registered Successfully ");
			onboarding.businessInformationValidation();
			onboarding.logout();
		} catch (Exception e) {
			onboarding.logout();
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.fail(e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to register");
		}
	}

	@Test(groups = { "functest", "Onboarding" }, testName = "Validate Personal Details", priority = 3)
	 public void validatePersonalDetails(Method method) {
	 try {
	 ExtentReportTestCaseName(method);
	 onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
	 onboarding.onboardingRegistration("OnboardingRegistration");
	 Thread.sleep(6000);
	 homePage.validateURL(util.environmentURl() +
	 "/#/pre-qualify?section=services", "Verify Registration ");
	 ExtentReportGeneration.test.log(Status.PASS, "Registered Successfully ");
	 onboarding.fillBusinessInformation("PersonalInformation");
	 util.jsClick(onboarding.getNxtBtn());
	 Thread.sleep(1000);
	 homePage.validateURL(util.environmentURl() + "/#/profile", "Verify About your Business submission ");
	 ExtentReportGeneration.test.log(Status.PASS, "Verified About your Business submission ");
	 onboarding.fillpersonalDetails("PersonalInformation");
	 util.jsClick(onboarding.getNxtBtn());
	 Thread.sleep(10000);
	 homePage.validateURL(util.environmentURl() + "/#/id-manual", "Verify PersonalInformation submission ");
	 ExtentReportGeneration.test.log(Status.PASS, "Verified PersonalInformation submission");
	 onboarding.logout();
	 } catch (Exception e) {
	 onboarding.logout();
	 log.fail("exception : " + e.getMessage());
	 ExtentReportGeneration.test.fail(e.getMessage());
	 ExtentReportGeneration.test.log(Status.FAIL, "Failed to register");
	 }
	 }

	@Test(groups = { "functest",
			"Onboarding" }, dataProvider = "over18", testName = "Validate Date of Birth", priority = 3)
	public void validateDateOfBirth(Method method, String dob) {
		try {
			ExtentReportTestCaseName(method, dob);
			onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
			onboarding.onboardingRegistration("OnboardingRegistration");
			Thread.sleep(6000);
			homePage.validateURL(util.environmentURl() + "/#/pre-qualify?section=services", "Verify Registration ");
			ExtentReportGeneration.test.log(Status.PASS, "Registered Successfully ");
			onboarding.fillBusinessInformation("PersonalInformation");
			util.jsClick(onboarding.getNxtBtn());
			Thread.sleep(1000);
			homePage.validateURL(util.environmentURl() + "/#/profile", "Verify About your Business submission ");
			ExtentReportGeneration.test.log(Status.PASS, "Verified About your Business submission ");
			verificationPage.getDOB(dob);
//			String day = String.format("%02d", verificationPage.getDy());
//			String month = String.format("%02d", verificationPage.getMon());
			String day = String.format("%d", verificationPage.getDy());
			String month = String.format("%d", verificationPage.getMon());
			ExtentReportGeneration.test.log(Status.INFO,
					"DOB entered " + verificationPage.getYr() + "-" + month + "-" + day);
			util.getDropDownByVisibleText(onboarding.dayDOB, day);
			util.getDropDownByVisibleText(onboarding.monthDOB, month);
			util.getDropDownByVisibleText(onboarding.yearDOB, Integer.toString(verificationPage.getYr()));
			onboarding.fillAddressAndContactDetails("PersonalInformation");
			util.jsClick(onboarding.getNxtBtn());

			if (dob.contains("Below18")) {
				Thread.sleep(1000);
				Assert.assertTrue(onboarding.DOBErrorMsg.isDisplayed(),
						"Age is below eighteen so application did not proceed to Identity Verification page");
			} else {
				Thread.sleep(10000);
				homePage.validateURL(util.environmentURl() + "/#/id-manual", "Verify PersonalInformation submission ");
				ExtentReportGeneration.test.log(Status.PASS, "Verified PersonalInformation submission");
			}
			onboarding.logout();
		} catch (Exception e) {
			onboarding.logout();
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.fail(e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to validate DOB for 18 years");
		}
	}

	@Test(groups = { "functest", "Onboarding" }, testName = "Validate Identity Verification", priority = 4)
	 public void validateIdentityVerification(Method method) {
	 try {
	 ExtentReportTestCaseName(method);
	 onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
	 onboarding.onboardingRegistration("OnboardingRegistration");
	 Thread.sleep(6000);
	 homePage.validateURL(util.environmentURl() +
	 "/#/pre-qualify?section=services", "Verify Registration ");
	 ExtentReportGeneration.test.log(Status.PASS, "Registered Successfully ");
	 onboarding.fillBusinessInformation("IdentityVerification");
	 util.jsClick(onboarding.getNxtBtn());
	 Thread.sleep(1000);
	 homePage.validateURL(util.environmentURl() + "/#/profile", "Verify About your Business submission ");
	 ExtentReportGeneration.test.log(Status.PASS, "Verified About your Business submission ");
	 onboarding.fillpersonalDetails("IdentityVerification");
	 util.jsClick(onboarding.getNxtBtn());
	 Thread.sleep(10000);
	 homePage.validateURL(util.environmentURl() + "/#/id-manual", "Verify PersonalInformation submission ");
	 ExtentReportGeneration.test.log(Status.PASS, "Verified PersonalInformation submission");
	 onboarding.fillIdentityVerification("IdentityVerification",2);
	 onboarding.submitBtn.click();
	 } catch (Exception e) {
	 onboarding.logout();
	 log.fail("exception : " + e.getMessage());
	 ExtentReportGeneration.test.fail(e.getMessage());
	 ExtentReportGeneration.test.log(Status.FAIL, "Failed to register");
	 }
	 }

//	@Test(groups = { "functest", "Onboarding" }, testName = "Validate Identity Verification Negative Scenario", priority = 4)
//	 public void validateIdentityVerificationNegativeScenario(Method method) {
//	 try {
//	 ExtentReportTestCaseName(method);
//	 onboarding.clickOnboardingSignUpLink(util.environmentURl() + "/#/register");
//	 onboarding.onboardingRegistration("OnboardingRegistration");
//	 Thread.sleep(6000);
//	 homePage.validateURL(util.environmentURl() +
//	 "/#/pre-qualify?section=services", "Verify Registration ");
//	 ExtentReportGeneration.test.log(Status.PASS, "Registered Successfully ");
//	 onboarding.fillBusinessInformation("IdentityVerification");
//	 util.jsClick(onboarding.getNxtBtn());
//	 Thread.sleep(1000);
//	 homePage.validateURL(util.environmentURl() + "/#/profile", "Verify About your Business submission ");
//	 ExtentReportGeneration.test.log(Status.PASS, "Verified About your Business submission ");
//	 onboarding.fillpersonalDetails("IdentityVerification");
//	 util.jsClick(onboarding.getNxtBtn());
//	 Thread.sleep(10000);
//	 homePage.validateURL(util.environmentURl() + "/#/id-manual", "Verify PersonalInformation submission ");
//	 ExtentReportGeneration.test.log(Status.PASS, "Verified PersonalInformation submission");
//	 int numOfDocumnets=3;
//	 for(int i=0;i<=numOfDocumnets;i++) {
//	 onboarding.fillIdentityVerification("IdentityVerification",i);
//	 System.out.println("IdentityVerification is completed");
//	 if(i<2) {
//		 boolean button=onboarding.submitBtn.isEnabled();
//		 System.out.println("Button is disabled++"+button);
//		Assert.assertFalse(onboarding.submitBtn.isEnabled(),"Submit button is enabled for less than 2 documents");
//		System.out.println("11111111111111111++"+button);
//	 }
//	 else {
//		 Assert.assertTrue(onboarding.submitBtn.isEnabled(),"Submit button is disabled for more than 2 documents");
//	 System.out.println("Button is enabled++"+onboarding.submitBtn.isEnabled());
//	 }}
//	 onboarding.logout();
//	 } catch (Exception e) {
//	 onboarding.logout();
//	 log.fail("exception : " + e.getMessage());
//	 ExtentReportGeneration.test.fail(e.getMessage());
//	 ExtentReportGeneration.test.log(Status.FAIL, "Failed in Identity verifictaion Negative scenarios");
//	 }
//	 }
	
	@DataProvider(name = "negativeScenarios")
	public static Object[][] negativeScenarios() {
		return new Object[][] { { "WithoutName" }, { "InvalidCompany" }, { "WeakPassword" },
				{ "NilEmailAndPassword" } };
	}

	@DataProvider(name = "over18")
	public static Object[][] over18() {
		return new Object[][] { { "Given age Above18" }, { "Given age Below18" }, { "given age 18" } };
	}
}
