package com.tests;

import java.lang.reflect.Method;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.utils.ConfigFileReader;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

@Listeners(com.utils.TestListener.class)

public class AdminTest extends TestConfig {

	static ExtentTest test;
	static ExtentReports report;
	static ExtentHtmlReporter htmlReporter;
	//VerificationPageTest verificationpage = new VerificationPageTest();

	TestLogger log = new TestLogger();
	//JsonReader jr = new JsonReader();
	UtilityLibrary util = new UtilityLibrary();
	ConfigFileReader configFileReader = new ConfigFileReader();
	ExtentReportGeneration extreport = new ExtentReportGeneration();

	@Test(groups = { "admin" }, testName = "getuserIDfromDB")
	public void getuserIDfromDB(Method method) {
		String emailID = null;
		try {
			util.pageWait();
			Thread.sleep(2000);
			ExtentReportTestCaseName(method);
			verificationPage.ValidateExistingPageIsLoginPage();
			homePage.clickSignUpLink(util.environmentURl() + "/register");
			Thread.sleep(2000);
			emailID = registrationPage.Registration("Australia");
			ExtentReportGeneration.test.log(Status.INFO, "Registration completed");
			util.waitForPageLoaded();
			Thread.sleep(15000);
			registrationPage.validateURL(util.environmentURl() + "/verification/individual/personal",
					"Verify Registration");
			ExtentReportGeneration.test.log(Status.INFO, "Validated the URL after registration");
			verificationPage.fillName("Australia");
			verificationPage.fillDOBPersonaldetails("Australia");
			verificationPage.fillAddressDetails("Australia");
			verificationPage.fillSecretQuestionPersonalDetails("Australia");
			verificationPage.navigateToNextTab();
			verificationPage.fillTradingPreferences("AsicEmploymentAndIncomePass");
			verificationPage.FillemploymentandIncomeAsicFca("AsicEmploymentAndIncomePass");
			verificationPage.declaration();
			Thread.sleep(2000);
			/*verificationPage.sikuliDocumentUpload();
			verificationPage.verifyDocumentUploadedandUserSubmitted();*/
			String userID = util.getuserIDfromDatabase(emailID);
			ExtentReportGeneration.test.log(Status.INFO, "user id is " + userID);
		} catch (Exception e) {
			log.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "");
			homePage.logout();
		}
	}

	
}
