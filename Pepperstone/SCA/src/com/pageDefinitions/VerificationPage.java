package com.pageDefinitions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import com.aventstack.extentreports.*;
import com.gargoylesoftware.htmlunit.javascript.host.worker.Worker;
import com.tests.VerificationPageTest;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class VerificationPage extends UtilityLibrary {
	static ExtentTest test;
	static ExtentReports report;
	public int yr, mon, dy;
	public TestLogger logger = new TestLogger();
	UtilityLibrary util = new UtilityLibrary();
	String fs=File.separator;
	 static String path = System.getProperty("user.dir");
	 VerificationPageTest verificationPageTest = new VerificationPageTest();
	 
	@FindBy(how = How.NAME, using = "firstName")
	WebElement firstName;

	@FindBy(how = How.NAME, using = "lastName")
	WebElement lastName;

	@FindBy(how = How.NAME, using = "day")
	public WebElement day;

	@FindBy(how = How.NAME, using = "month")
	public WebElement month;

	@FindBy(how = How.NAME, using = "year")
	public WebElement year;

	@FindBy(how = How.NAME, using = "country")
	WebElement country;

	@FindBy(how = How.NAME, using = "address")
	WebElement address;

	@FindBy(how = How.NAME, using = "address2")
	WebElement address2;

	@FindBy(how = How.NAME, using = "city")
	WebElement city;

	@FindBy(how = How.NAME, using = "zipCode")
	WebElement zipCode;

	@FindBy(how = How.NAME, using = "state")
	WebElement state;

	@FindBy(how = How.NAME, using = "nationality")
	WebElement nationality;

	@FindBy(how = How.NAME, using = "nationalId")
	WebElement nationalId;

	@FindBy(how = How.NAME, using = "secretQuestion")
	WebElement secretQuestion;

	@FindBy(how = How.NAME, using = "secretAnswer")
	WebElement secretAnswer;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-input']//input")
	WebElement phone;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement next;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'username')]")
	WebElement emailAddress;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'password')]")
	WebElement password;
	
	@FindBy(how = How.XPATH, using = "//button/span[text()='register']")
	WebElement btnRegister;
	
	@FindBy(how = How.NAME, using = "legalEntity")
	WebElement accountType;

	@FindBy(how = How.NAME, using = "location")
	WebElement operatingCountryIBprogram;

	@FindBy(how = How.NAME, using = "clientsLocation")
	WebElement clientRegion;

	@FindBy(how = How.NAME, using = "ibType")
	WebElement IBprogramType;

	@FindBy(how = How.NAME, using = "hasLicence")
	WebElement regulatoryLicense;

	@FindBy(how = How.NAME, using = "programDetails")
	WebElement programDetailsTextArea1;

	@FindBy(how = How.NAME, using = "programAdvertisingDetails")
	WebElement programAdvertisingDetailsTextArea2;

	@FindBy(how = How.NAME, using = "communicationFrequency")
	WebElement communicationFrequency;

	@FindBy(how = How.NAME, using = "companyType")
	WebElement individualOrCompany;

	@FindBy(how = How.NAME, using = "rebateCurrency")
	WebElement rebateCurrency;

	@FindBy(how = How.NAME, using = "referredClientNumber")
	WebElement referredClientNumber;

	@FindBy(how = How.NAME, using = "hasOtherCollaboration")
	WebElement workingWithOtherBrokers;

	@FindBy(how = How.NAME, using = "expectedMonthlyReferredClientNumber")
	WebElement expectedMonthlyReferredClientNumber;

	@FindBy(how = How.NAME, using = "averageMonthlyClientTradingVolume")
	WebElement averageMonthlyClientTradingVolume;

	@FindBy(how = How.NAME, using = "totalClientDeposits")
	WebElement totalClientDeposits;

	@FindBy(how = How.NAME, using = "agreeTermsAndConditions")
	WebElement agreeTermsAndConditions;

	@FindBy(how = How.NAME, using = "agreeAccuracyVerification")
	WebElement agreeAccuracyVerification;

	@FindBy(how = How.ID, using = "myProfileMenuItem")
	WebElement myProfileMenuItem;

	@FindBy(how = How.XPATH, using = "//span[@id='myProfileMenuItem']//span[text()='log out']")
	WebElement logout;

	@FindBy(how = How.XPATH, using = "//span[text()='submit']")
	WebElement submit;

	@FindBy(how = How.XPATH, using = "//button/span[text()='submit']")
	WebElement warningSubmit;

	@FindBy(how = How.XPATH, using = "//p[text()='Please enter a valid national ID']")
	WebElement NationalIderror;

	@FindBy(how = How.NAME, using = "title")
	WebElement title;

	@FindBy(how = How.XPATH, using = "//span[text()='modify']")
	WebElement modify;

	@FindBy(how = How.XPATH, using = "//label[text()='Select leverage']")
	WebElement leverage;

	@FindBy(how = How.XPATH, using = "//label[text()='Account Type']/..//div[text()='Razor']")
	WebElement razorAccountType;

	@FindBy(how = How.XPATH, using = "//label[text()='Account Type']/..//div[text()='Standard']")
	WebElement standardAccountType;

	@FindBy(how = How.XPATH, using = "//label[text()='Account Type']/..//div[text()='Managed Account']")
	WebElement managedAccountType;

	@FindBy(how = How.XPATH, using = "//label[text()='Date of Birth']/ancestor::div/following-sibling::p[text()='You must be 18 or over to open an account.']")
	public WebElement dobErrorMsg;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'expanded')]/span[text()='Trading Preferences']")
	public WebElement tradingPreferencesExpanded;

	@FindBy(how = How.XPATH, using = "//div[text()='cTrader']")
	WebElement cTrader;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Annual income']/..//div[contains(text(),'Less than')]")
	WebElement annualIncomeLessThan;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Annual income']/..//div[contains(text(),'$500,000 +')]")
	WebElement annualIncome$500000;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Source of Funds']/..//div[contains(text(),'Loans')]")
	WebElement sourceOfFundsLoan;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Source of Funds']/..//div[contains(text(),'Inheritance')]")
	WebElement sourceOfFundsInheritance;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Employment Status']/..//div[contains(text(),'Entrepreneur')]")
	WebElement employmentStatusEntrepreneur;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Employment Status']/..//div[contains(text(),'Unemployed')]")
	WebElement employmentStatusUnemployed;

	@FindBy(how = How.NAME, using = "netAssets")
	WebElement netAssets;

	@FindBy(how = How.NAME, using = "occupation")
	WebElement occupation;

	@FindBy(how = How.NAME, using = "industry")
	WebElement industry;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Annual income']/..//p[contains(@role,'alert')]")
	WebElement annualIncomeAlert;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Source of Funds']/..//p[contains(@role,'alert')]")
	WebElement sourceOfFundsAlert;

	@FindBy(how = How.XPATH, using = "//div/label[text()='Employment Status']/..//p[contains(@role,'alert')]")
	WebElement employmentStatusAlert;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'netAssets')]/parent::div/following-sibling::p[(text()='This field is required.')]")
	WebElement netAssetsAlert;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'occupation')]/parent::div/following-sibling::p[(text()='This field is required.')]")
	WebElement occupationAlert;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'industry')]/parent::div/following-sibling::p[(text()='This field is required.')]")
	WebElement industryAlert;

	@FindAll(@FindBy(xpath = "//select[@name='nationality']"))
	List<WebElement> nationalityFieldOnPage;

	@FindAll(@FindBy(xpath = "//span[text()='submit']"))
	List<WebElement> submitpopup;

	@FindAll(@FindBy(xpath = "//select[@name='title']"))
	List<WebElement> titleField;

	@FindAll(@FindBy(xpath = "//label[text()='Account Type']/..//div[text()='Managed Account']"))
	List<WebElement> managedAccount;

	@FindAll(@FindBy(xpath = "//label[text()='Select leverage']"))
	List<WebElement> leverageOptions;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Other')]")
	WebElement other;

	//@FindBy(how = How.XPATH, using = "//*[contains(text(),'FUNDS?')]/../div")
	@FindBy(how = How.XPATH, using = "//div[@class='chip-choice chip-choice--required'][1]")
	WebElement funds;

	//@FindBy(how = How.XPATH, using = "//label[contains(text(),'INDUSTRY')]/../div")
	@FindBy(how = How.XPATH, using = "//div[@class='chip-choice chip-choice--required'][2]")
	WebElement whichIndustry;

	//@FindBy(how = How.XPATH, using = "//*[contains(text(),'POSITION DO YOU HOLD?')]/../div")
	@FindBy(how = How.XPATH, using = "//div[@class='chip-choice chip-choice--required'][3]")
	WebElement positionHold;

	//@FindBy(how = How.XPATH, using = "//*[contains(text(),'ANNUAL SALARY?')]/../div")
	@FindBy(how = How.XPATH, using = "//div[@class='chip-choice chip-choice--required'][4]")
	WebElement annualSalary;

	//@FindBy(how = How.XPATH, using = "//*[contains(text(),'SAVINGS AND INVESTMENTS')]/../div")
	@FindBy(how = How.XPATH, using = "//div[@class='chip-choice chip-choice--required'][5]")
	WebElement savingsAndInvestments;

	//@FindBy(how = How.XPATH, using = "//*[contains(text(),'EXPERIENCE')]/../div")
	@FindBy(how = How.XPATH, using = "//div[@class='chip-choice chip-choice--required'][6]")
	WebElement experience;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'annual salary')]/../p")
	WebElement annualsalaryAlert;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'platform')]/../div")
	WebElement selectPlatformType;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Appropriateness Test')]/parent::button")
	WebElement appropriatenessTestButton;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Bonds and Shares')]/../div")
	WebElement bondsAndShares;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Exchange Traded Derivatives (Warrants, Futures, Options and Includes Forex)')]/../div")
	WebElement exchangeTraded;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'OTC Derivatives')]/../div")
	WebElement OTCDerivatives;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Declaration')]")
	WebElement declarationPage;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'ID Verification')]")
	WebElement idverification;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	WebElement agree;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'TRUE')]")
	WebElement trueBttn;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'FALSE')]")
	WebElement falseBttn;

	@FindBy(how = How.XPATH, using = "//p[contains(text(),'Congratulations')]")
	WebElement quizPass;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Upload Your Documents')]")
	WebElement uploadDocuments;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'previous')]/..")
	WebElement previous;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'next')]/..")
	WebElement nextbtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Unsuccessful']")
	WebElement unSuccessful;

	@FindBy(how = How.XPATH, using = "//span[text()='close']")
	WebElement closeBtn;

	@FindBy(how = How.XPATH, using = "//button[text()='close']")
	WebElement closeAlert;

	@FindBy(how = How.XPATH, using = "//p[contains(text(),'important to trade with capital you can afford to')]")
	WebElement purposeOfTradingWarning;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Experience of Qualifications')]")
	WebElement experienceOfQualification;

	@FindBy(how = How.XPATH, using = "//div[@class='item'][1]")
	WebElement autoSelectAddress;

	@FindBy(how = How.XPATH, using = "//div[text()='Use this address']")
	WebElement useThisAddress;

	@FindAll(@FindBy(css = ".item"))
	List<WebElement> autoSelectAddressList;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Have you previously traded CFD')]/parent::div//div[text()='Yes']")
	WebElement haveYouTradedYES;

	@FindBy(how = How.NAME, using = "confirm")
	WebElement iConfirmCheckbox;

	@FindBy(how = How.XPATH, using = "//div[text()='Passport']")
	WebElement passportEV;

	@FindBy(how = How.XPATH, using = "//select[@class='mdc-select__native-control']")
	WebElement passportDropDown;

	@FindBy(how = How.NAME, using = "greenidPassportdvsNumber")
	WebElement passportNumber;

	@FindBy(how = How.NAME, using = "greenidVisadvsPassportNumber")
	WebElement visaPassportNumber;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Australian Driver')]")
	WebElement australianDriversLicence;

	@FindBy(how = How.XPATH, using = "//select[@class='mdc-select__native-control']")
	WebElement australianDriversLicenceDropDown;

	@FindBy(how = How.NAME, using = "greenidActregodvsNumber")
	WebElement drivingLicenseNumber;

	@FindBy(how = How.XPATH, using = "//p[text()='This field is required.']")
	WebElement licenseNumberReuiredMsg;

	@FindBy(how = How.XPATH, using = "//span[text()='verify my details']")
	WebElement verifyMyDetails;

	@FindBy(how = How.XPATH, using = "//div[text()='Australian Electoral Roll']")
	WebElement australianElectoralRoll;

	@FindBy(how = How.NAME, using = "greenidAecStreetnumber")
	WebElement streetNumber;

	@FindBy(how = How.NAME, using = "greenidVisadvsCountryOfIssue")
	WebElement countryOfIssue;

	@FindBy(how = How.XPATH, using = "//a[text()=' in progress']")
	WebElement inProgressStatus;

	@FindBy(how = How.NAME, using = "greenidAecStreetname")
	WebElement streetNameEV;

	@FindBy(how = How.NAME, using = "greenidAecSuburb")
	WebElement subUrbEV;

	@FindBy(how = How.NAME, using = "greenidAecState")
	WebElement stateEV;

	@FindBy(how = How.NAME, using = "greenidAecPostcode")
	WebElement postCodeEV;

	// @FindBy(how = How.XPATH, using = "//h4[contains(text(),'Unfortunately, we
	// could not verify your')]")
	// WebElement notVerifiedStatusPP;

	@FindBy(how = How.XPATH, using = "//span[text()='This value is not valid.']")
	WebElement invalidPP_LicenceNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='Share CFD trading is only available on MT5']")
	WebElement msgInTradingPreference;

	@FindBy(how = How.XPATH, using = "//span[text()='(optional)']")
	WebElement refferedByOptional;

	@FindBy(how = How.NAME, using = "currency")
	WebElement currencyDropDown;

	@FindBy(how = How.NAME, using = "password")
	WebElement tradingAccountPassword;

	@FindBy(how = How.NAME, using = "confirmPassword")
	WebElement confirmPassword;

	@FindBy(how = How.NAME, using = "userEnteredMamToken")
	WebElement managedAccountToken;

	@FindBy(how = How.XPATH, using = "//div[text()='Swap-free']")
	WebElement swapFreeAccount;

	@FindAll(@FindBy(css = ".panel__header"))
	List<WebElement> pannelHeader;

	@FindBy(how = How.CSS, using = ".change-language-icon")
	WebElement changeLanguage;

	@FindAll(@FindBy(css = ".change-language li"))
	List<WebElement> languages;

	@FindBy(how = How.XPATH, using = "//header/h1")
	WebElement header;

	@FindAll(@FindBy(css = ".dialog-component > div >footer>button"))
	List<WebElement> warningCountrySelection;

	@FindBy(how = How.XPATH, using = "//a[@href='/verification/individual/id/manual']")
	WebElement manualVerification;

	@FindBy(how = How.CSS, using = ".mv-file-input__button")
	WebElement attach;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Yes')]")
	WebElement previouslyTraded;

	@FindBy(how = How.CSS, using = ".manual-verification__uploaded-docs-img-wrapper img")
	WebElement uploadedDocument;

	@FindBy(how = How.XPATH, using = "//span[text()='Congratulations']")
	WebElement onboardingConfirm;
	
	@FindBy(how = How.CSS, using = "[name='location']")
	WebElement IBLocation;
	
	@FindBy(how = How.CSS, using = "[name='clientsLocation']")
	WebElement clientsLocation;
	
	@FindBy(how = How.XPATH, using = "//input[@name='chineseName']")
	WebElement chineseName;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'LEVERAGED PRODUCTS')]/../div")
	WebElement previouslyTradedLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='dynamic-fields-widget-confirm']")
	WebElement confirmCheckBox;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'HIGHER QUALIFICATIONS')]/../div")
	WebElement higherQualification;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Risk Management')]")
	WebElement riskManagement_SVG;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'No')]")
	WebElement riskManagement_No_SVG;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Professional Qualification(s)')]")
	WebElement professionalQualification_SVG;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'None of the above')]")
	WebElement professionalQualification_None_SVG;
		
	@FindBy(how = How.XPATH, using = "//span[text()='FX']/..//div[text()='0-6 months']")
	WebElement fXValue_TradingExperience_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='CFDs']/..//div[text()='0-6 months']")
	WebElement cfdsValue_TradingExperience_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Futures/Options']/..//div[text()='0-6 months']")
	WebElement futureValue_TradingExperience_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Shares']/..//div[text()='0-6 months']")
	WebElement sharesValue_TradingExperience_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='FX']/..//div[text()='Occasionally']")
	WebElement fXValue_TradingFrequency_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='CFDs']/..//div[text()='Occasionally']")
	WebElement cfdsValue_TradingFrequency_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Futures/Options']/..//div[text()='Occasionally']")
	WebElement futureValue_TradingFrequency_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Shares']/..//div[text()='Occasionally']")
	WebElement sharesValue_TradingFrequency_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='FX']/..//div[text()='1-9']")
	WebElement fXValue_TradingLots_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='CFDs']/..//div[text()='1-9']")
	WebElement cfdsValue_TradingLots_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Futures/Options']/..//div[text()='1-9']")
	WebElement futureValue_TradingLots_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Shares']/..//div[text()='1-9']")
	WebElement sharesValue_TradingLots_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='FX']/..//div[text()='Never']")
	WebElement fXValue_Options_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='CFDs']/..//div[text()='Never']")
	WebElement cfdsValue_Options_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Futures/Options']/..//div[text()='Never']")
	WebElement futureValue_Options_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Shares']/..//div[text()='Never']")
	WebElement sharesValue_Options_SVG;
	
	@FindBy(how = How.XPATH, using = "//button[@class='mdc-button ps-button ps-button--primary']/span")
	WebElement beginAppropriatenessTestButton;
	
	@FindBy(how = How.XPATH, using = "//span[text()='FX']/..//div[text()='None']")
	WebElement fXValue_TradingLots_None_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='CFDs']/..//div[text()='None']")
	WebElement cfdsValue_TradingLots_None_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Futures/Options']/..//div[text()='None']")
	WebElement futureValue_TradingLots_None_SVG;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Shares']/..//div[text()='None']")
	WebElement sharesValue_TradingLots_None_SVG;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-header__title']")
	WebElement question_Set2_SVG;
	
	@FindBy(how = How.XPATH, using = "//div[@class='question-header__progress']")
	WebElement paging_Set2_SVG;
	
	@FindBy(how = How.XPATH, using = "//div[@class='mdc-dialog__surface']//h1//span")
	WebElement unsuccesfulMessage_SVG;
	
	@FindBy(how = How.XPATH, using = "//button[@class='mdc-button ps-button ps-button--negative']")
	WebElement closeButton_SVG;
	
	@FindBy(how = How.XPATH, using = "//button[@class='mdc-button ps-button ps-button--negative']")
	WebElement negativeCloseButton_SVG;
	
	public WebElement getSelectPlatformType() {
		return selectPlatformType;
	}
	
	
	public WebElement getNegativeCloseButton_SVG() {
		return negativeCloseButton_SVG;
	}

	public WebElement getCloseButton_SVG() {
		return closeButton_SVG;
	}

	public WebElement getUnsuccesfulMessage_SVG() {
		return unsuccesfulMessage_SVG;
	}

	public WebElement getPaging_Set2_SVG() {
		return paging_Set2_SVG;
	}

	public WebElement getNext() {
		return next;
	}
	
	public WebElement getBeginAppropriatenessTestButton() {
		return beginAppropriatenessTestButton;
	}

	public WebElement getAppropriatenessTestButton() {
		return appropriatenessTestButton;
	}
		
	public WebElement getDeclarationPage() {
		return declarationPage;
	}

	public WebElement getConfirmCheckBox() {
		return confirmCheckBox;
	}

	public WebElement getConfirmPassword() {
		return confirmPassword;
	}
	
	public WebElement getTradingAccountPassword() {
		return tradingAccountPassword;
	}

	public WebElement getChineseName() {
		return chineseName;
	}

	public WebElement getInProgressStatus() {
		return inProgressStatus;
	}

	public WebElement getiConfirmCheckbox() {
		return iConfirmCheckbox;
	}

	public WebElement getHaveYouTradedYES() {
		return haveYouTradedYES;
	}

	public WebElement getUnSuccessful() {
		return unSuccessful;
	}

	public WebElement getPurposeOfTradingWarning() {
		return purposeOfTradingWarning;
	}

	public WebElement getExperienceOfQualification() {
		return experienceOfQualification;
	}

	public int getDy() {
		return dy;
	}

	public int getYr() {
		return yr;
	}

	public int getMon() {
		return mon;
	}

	JsonReader jr = new JsonReader("jsonFilePathverificationPage");

	public void fillPersonalDetails(String elementName) {
		try {
			fillCountryInPersonalDetails(elementName);
			clickSubmit();
			filltitle(elementName);
			fillName(elementName);
			fillDOBPersonaldetails(elementName);
			fillAddressDetails(elementName);
			fillNationalityPersonalDetails(elementName);
			fillSecretQuestionPersonalDetails(elementName);
			fillMobileNumber(elementName);
			navigateToNextTab();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to fill personal details tab");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void fillPersonalDetailsWithoutNationalityAndTitle(String elementName) {
		try {
			fillCountryInPersonalDetails(elementName);
			clickSubmit();
			fillName(elementName);
			fillDOBPersonaldetails(elementName);
			fillAddressDetails(elementName);
			fillSecretQuestionPersonalDetails(elementName);
			fillMobileNumber(elementName);
			navigateToNextTab();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to fill personal details tab");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	public void fillPersonalDetailsWithNationalID(String elementName) {
		try {
			fillName(elementName);
			fillDOBPersonaldetails(elementName);
			fillAddressDetails(elementName);
			fillNationalityPersonalDetails(elementName);
			fillSecretQuestionPersonalDetails(elementName);
			fillMobileNumber(elementName);
			navigateToNextTab();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to fill personal details tab");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	public void fillCountryInPersonalDetails(String elementName) {
		try {
			Thread.sleep(2000);
			getDropDownByVisibleText(country, jr.GetElementValue(elementName + ".Country"));
			logger.info("Sucessfully filled country in personal details");
			ExtentReportGeneration.test.info("Sucessfully filled country");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			ExtentReportGeneration.test.fail("Failed to fill country");
			logger.fail("Failed to fill country in personal details");
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void clickSubmit() {
		try {
			if (submitpopup.size() != 0) {
				submit.click();
				Thread.sleep(1000);
				logger.info("Submit button sucessfully clicked");
				ExtentReportGeneration.test.info("Submit button sucessfully clicked");
			}
		} catch (Exception e) {
			logger.fail("Failed to click on submit button");
			ExtentReportGeneration.test.fail("Failed to click Submit button");
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void filltitle(String elementName) {
		try {
			if (titleField.size() != 0) {
				getDropDownByVisibleText(title, jr.GetElementValue(elementName + ".Title"));
				logger.info("Sucessfully filled Title");
				ExtentReportGeneration.test.info("Sucessfully filled Title");
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to fill title");
			logger.fail("Failed to select title");
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillName(String elementName) {
		try {
			firstName.sendKeys(jr.GetElementValue(elementName + ".FirstName"));
			lastName.sendKeys(jr.GetElementValue(elementName + ".LastName"));
			logger.info("Sucessfully filled Firstname and Lastname");
			ExtentReportGeneration.test.info("Sucessfully filled Firstname and Lastname");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to fill LastName and Firstname");
			logger.fail("Failed to fill Firstname and Lastname");
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillMobileNumber(String elementName) {
		try {
			phone.sendKeys(jr.GetElementValue(elementName + ".Mobile"));
			Thread.sleep(1000);
			logger.info("Sucessfully filled Mobile Number");
			ExtentReportGeneration.test.info("Sucessfully filled Mobile Number");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to fill Mobile Number");
			logger.fail("Failed to fill Mobile Number");
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillDOBPersonaldetails(String elementName) {
		try {
			getDropDownByVisibleText(day, jr.GetElementValue(elementName + ".DOBDay"));
			getDropDownByVisibleText(month, jr.GetElementValue(elementName + ".DOBMonth"));
			getDropDownByVisibleText(year, jr.GetElementValue(elementName + ".DOBYear"));
			logger.info("Sucessfully filled DOB");
			ExtentReportGeneration.test.info("Sucessfully filled DOB");
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.fail("Failed to fill DOB");
		}
	}

	public void fillAddressDetails(String elementName) {
		try {
			address.sendKeys(jr.GetElementValue(elementName + ".Address"));
			Thread.sleep(3000);
			if (autoSelectAddressList.size() != 0) {
				if(autoSelectAddressList.get(0).getText().contains("Use this address")){
					ExtentReportGeneration.test.warning("Only 'Use this address' appears in dropdown, Experain/google dropdown is not working");
				}
				autoSelectAddressList.get(0).click();
				Thread.sleep(5000);
			}
			// tabFromElement(address);
			String sug1 = city.getAttribute("value");
			if (sug1.length() == 0)
				city.sendKeys(jr.GetElementValue(elementName + ".City"));
			String sug2 = zipCode.getAttribute("value");
			if (sug2.length() == 0)
				zipCode.sendKeys(jr.GetElementValue(elementName + ".Postcode"));
			getDropDownByVisibleText(state, jr.GetElementValue(elementName + ".State"));
			logger.info("Sucessfully filled address");
			ExtentReportGeneration.test.info("Sucessfully filled address");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to fill Address");
			logger.fail("Failed to fill address");
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillNationalityPersonalDetails(String elementName) {
		try {
			if (nationalityFieldOnPage.size() != 0) {
				getDropDownByVisibleText(nationality, jr.GetElementValue(elementName + ".Nationality"));
				nationalId.sendKeys(jr.GetElementValue(elementName + ".NationalID"));
				tabFromElement(nationalId);
				Thread.sleep(1000);
				logger.info("Sucessfully filled Nationality");
				ExtentReportGeneration.test.info("Sucessfully filled Nationality");
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to fill Nationality");
			logger.info("Failed to fill Nationality details");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void fillSecretQuestionPersonalDetails(String elementName) {
		try {
			scrollIntoView(secretQuestion);
			getDropDownByVisibleText(secretQuestion, jr.GetElementValue(elementName + ".SecretQuestion"));
			secretAnswer.sendKeys(jr.GetElementValue(elementName + ".SecretAnswer"));
			logger.info("Sucessfully filled secret Questionaries");
			ExtentReportGeneration.test.info("Sucessfully filled secret Questionaries");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to fill Secret question");
			logger.info("Failed to fill Secret Questions and Answer details");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void navigateToNextTab() {
		try {
			Thread.sleep(5000);
			next.click();
			logger.info("clicked on next button");
		} catch (Exception e) {
			logger.info("Next is not Enbled");
			logger.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.fail("Failed to click Next button");
		}
	}

	public void modifybttnclick() {
		try {
			modify.click();
		} catch (Exception e) {
			logger.info("Modify is not visisble");
			logger.fail("exception : " + e.getMessage());

		}
	}

	public void ValidatePersonalDetailSubmission() throws InterruptedException {
		try {
			waitForPageLoaded();
			Thread.sleep(10000);
			String expected = environmentURl() + "/verification/individual/trading-account-preferences";
			String actual = driver.getCurrentUrl();
			Assert.assertEquals(actual, expected);
		} catch (AssertionError e) {
			ExtentReportGeneration.test.fail("Did not move to trading-account-preference");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void ValidateNationalIDErrorMessage() {
		try {
			pageWait();
			if (NationalIderror.isDisplayed()) {
				logger.pass("NationalID error message is present");
			}
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void ValidateExistingPageIsLoginPage() {
		try {
			if (!driver.getCurrentUrl().contains("login") || driver.getCurrentUrl().contains("individual")) {
				logout();
			}
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void logout() {
		try {
			do {
				Thread.sleep(2000);
				myProfileMenuItem.click();
				Thread.sleep(2000);
				isDisplayed(logout, "logout button displayed");
				logout.click();
				Thread.sleep(5000);
			} while (driver.getCurrentUrl().contains("verification"));
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Logout failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Logout failed");
			logger.fail("exception is " + e);
		}
	}

	public void checkLeverageOptionDisabled(String country) {
		try {
				Assert.assertEquals(leverageOptions.size(), 0);
				logger.info("Leverage option is not available for " + country);
				ExtentReportGeneration.test.pass("Leverage option is not available for " + country);
			
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Leverage validation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Leverage validation failed");
			logger.fail("exception is " + e);
		}
	}

	public void checkAccountType(String elementName) {
		try {
			switch (jr.GetElementValue(elementName + ".accounttype")) {
			case "ctrader/Razor/Standard":
				ExtentReportGeneration.test.info("Only Razor and Standard account type expected");
				Assert.assertTrue(razorAccountType.isDisplayed());
				Assert.assertTrue(standardAccountType.isDisplayed());
				Assert.assertEquals(0, managedAccount.size());
				break;
			case "Metatrader 5/Metatrader 4/Razor/Standard/Managed":
				ExtentReportGeneration.test.info("Only Razor,Standard and Managed account type expected");
				Assert.assertTrue(razorAccountType.isDisplayed());
				Assert.assertTrue(standardAccountType.isDisplayed());
				Assert.assertTrue(managedAccountType.isDisplayed());
				break;
			default:
				ExtentReportGeneration.test.info("Only Razor,Standard and Managed account type expected");
				Assert.assertTrue(razorAccountType.isDisplayed());
				Assert.assertTrue(standardAccountType.isDisplayed());
				Assert.assertTrue(managedAccountType.isDisplayed());
				break;
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Account type validation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Account type validation failed");
			logger.fail("exception is " + e);
		}
	}

	public void checkAccountTypeBasedOnCountry(String platform, String country) {
		try {
			switch (platform) {
			case "ctrader":
				ExtentReportGeneration.test.info("Only Razor and Standard account type expected");
				Assert.assertTrue(razorAccountType.isDisplayed());
				Assert.assertTrue(standardAccountType.isDisplayed());
				Assert.assertEquals(0, managedAccount.size());
				break;
			case "Metatrader 5/Metatrader 4":
				ExtentReportGeneration.test.info("Only Razor,Standard and Managed account type expected");
				Assert.assertTrue(razorAccountType.isDisplayed());
				Assert.assertTrue(standardAccountType.isDisplayed());
				Assert.assertTrue(managedAccountType.isDisplayed());
				break;
			case "Metatrader 4/ctrader":
				Assert.assertTrue(msgInTradingPreference.isDisplayed());
				Assert.assertFalse(swapFreeAccount.isDisplayed());
				if (country.contains("United Arab Emirates"))
					Assert.assertTrue(swapFreeAccount.isDisplayed());
				break;
			default:
				ExtentReportGeneration.test.info("Only Razor,Standard and Managed account type expected");
				Assert.assertTrue(razorAccountType.isDisplayed());
				Assert.assertTrue(standardAccountType.isDisplayed());
				Assert.assertTrue(managedAccountType.isDisplayed());
				break;
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Account type validation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Account type validation failed");
			logger.fail("exception is " + e);
		}
	}

	public void selectAccountType(String elementName) {
		try {
			switch (jr.GetElementValue(elementName + ".accounttype")) {
			case "Standard":
				standardAccountType.click();
				break;
			case "Razor":
				razorAccountType.click();
				break;
			case "ManagedAccount":
				managedAccountType.click();
				break;
			default:
				standardAccountType.click();
				break;
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Account type Selection failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Account type Selection failed");
			logger.fail("exception is " + e);
		}
	}

	public void validateTradingPreference(String country) {
		try {
			String platform = jr.GetElementValue(country + ".Platform");
			checkAccountType(platform);
			String accountType = jr.GetElementValue(country + ".accounttype");
			selectAccountType(accountType);
			Assert.assertTrue(currencyDropDown.isDisplayed(), "Currency dropdown is not displayed");
			Assert.assertTrue(refferedByOptional.isDisplayed(), "RefferedByOptional field is not displayed");
			if (!platform.contains("ctrader")) {
				Assert.assertTrue(tradingAccountPassword.isDisplayed(), "TradingAccountPassword is not displayed");
				Assert.assertTrue(confirmPassword.isDisplayed(), "ConfirmPassword is not displayed");
			}
			if (accountType.equalsIgnoreCase("Managed Account"))
				Assert.assertTrue(managedAccountToken.isDisplayed(), "ManagedAccountToken is not displayed");
			ExtentReportGeneration.test.info("Trading Preference validation is Success");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Trading Preference validation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Trading Preference validation failed");
			logger.fail("exception is " + e);
		}
	}

	public void checkPlatform_AccType(String country) {
		try {
			checkPlatformType(country);
			getDynamicElement(selectPlatformType, "Metatrader 5").click();
			checkAccountTypeBasedOnCountry("Metatrader 5", country);
			getDynamicElement(selectPlatformType, "Metatrader 4").click();
			checkAccountTypeBasedOnCountry("Metatrader 4", country);
			getDynamicElement(selectPlatformType, "cTrader").click();
			checkAccountTypeBasedOnCountry("ctrader", country);
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Platform and Account type validation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Platform and Account type validation failed");
			logger.fail("exception is " + e);
		}
	}

	public void getDOB(String age) {
		try {
			if (age.contains("Below18")) {
				yr = agebelowEighteen().getYear();
				mon = agebelowEighteen().getMonthValue();
				dy = agebelowEighteen().getDayOfMonth();
			} else if (age.contains("Above18")) {
				yr = ageaboveEighteen().getYear();
				mon = ageaboveEighteen().getMonthValue();
				dy = ageaboveEighteen().getDayOfMonth();
			} else {
				yr = ageEighteen().getYear();
				mon = ageEighteen().getMonthValue();
				dy = ageEighteen().getDayOfMonth();
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("DOB generation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("DOB generation failed");
			logger.fail("exception is " + e);
		}
	}

	// public void fillTradingPreferences() {
	// try {
	// cTrader.click();
	// standardAccountType.click();
	// scrollIntoView(next);
	// navigateToNextTab();
	// logger.info("Sucessfully filled TradingPreferences section");
	// } catch (Exception e) {
	// ExtentReportGeneration.test.fail(e.getMessage());
	// logger.info("Failed to fill TradingPreferences");
	// logger.fail("exception : " + e.getMessage());
	// logout();
	// }
	// }

	public void ValidateTradingPreferenceSubmission() {
		try {
			Thread.sleep(6000);
			String expected = environmentURl() + "/verification/individual/employment-and-income";
			String actual = driver.getCurrentUrl();
			Assert.assertEquals(actual, expected);
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void selectAnnualIncome(String elementName) {
		try {
			switch (jr.GetElementValue(elementName + ".AnnualIncome")) {
			case "Less than":
				annualIncomeLessThan.click();
				break;
			case "$500,000 +":
				annualIncome$500000.click();
				break;
			default:
				annualIncome$500000.click();
				break;

			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Annual Income selection failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Annual Income selection failed");
			logger.fail("exception is " + e);
		}
	}

	public void selectSourceOfFunds(String elementName) {
		try {
			switch (jr.GetElementValue(elementName + ".SourceOfFunds")) {
			case "Loans":
				sourceOfFundsLoan.click();
				break;
			case "Inheritance":
				sourceOfFundsInheritance.click();
				break;
			default:
				sourceOfFundsInheritance.click();
				break;

			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Source Of Funds selection failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Source Of Funds selection failed");
			logger.fail("exception is " + e);
		}
	}

	public void selectEmploymentStatus(String elementName) {
		try {
			switch (jr.GetElementValue(elementName + ".EmploymentStatus")) {
			case "Entrepreneur":
				employmentStatusEntrepreneur.click();
				break;
			case "Unemployed":
				employmentStatusUnemployed.click();
				break;
			default:
				employmentStatusEntrepreneur.click();
				break;

			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Employment Status selection failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Employment Status selection failed");
			logger.fail("exception is " + e);
		}
	}

	public void fillTextFieldsInEmploymentAndIncomeDFSA(String elementName) {
		try {
			netAssets.sendKeys(jr.GetElementValue(elementName + ".NetAssets"));
			occupation.sendKeys(jr.GetElementValue(elementName + ".Occupation"));
			industry.sendKeys(jr.GetElementValue(elementName + ".Industry"));
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Text fields are not filled");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Text fields are not filled");
			logger.fail("exception is " + e);
		}
	}

	public void fillEmploymentAndIncomeDFSA(String elementName) {
		try {
			selectAnnualIncome(elementName);
			fillTextFieldsInEmploymentAndIncomeDFSA(elementName);
			selectSourceOfFunds(elementName);
			selectEmploymentStatus(elementName);
			navigateToNextTab();
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled Employment and Income section");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to fill Employment And Income tab");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void ValidateEmploymentAndIncomeSubmission() throws InterruptedException {
		try {
			Thread.sleep(6000);
			String expected = environmentURl() + "/verification/individual/quiz";
			String actual = driver.getCurrentUrl();
			Assert.assertEquals(actual, expected);
			ExtentReportGeneration.test.log(Status.INFO,
					"Successfully validated submission of Employment and Income section");
		} catch (AssertionError e) {
			logger.fail("Failed to move to quiz after Employment & Income");
			logout();
		}
	}

	public void ValidateWarningMsgInEmploymentAndIncome() {
		try {
			Thread.sleep(2000);
			Assert.assertTrue(annualIncomeAlert.isDisplayed(), "Annual Income alert is not displayed");
			Assert.assertTrue(sourceOfFundsAlert.isDisplayed(), "Source of Funds alert is not displayed");
			Assert.assertTrue(employmentStatusAlert.isDisplayed(), "Employment status alert is not displayed");
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void ValidateRequiredMsgInEmploymentAndIncome() {
		try {
			Thread.sleep(2000);
			Assert.assertTrue(netAssetsAlert.isDisplayed(), "NetAssets alert is not displayed");
			Assert.assertTrue(occupationAlert.isDisplayed(), "Occupation alert is not displayed");
			Assert.assertTrue(industryAlert.isDisplayed(), "Industry alert is not displayed");
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public WebElement getDynamicElement(WebElement parentElement, String option) {
		return parentElement.findElement(By.xpath("//div[contains(text(),'" + option + "')]")); // provide
																								// HTML
																								// text
																								// of
																								// option
	}

	public WebElement getDynamicElement(String option) {
		return driver.findElement(By.xpath("//div[contains(text(),'" + option + "')]/..")); // provide
																							// HTML
																							// text
																							// of
																							// option
	}

	public WebElement getelemnetbyindex(String option) {
		return driver.findElement(By.xpath("//div[contains(text(),'" + option + "')]/..")); // provide
																							// HTML
																							// text
																							// of
																							// option
	}

	public void fillTradingPreferences(String elementName) {
		try {
			switch (jr.GetElementValue(elementName + ".Platform")) {
			case "Metatrader 5":
				getDynamicElement(selectPlatformType, "Metatrader 5").click();
				break;
			case "Metatrader 4":
				getDynamicElement(selectPlatformType, "Metatrader 4").click();
				break;
			case "cTrader":
				getDynamicElement(selectPlatformType, "cTrader").click();
				break;
			default:
				getDynamicElement(selectPlatformType, "Metatrader 5").click();
				break;
			}
			Thread.sleep(2000);
			scrollIntoView(next);
			next.click();
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled Trading Preferences");

		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to fill Trading Preferences form.");
			test.fail("Failed to fill Trading Preferences form.");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void checkPlatformType(String country) {
		try {
			switch (country) {
			case "Australia/United Kingdom/United Arab Emirates":
				ExtentReportGeneration.test.info("Only Metatrader 5,Metatrader 4 and cTrader platforms are expected");
				Assert.assertTrue(getDynamicElement(selectPlatformType, "Metatrader 5").isDisplayed());
				Assert.assertTrue(getDynamicElement(selectPlatformType, "Metatrader 4").isDisplayed());
				Assert.assertTrue(getDynamicElement(selectPlatformType, "cTrader").isDisplayed());
				break;
			default:
				ExtentReportGeneration.test.info("Only Metatrader 5,Metatrader 4 and cTrader platforms are expected");
				Assert.assertTrue(getDynamicElement(selectPlatformType, "Metatrader 5").isDisplayed());
				Assert.assertTrue(getDynamicElement(selectPlatformType, "Metatrader 4").isDisplayed());
				Assert.assertTrue(getDynamicElement(selectPlatformType, "cTrader").isDisplayed());
				break;
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to check PlatformType");
			test.fail("Failed to check PlatformType");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void FillemploymentandIncomeAsicFca(String elementName) {
		try {
			Thread.sleep(2000);
			getDynamicElement(funds, jr.GetElementValue(elementName + ".Funds")).click();
			getDynamicElement(whichIndustry, jr.GetElementValue(elementName + ".Industry")).click();
			scrollIntoView(getDynamicElement(positionHold, jr.GetElementValue(elementName + ".PositionHold")));
			getDynamicElement(positionHold, jr.GetElementValue(elementName + ".PositionHold")).click();
			getDynamicElement(annualSalary, jr.GetElementValue(elementName + ".AnnualSalary")).click();
			String savingsNinvestment = jr.GetElementValue(elementName + ".SavingsAndInvestments");
			driver.findElement(By.xpath("//*[contains(text(),'savings and investments')]/../div//div[contains(text(),'"
					+ savingsNinvestment + "')]/..")).click();
			getDynamicElement(experience, jr.GetElementValue(elementName + ".Experience")).click();
			if (isElementPresent(previouslyTraded)) {
				previouslyTraded.click();
				Thread.sleep(1000);
				iConfirmCheckbox.click();
			}
			scrollIntoView(next);
			next.click();
		}

		catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to select options in Employment and Income page");
			test.fail("Failed to select options in Employment and Income page");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void validateEmploymentAndIncome(String country) {
		try {
			Thread.sleep(2000);
			if (country.equalsIgnoreCase("UK"))
				Assert.assertTrue(annualsalaryAlert.getText()
						.contains("Trading CFDs and foreign exchange contracts carries significant risk"));
			else
				Assert.assertTrue(annualsalaryAlert.getText()
						.contains("It's important to trade with capital you can afford to lose"));
			logger.info("Error message present for least income section");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to get error message with least income selected");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void startAppropriatenessTest() {
		try {
			appropriatenessTestButton.click();
			waitForPageLoaded();
			Assert.assertTrue(OTCDerivatives.isDisplayed());
			ExtentReportGeneration.test.log(Status.INFO, "Stated Appropriateness test");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to start Appropriateness test");
		}
	}

	public void fillAppropriatenessTestSet1Q1(String elementName) {
		try {
			String BondsAndSharess = jr.GetElementValue(elementName + ".BondsAndShares");
			getDynamicElement(bondsAndShares, BondsAndSharess).click();
			String ExchangeTraded = jr.GetElementValue(elementName + ".ExcahngeTraded");
			WebElement traded = driver
					.findElement(By.xpath("//span[contains(text(),'Exchange Traded')]/../div//div[contains(text(),'"
							+ ExchangeTraded + "')]/.."));
			scrollIntoView(traded);
			traded.click();
			String OTCDerivatives = jr.GetElementValue(elementName + ".OTCDerivatives");
			WebElement OTC = driver
					.findElement(By.xpath("//span[contains(text(),'OTC Derivatives')]/../div//div[contains(text(),'"
							+ OTCDerivatives + "')]/.."));
			scrollIntoView(OTC);
			OTC.click();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill Q1 first set of Appropriateness Test");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void AppropriatenesstestFirstset(String elementName) {
		try {
			fillAppropriatenessTestSet1Q1(elementName);
			// if (!BondsAndSharess.equalsIgnoreCase("Never") &&
			// !ExchangeTraded.equalsIgnoreCase("Never")
			// && !OTCDerivatives.equalsIgnoreCase("Never"))
			getDynamicElement(jr.GetElementValue(elementName + ".HowYouTraded")).click();
			getDynamicElement(jr.GetElementValue(elementName + ".ExperienceOfQualification")).click();
			getDynamicElement(jr.GetElementValue(elementName + ".PurposeOfTrade")).click();
			Thread.sleep(2000);
			scrollIntoView(next);
			// next.click();
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled First set of Appropriateness test");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill first set of Appropriateness Test");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void knowledgeTest(String elementName, int setnumber) {
		try {
			for (int i = 1; i <= 6; i++) {
				Thread.sleep(500);
				if (setnumber == 2)
					Thread.sleep(3000);
				if (jr.GetElementValue(elementName + ".Set" + setnumber + "Q" + i + "").equalsIgnoreCase("TRUE"))
					trueBttn.click();
				if (jr.GetElementValue(elementName + ".Set" + setnumber + "Q" + i + "").equalsIgnoreCase("FALSE"))
					falseBttn.click();
			}
			Thread.sleep(2000);
			scrollIntoView(next);
			waitForElementToBeClickable(next);
			next.click();
			ExtentReportGeneration.test.log(Status.INFO, "Sucessfully filled Knowledge test" + setnumber);

		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill set" + setnumber + " of Knowledge Test");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void validateAndCloseUnsuccessfulPopup() {
		try {
			Assert.assertTrue(unSuccessful.isDisplayed(), "UnSuccessful warning pop-up is not displayed");
			ExtentReportGeneration.test.info("UnSuccessful warning pop-up is displayed");
			closeBtn.click();
			driver.navigate().back();
			closeAlert.click();
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to validate UnSuccessful warning pop-up");
		}
	}

	public void validateAppropriatenessTestSubmission() throws InterruptedException {
		try {
			waitForPageWithUrl(util.environmentURl() + "/verification/individual/legal", 8);
			validateURL(util.environmentURl() + "/verification/individual/legal",
					"Sucessfully moved to declaration page submitting Appropriateness/Knowledge test");
			ExtentReportGeneration.test
					.info("successfully Submitted Appropriateness test and moved to declaration page");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to validate appropriateness test submission");
		}
	}

	public void declaration() {
		try {
			waitForPageLoaded();
			Thread.sleep(2000);
			agree.click();
			Thread.sleep(2000);
			scrollIntoView(next);
			waitForElementToBeClickable(next);
			next.click();
			ExtentReportGeneration.test.log(Status.INFO, "Clicked I agree and submitted declaration");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to Submit Declaration");
		}
	}

	public void validateDeclarationSubmission() throws InterruptedException {
		try {
			Thread.sleep(5000);
			Assert.assertTrue(uploadDocuments.isDisplayed());
			ExtentReportGeneration.test.log(Status.INFO,
					"Successfully completed declation and moved to ID verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to do Declaration");
		}
	}

	public void fillEVerificationPassport(String elementName) throws InterruptedException {
		try {
			passportEV.click();
			String passportType = jr.GetElementValue(elementName + ".PassportType");
			getDropDownByVisibleText(passportDropDown, passportType);
			Thread.sleep(1000);
			if (!passportType.contains("foreign")) {
				passportNumber.sendKeys(jr.GetElementValue(elementName + ".PassportNumber"));
			} else {
				Thread.sleep(500);
				visaPassportNumber.sendKeys(jr.GetElementValue(elementName + ".VisaPassportNumber"));
				getDropDownByVisibleText(countryOfIssue, jr.GetElementValue(elementName + ".CountryOfIssue"));

			}
			Thread.sleep(500);
			verifyMyDetails.click();
			Thread.sleep(5000);
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled Passport details of E-Verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to fill Passport details of E-Verification");
		}
	}

	public void fillEVerificationDriverLicence(String elementName) throws InterruptedException {
		try {
			australianDriversLicence.click();
			getDropDownByVisibleText(australianDriversLicenceDropDown,
					jr.GetElementValue(elementName + ".DriverLicenceType"));
			drivingLicenseNumber.sendKeys(jr.GetElementValue(elementName + ".DrivingLicenceNumber"));
			verifyMyDetails.click();
			Thread.sleep(5000);
			ExtentReportGeneration.test.log(Status.INFO,
					"Successfully filled Driver's licence details of E-Verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to fill Driver's licence details of E-Verification");
		}
	}

	public void fillEVerificationElectralRoll(String elementName) {
		try {
			australianElectoralRoll.click();
			streetNumber.sendKeys(jr.GetElementValue(elementName + ".StreetNumber"));
			verifyMyDetails.click();
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled ElectralRoll details of E-Verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to fill ElectralRoll details of E-Verification");
		}
	}

	public void fillEVerification(String document) throws InterruptedException {
		try {
			if (document.contains("Passport")) {
				fillEVerificationPassport(document);
				passportEV.click();
				Thread.sleep(500);
				Assert.assertTrue(inProgressStatus.isDisplayed(), "Status is not IN-PROGRESS for Passport");
			} else {
				fillEVerificationDriverLicence(document);
				australianDriversLicence.click();
				Assert.assertTrue(inProgressStatus.isDisplayed(), "Status is not IN-PROGRESS for DriverLicence");
			}
			fillEVerificationElectralRoll(document);
			Thread.sleep(3000);
			australianElectoralRoll.click();
			Thread.sleep(500);
			Assert.assertTrue(inProgressStatus.isDisplayed(), "Status is not IN-PROGRESS for ElectoralRoll");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to fill E-Verification");
		}
	}

	public void mandatoryFieldEV(String elementName) throws InterruptedException {
		try {
			passportEV.click();
			getDropDownByVisibleText(passportDropDown, "Australian Passport");
			verifyMyDetails.click();
			verifyMyDetails.click();
			Assert.assertTrue(licenseNumberReuiredMsg.isDisplayed(),
					"Australian Passport Required msg is not displayed");
			Thread.sleep(500);
			getDropDownByVisibleText(passportDropDown, "Visa (foreign passport)");
			verifyMyDetails.click();
			verifyMyDetails.click();
			Assert.assertTrue(licenseNumberReuiredMsg.isDisplayed(),
					"Visa (foreign passport) Required msg is not displayed");
			Thread.sleep(500);
			visaPassportNumber.sendKeys(jr.GetElementValue(elementName + ".VisaPassportNumber"));
			getDropDownByVisibleText(countryOfIssue, jr.GetElementValue(elementName + ".CountryOfIssue"));
			verifyMyDetails.click();
			Thread.sleep(5000);
			australianDriversLicence.click();
			getDropDownByVisibleText(australianDriversLicenceDropDown,
					jr.GetElementValue(elementName + ".DriverLicenceType"));
			verifyMyDetails.click();
			verifyMyDetails.click();
			Assert.assertTrue(licenseNumberReuiredMsg.isDisplayed(), "DriverLicence Required msg is not displayed");
			Thread.sleep(500);
			drivingLicenseNumber.sendKeys(jr.GetElementValue(elementName + ".DrivingLicenceNumber"));
			verifyMyDetails.click();
			Thread.sleep(5000);
			australianElectoralRoll.click();
			verifyMyDetails.click();
			verifyMyDetails.click();
			Assert.assertTrue(licenseNumberReuiredMsg.isDisplayed(), "StreetNumber Required msg is not displayed");
			closeAlert.click();
			ExtentReportGeneration.test.log(Status.INFO, "MandatoryField validation is Success, E-Verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Mandatory field validation failed, E-Verification");
		}
	}

	public void addressfieldPopulationEV(String elementName) {
		try {
			Assert.assertEquals(streetNameEV.getAttribute("value"), jr.GetElementValue(elementName + ".StreetName"));
			Assert.assertEquals(subUrbEV.getAttribute("value"), jr.GetElementValue(elementName + ".SubUrb"));
			Assert.assertEquals(stateEV.getAttribute("value"), jr.GetElementValue(elementName + ".State"));
			Assert.assertEquals(postCodeEV.getAttribute("value"), jr.GetElementValue(elementName + ".PostCode"));
			ExtentReportGeneration.test.log(Status.INFO, "Address field Prepopulation is Success, E-Verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "Address field Prepopulation failed, E-Verification");
		}
	}

	public void invalidDataValidationEV(String elementName) throws InterruptedException {
		try {
			passportEV.click();
			getDropDownByVisibleText(passportDropDown, jr.GetElementValue(elementName + ".PassportType"));
			passportNumber.sendKeys(jr.GetElementValue(elementName + ".PassportNumber"));
			Thread.sleep(500);
			verifyMyDetails.click();
			Assert.assertTrue(invalidPP_LicenceNumber.isDisplayed(), "Invalid Passport number validation failed");
			australianDriversLicence.click();
			getDropDownByVisibleText(australianDriversLicenceDropDown,
					jr.GetElementValue(elementName + ".DriverLicenceType"));
			drivingLicenseNumber.sendKeys(jr.GetElementValue(elementName + ".DrivingLicenceNumber"));
			verifyMyDetails.click();
			Assert.assertTrue(invalidPP_LicenceNumber.isDisplayed(), "Invalid Licence number validation failed");
			closeAlert.click();
			ExtentReportGeneration.test.log(Status.INFO, "InvalidDataValidation is Success, E-Verification");
		} catch (AssertionError e) {
			ExtentReportGeneration.test.log(Status.FAIL, "InvalidDataValidation failed, E-Verification");
		}
	}

	public void translationsWithDifferentLanguages() {
		try {
			Thread.sleep(1000);
			changeLanguage.click();
			for (int i = 1; i < 14; i++) {
				if (i > 1)
					changeLanguage.click();
				Thread.sleep(1000);
				String selectlanguage = languages.get(i).getText();
				Writer writer = new PrintWriter("files\\new.txt", "UTF-8");
				writer.write(selectlanguage);
				writer.close();
				BufferedReader bufferedReader = new BufferedReader(new FileReader("files\\new.txt"));
				String lang = bufferedReader.readLine();

				ExtentReportGeneration.test.info("language selected is " + lang);
				System.out.println(selectlanguage);
				languages.get(i).click();
				Thread.sleep(1000);
				if (isElementPresent(warningCountrySelection.get(0)) == true)
					warningCountrySelection.get(0).click();
				Thread.sleep(1000);
				System.out.println(pannelHeader.get(0).getText());

				if (selectlanguage.contains("EspaÃ±ol") && !selectlanguage.contains("EspaÃ±ol (LATAM)"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("DATOS PERSONALES"), "Failed for EspaÃ±ol");
				else if (selectlanguage.contains("Ð ÑƒÑ�Ñ�ÐºÐ¸Ð¹"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("Ð›Ð˜Ð§Ð�Ð«Ð• Ð”Ð�Ð�Ð�Ð«Ð•"), "failed for Ð ÑƒÑ�Ñ�ÐºÐ¸Ð¹");
				else if (selectlanguage.contains("ä¸­æ–‡"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("ä¸ªäººè¯¦ç»†ä¿¡æ�¯"), "Failed for ä¸­æ–‡");
				else if (selectlanguage.contains("Tiáº¿ng Viá»‡t"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("THÃ”NG TIN CÃ� NHÃ‚N"),
							"Failed for Tiáº¿ng Viá»‡t");
				else if (selectlanguage.contains("Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("Ø§Ù„ØªÙ�Ø§ØµÙŠÙ„ Ø§Ù„Ø´Ø®ØµÙŠØ©"), "Failed for Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©");
				else if (selectlanguage.contains("à¹„à¸—à¸¢"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("à¸‚à¹‰à¸­à¸¡à¸¹à¸¥à¸ªà¹ˆà¸§à¸™à¸šà¸¸à¸„à¸„à¸¥"), "failed for à¹„à¸—à¸¢");
				else if (selectlanguage.contains("Italiano"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("DATI PERSONALI"), "failed for Italiano");
				else if (selectlanguage.contains("FranÃ§ais"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("DÃ‰TAILS PERSONNELS"),
							"Failed for FranÃ§ais");
				else if (selectlanguage.contains("Deutsch"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("PERSÖNLICHE ANGABEN"),
							"failed for Deutsch");
				else if (selectlanguage.contains("Polski"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("DANE OSOBOWE"), "Failed for Polski");
				else if (selectlanguage.contains("EspaÃ±ol (LATAM)") && !selectlanguage.contains("EspaÃ±ol"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("DATOS PERSONALES"),
							"Failed for EspaÃ±ol (LATAM)");
				else if (selectlanguage.contains("TÃ¼rkÃ§e"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("KIÅžISEL BILGILER"), "Failed for TÃ¼rkÃ§e");
				else if (selectlanguage.contains("Portugues do Brasil"))
					Assert.assertTrue(pannelHeader.get(0).getText().contains("DADOS PESSOAIS"),
							"Failed for Portugues do Brasil");
				ExtentReportGeneration.test
						.pass("Translations are properly appearing on verification Page for " + lang);
			}
			changeLanguage.click();
			languages.get(0).click();
			driver.navigate().refresh();

		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Translations are broken");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Translations are broken for verificationPage");
			logger.fail("exception is " + e);
			changeLanguage.click();
			languages.get(0).click();
			driver.navigate().refresh();
		}
	}

	public void sikuliDocumentUpload() {
		try {
		
			manualVerification.click();
			attach.click();
			String inputFilePath = path + fs +"files\\uploadfile.png";
			Screen screen = new Screen();
			Pattern fileInputTextBox = new Pattern(
					path + fs +"files\\FileTextBox.png");
			Pattern openButton = new Pattern(
					path + fs +"files\\OpenButton.png");
			screen.wait(fileInputTextBox, 20);
			screen.type(fileInputTextBox, inputFilePath);
			screen.wait(openButton, 10);
			screen.find(openButton);
			screen.click(openButton);
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "InvalidDataValidation failed, E-Verification");
		}

	}

	public void verifyDocumentUploadedandUserSubmitted() throws InterruptedException {
		try {
			Thread.sleep(240000);
			waitForPageLoaded();
			Assert.assertTrue(isElementPresent(uploadedDocument));
			scrollIntoView(submit);
			clickSubmit();
			Assert.assertTrue(isElementPresent(onboardingConfirm));
			closeBtn.click();
		} catch (AssertionError e) {
			logger.fail("exception : " + e.getMessage());
			ExtentReportGeneration.test.log(Status.FAIL, "failed to upload document and onboard user");
		}
	}
	

	public void validateIBBannedCountriesInDropdown(WebElement country) throws InterruptedException {
		try {
			Select dropdown = new Select(country);
			List<WebElement> options = dropdown.getOptions();
			System.out.println("optionSizee++++" + options.size());
			for (int i = 1; i < options.size(); i++) {
				WebElement optionElement = options.get(i);
				System.out.println("optionElement++++" + optionElement);
				String optionValue = optionElement.getAttribute("value");
				System.out.println("optionValue++++" + optionValue);
				if (optionValue.contains("SG") || optionValue.contains("ZA")) {
					ExtentReportGeneration.test.fail("Banned countries - Singapore & South Africa are not removed yet");
					logger.fail("Banned countries - Singapore & South Africa are not removed yet");
				} 
			}

		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Banned country validation in dropdown failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Banned country validation in dropdown failed");
			logger.fail("exception is " + e.getMessage());
		}
	}

	public void fillProgramInformationIB(String elementName) throws InterruptedException {
		try {
			getDropDownByVisibleText(IBprogramType, jr.GetElementValue(elementName + ".IBprogramType"));
			getDropDownByVisibleText(regulatoryLicense, jr.GetElementValue(elementName + ".regulatoryLicense"));
			programDetailsTextArea1.sendKeys(jr.GetElementValue(elementName + ".programDetailsTextArea1"));
			programAdvertisingDetailsTextArea2.sendKeys(jr.GetElementValue(elementName + ".programAdvertisingDetailsTextArea2"));
			getDropDownByVisibleText(communicationFrequency, jr.GetElementValue(elementName + ".communicationFrequency"));
			getDropDownByVisibleText(individualOrCompany, jr.GetElementValue(elementName + ".individualOrCompany"));
			getDropDownByVisibleText(rebateCurrency, jr.GetElementValue(elementName + ".rebateCurrency"));
			getDropDownByVisibleText(referredClientNumber, jr.GetElementValue(elementName + ".referredClientNumber"));
			workingWithOtherBrokers.click();
			getDropDownByVisibleText(expectedMonthlyReferredClientNumber, jr.GetElementValue(elementName + ".expectedMonthlyReferredClientNumber"));
			getDropDownByVisibleText(averageMonthlyClientTradingVolume, jr.GetElementValue(elementName + ".averageMonthlyClientTradingVolume"));
			getDropDownByVisibleText(totalClientDeposits,jr.GetElementValue(elementName + ".totalClientDeposits"));
			Thread.sleep(500);
			agreeTermsAndConditions.click();
			agreeAccuracyVerification.click();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed in filling Program Information for IB");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed in filling Program Information for IB");
			logger.fail("exception is " + e);
		}
	}

	public void validateIBBannedCountries(String elementName) throws InterruptedException {
		try {
			System.out.println("Inside++++++++++++++");
			Thread.sleep(2000);
			getDropDownByVisibleText(accountType, jr.GetElementValue(elementName + ".AccountType"));
//			validateIBBannedCountriesInDropdown(country);
			ExtentReportGeneration.test.log(Status.INFO,
					"Banned countries verification is completed in Registration page");
			String	generatedEmailAddress = jr.GetElementValue(elementName + ".Email") + getRandomNumber(100000) + getRandomAlphaNumericString(3)
			+ jr.GetElementValue(elementName + ".Company");
			emailAddress.sendKeys(generatedEmailAddress);
			password.sendKeys(jr.GetElementValue(elementName + ".Password"));
			confirmPassword.sendKeys(jr.GetElementValue(elementName + ".ConfirmPassword"));
			Thread.sleep(1000);
			jsClick(btnRegister);
			Thread.sleep(9000);
			validateURL(environmentURl() + "/verification/ib", "Verify Registration");
			validateIBBannedCountriesInDropdown(operatingCountryIBprogram);
			validateIBBannedCountriesInDropdown(clientRegion);
			ExtentReportGeneration.test.log(Status.INFO,
					"Banned countries verification is completed in IB details section");
			getDropDownByVisibleText(operatingCountryIBprogram, jr.GetElementValue(elementName + ".operatingCountryIBprogram"));
			getDropDownByVisibleText(clientRegion,jr.GetElementValue(elementName + ".clientRegion"));
			Thread.sleep(500);
			jsClick(next);
			Thread.sleep(1000);
			fillProgramInformationIB(elementName);
			Thread.sleep(500);
			jsClick(next);
			Thread.sleep(1000);
			validateURL(environmentURl() + "/verification/ib/personal",
					"Verify ProgramInformation submission");
			validateIBBannedCountriesInDropdown(country);
			ExtentReportGeneration.test.log(Status.INFO,
					"Banned countries verification is completed in Personal details section");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Banned country validation failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Banned country validation failed");
			logger.fail("exception is " + e);
		}
	}

	public void fillChineseName(String elementName) {
		try {
			Thread.sleep(3000);
			getChineseName().sendKeys(jr.GetElementValue(elementName + ".ChineseName"));
			logger.info("Sucessfully filled Chinese name.");
			ExtentReportGeneration.test.info("Sucessfully filled Chinese name.");
		} catch (Exception e)  {
			
			ExtentReportGeneration.test.fail("Unable to fill filled Chinese name.");
			logger.fail("Failed to fill filled Chinese name.");
			logger.fail("exception : " + e.getMessage());
		}
	}
	
	public void fillTradingAccountPassword(String elementName) {
		try {
			Thread.sleep(3000);
			getTradingAccountPassword().sendKeys(jr.GetElementValue(elementName + ".Accountpassword"));
			getConfirmPassword().sendKeys(jr.GetElementValue(elementName + ".ConfirmPassword"));
			logger.info("Sucessfully filled Trading Account Password and Confirm Password.");
			ExtentReportGeneration.test.info("Sucessfully filled Account Password and Confirm Password.");
		} catch (Exception e)  {
			
			ExtentReportGeneration.test.fail("Unable to fill filled Account Password and Confirm Password.");
			logger.fail("Failed to fill filled Account Password and Confirm Password.");
			logger.fail("exception : " + e.getMessage());
		}
	}
	
	public void FillemploymentandIncomeSVGWithYesOption(String testDataName,boolean flag) {
		try {
			Thread.sleep(3000);
			getDynamicElement(funds, jr.GetElementValue(testDataName + ".Funds")).click();
			getDynamicElement(whichIndustry, jr.GetElementValue(testDataName + ".Industry")).click();
			getDynamicElement(positionHold, jr.GetElementValue(testDataName + ".PositionHold")).click();
			scrollIntoView(getDynamicElement(annualSalary, jr.GetElementValue(testDataName + ".AnnualSalary")));
			getDynamicElement(annualSalary, jr.GetElementValue(testDataName + ".AnnualSalary")).click();
			getDynamicElement(savingsAndInvestments, jr.GetElementValue(testDataName + ".SavingsAndInvestments")).click();
			getDynamicElement(experience, jr.GetElementValue(testDataName + ".Experience")).click();
			if(flag) {
			getDynamicElement(previouslyTradedLabel, jr.GetElementValue(testDataName + ".PreviouslyTradedYes")).click();
			getConfirmCheckBox().click();
			scrollIntoView(next);
			navigateToNextTab();
			}else {
				getDynamicElement(previouslyTradedLabel, jr.GetElementValue(testDataName + ".PreviouslyTradedNo")).click();
				getDynamicElement(higherQualification, jr.GetElementValue(testDataName + ".HigherQualifications")).click();
				getConfirmCheckBox().click();
				scrollIntoView(next);
				navigateToNextTab();
			}
		}

		catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to select options in Employment and Income page");
			test.fail("Failed to select options in Employment and Income page");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	
	public void FillemploymentandIncomeSVG(String testDataName) {
		try {
			Thread.sleep(3000);
			getDynamicElement(funds, jr.GetElementValue(testDataName + ".Funds")).click();
			getDynamicElement(whichIndustry, jr.GetElementValue(testDataName + ".Industry")).click();
			getDynamicElement(positionHold, jr.GetElementValue(testDataName + ".PositionHold")).click();
			scrollIntoView(annualSalary);
			//scrollIntoView(getDynamicElement(annualSalary, jr.GetElementValue(testDataName + ".AnnualSalary")));
			getDynamicElement(annualSalary, jr.GetElementValue(testDataName + ".AnnualSalary")).click();
			getDynamicElement(savingsAndInvestments, jr.GetElementValue(testDataName + ".SavingsAndInvestments")).click();
			getDynamicElement(experience, jr.GetElementValue(testDataName + ".Experience")).click();
			scrollIntoView(next);
			navigateToNextTab();
			}
		catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to select options in Employment and Income page");
			test.fail("Failed to select options in Employment and Income page");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	
	public void verifySuitabilityTest_SVG(String testDataName) {
		try {
			
			Thread.sleep(3000);
			getDynamicElement(riskManagement_SVG, jr.GetElementValue(testDataName + ".RequiringKnowledge")).click();
			scrollIntoView(nextbtn);
			nextbtn.click();
			getDynamicElement(professionalQualification_SVG, jr.GetElementValue(testDataName + ".ProfessionalQualificationForTradingAndLeverage")).click();
			nextbtn.click();
			getDynamicElement(fXValue_TradingExperience_SVG, jr.GetElementValue(testDataName + ".FX_TradingExperience")).click();
			cfdsValue_TradingExperience_SVG.click();
			futureValue_TradingExperience_SVG.click();
			sharesValue_TradingExperience_SVG.click();
			Thread.sleep(2000);
			
			getDynamicElement(fXValue_TradingFrequency_SVG, jr.GetElementValue(testDataName + ".FX_TradingFrequency")).click();
			cfdsValue_TradingFrequency_SVG.click();
			futureValue_TradingFrequency_SVG.click();
			sharesValue_TradingFrequency_SVG.click();
			Thread.sleep(2000);
			
			getDynamicElement(fXValue_TradingLots_SVG, jr.GetElementValue(testDataName + ".FX_TradingLots")).click();
			cfdsValue_TradingLots_SVG.click();
			futureValue_TradingLots_SVG.click();
			sharesValue_TradingLots_SVG.click();
			Thread.sleep(2000);
			
			getDynamicElement(fXValue_Options_SVG, jr.GetElementValue(testDataName + ".FX_Options")).click();
			cfdsValue_Options_SVG.click();
			futureValue_Options_SVG.click();
			sharesValue_Options_SVG.click();
			Thread.sleep(2000);
			scrollIntoView(next);
			Thread.sleep(5000);
			navigateToNextTab();
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled First set of Appropriateness test");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill first set of Appropriateness Test");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	
	public void beginAppropriatenessTest() throws InterruptedException {
		try {
				Thread.sleep(5000);
				scrollIntoView(beginAppropriatenessTestButton);
				Actions action=new Actions(driver);
				action.moveToElement(beginAppropriatenessTestButton).build().perform();
				beginAppropriatenessTestButton.click();
				waitForPageLoaded();
				logger.info("clicked begin AppropriatenessTest button.");
				Assert.assertTrue(riskManagement_SVG.isDisplayed(),"Failed:-Landing page is not Appropriateness test.");
				ExtentReportGeneration.test.log(Status.INFO, "Appropriateness test Started.");
		} catch (AssertionError e) {
			logger.info("begin AppropriatenessTest button is not Enbled");
			ExtentReportGeneration.test.log(Status.FAIL, "Failed to start Appropriateness test");
		}
	}
	
	public void verifyFailSuitabilityTest_SVG(String testDataName) {
		try {
			
			Thread.sleep(3000);
			getDynamicElement(riskManagement_No_SVG, jr.GetElementValue(testDataName + ".RequiringKnowledge")).click();
			
			getDynamicElement(professionalQualification_None_SVG, jr.GetElementValue(testDataName + ".ProfessionalQualificationForTradingAndLeverage")).click();
			nextbtn.click();
			getDynamicElement(fXValue_TradingExperience_SVG, jr.GetElementValue(testDataName + ".FX_TradingExperience")).click();
			cfdsValue_TradingExperience_SVG.click();
			futureValue_TradingExperience_SVG.click();
			sharesValue_TradingExperience_SVG.click();
			Thread.sleep(2000);
			
			getDynamicElement(fXValue_TradingFrequency_SVG, jr.GetElementValue(testDataName + ".FX_TradingFrequency")).click();
			cfdsValue_TradingFrequency_SVG.click();
			futureValue_TradingFrequency_SVG.click();
			sharesValue_TradingFrequency_SVG.click();
			Thread.sleep(2000);
			
			getDynamicElement(fXValue_TradingLots_None_SVG, jr.GetElementValue(testDataName + ".FX_TradingLots")).click();
			cfdsValue_TradingLots_None_SVG.click();
			futureValue_TradingLots_None_SVG.click();
			sharesValue_TradingLots_None_SVG.click();
			Thread.sleep(2000);
			
			getDynamicElement(fXValue_Options_SVG, jr.GetElementValue(testDataName + ".FX_Options")).click();
			cfdsValue_Options_SVG.click();
			futureValue_Options_SVG.click();
			sharesValue_Options_SVG.click();
			Thread.sleep(2000);
			scrollIntoView(next);
			Thread.sleep(5000);
			navigateToNextTab();
			ExtentReportGeneration.test.log(Status.INFO, "Successfully filled First set of Appropriateness test");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill first set of Appropriateness Test");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	
	public void verifyKnowledgeTestWithSet2_SVG() {
		try {
			Thread.sleep(3000);
			Map<String,String> set2Question=createTestData();
			for(int i=0;i<10;i++) {
				String question=question_Set2_SVG.getText();
				logger.info(question);
				if(set2Question.containsKey(question)) {
					String optionToClick=set2Question.get(question);
					logger.info(optionToClick);
					getDynamicElementOption(optionToClick).click();
					logger.info("Option Clicked Successfully.");
				}
			}
			navigateToNextTab();
		}catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill Second set of Appropriateness Test");
			logger.fail("exception : " + e.getMessage());
			logout();
			
		}
		
	}
	
	public Map<String,String> createTestData(){
		Map<String,String> testData=new HashMap<>();
		
		testData.put("Which of the following statements best describes the risk in trading a leveraged CFD product compared to physical shares?", "It has a greater risk than trading physical shares");
		testData.put("What is market volatility?", "When there are large movements in price over a short period of time");
		testData.put("What does the term ‘equity’ refer to?", "Equity is your account balance plus the floating profit (or loss) of all your open positions.");
		testData.put("When trading a leveraged product, which statement is true?", "Both profits and losses can be magnified");
		testData.put("Which of the following best describes ‘gapping’?", "Gapping is the break between prices on a chart that occurs when the price of a market makes a sharp move up or down with no trading occurring in between those prices");
		testData.put("What is the term used to describe trading with borrowed capital?", "Margin trading");
		testData.put("Which of the following is NOT a major currency pair?", "NZDJPY");
		testData.put("Why would you use a stop loss?", "To limit your loss on a trade");
		testData.put("What does a stop loss do?", "Limits your loss by entering into a trade at the next available price");
		testData.put("When you trade a share CFD, which statement is true?", "You don’t own the underlying stock directly in your name");
		testData.put("When are you likely to be asked to add funds to your account?", "If the market moves against you, and it results in a margin call");
		testData.put("The platform shows the current bid/ask price of 5500/5503. What is the buy price?", "The buy price is 5503");
		testData.put("What is “the spread”?", "The difference between the bid and the ask price");
		testData.put("Who is ultimately responsible for monitoring and managing your trading account?", "It is my responsibility to monitor and manage my trading account");
		testData.put("If you wanted to trade a $20,000 position, how much money will be set aside in your account if you are using a 100:1 leverage?", "$200");
		
		return testData;
		
	}
	
	public WebElement getDynamicElementOption(String option) {
		return driver.findElement(By.xpath("//div[@class='mdc-chip-set mdc-chip-set--choice']/div/div[contains(text(),'" + option + "')]")); 
																							
	}
	
	public void knowledgeTestWithFailSet2_SVG() {
		try {
			Thread.sleep(3000);
			Map<String,String> set2Question=createFailTestData();
			for(int i=0;i<10;i++) {
				String question=question_Set2_SVG.getText();
				logger.info(question);
				if(set2Question.containsKey(question)) {
					String optionToClick=set2Question.get(question);
					logger.info(optionToClick);
					getDynamicElementOption(optionToClick).click();
					logger.info("Option Clicked Successfully.");
				}
			}
			navigateToNextTab();
		}catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Failed to Fill Second set of Appropriateness Test");
			logger.fail("exception : " + e.getMessage());
			logout();
			
		}
		
	}
	
	public Map<String,String> createFailTestData(){
		Map<String,String> testData=new HashMap<>();
		
		testData.put("Which of the following statements best describes the risk in trading a leveraged CFD product compared to physical shares?", "It has a greater risk than trading physical shares");
		testData.put("What is market volatility?", "When there are large movements in price over a short period of time");
		testData.put("What does the term ‘equity’ refer to?", "Equity is your account balance plus the floating profit (or loss) of all your open positions.");
		testData.put("When trading a leveraged product, which statement is true?", "Both profits and losses can be magnified");
		testData.put("Which of the following best describes ‘gapping’?", "Gapping is the break between prices on a chart that occurs when the price of a market makes a sharp move up or down with no trading occurring in between those prices");
		
		testData.put("What is the term used to describe trading with borrowed capital?", "Profitable trading");
		testData.put("Which of the following is NOT a major currency pair?", "USDJPY");
		testData.put("Why would you use a stop loss?", "To open a new position");
		testData.put("What does a stop loss do?", "Guarantees you won’t lose money");
		testData.put("When you trade a share CFD, which statement is true?", "You have the right to attend the annual general meeting");
		testData.put("When are you likely to be asked to add funds to your account?", "When I have closed my position for a profit");
		testData.put("The platform shows the current bid/ask price of 5500/5503. What is the buy price?", "The buy price is 5500");
		testData.put("What is “the spread”?", "The profit from a trade");
		testData.put("Who is ultimately responsible for monitoring and managing your trading account?", "Pepperstone’s risk team");
		testData.put("If you wanted to trade a $20,000 position, how much money will be set aside in your account if you are using a 100:1 leverage?", "$2,000");
		
		return testData;
		
	}
	

}
