package com.pageDefinitions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class HomePage extends UtilityLibrary {
	public TestLogger logger = new TestLogger();
	UtilityLibrary util = new UtilityLibrary();

	@FindBy(how = How.ID, using = "login-username")
	WebElement txtUsername;

	@FindBy(how = How.ID, using = "login-password")
	WebElement txtPassword;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'log in')]")
	WebElement buttonLogin;

	@FindBy(how = How.LINK_TEXT, using = "Contact us")
	WebElement lnkContactUs;

	@FindBy(how = How.LINK_TEXT, using = "Sign up here")
	WebElement lnkSignUp;

	/*@FindBy(how = How.XPATH, using = "//span[text()='LinkedIn']/parent::button")
	WebElement btnLinkedin;*/
	
	@FindBy(how = How.XPATH, using = "//img[@alt='linked-in']")
	WebElement btnLinkedin;
	
	@FindBy(how = How.XPATH, using = "//img[@alt='google']")
	WebElement btnGoogle;

	@FindBy(how = How.LINK_TEXT, using = "Forgotten password?")
	WebElement lnkForgotPwd;

	@FindBy(how = How.ID, using = "login-remember-me")
	WebElement chkboxRememberMe;

	@FindBy(how = How.ID, using = "myProfileMenuItem")
	WebElement myProfileMenuItem;

	@FindBy(how = How.XPATH, using = "//span[@id='myProfileMenuItem']//span[text()='log out']")
	WebElement logout;

	@FindBy(how = How.ID, using = "helpButtonSpan")
	WebElement chatNow;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@class,'FirstName')]")
	WebElement chatFirstname;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@class,'LastName')]")
	WebElement chatLasttname;
	 
	@FindBy(how = How.XPATH, using = "//input[contains(@class,'Email')]")
	WebElement chatEmailId;
	
	@FindBy(how = How.XPATH, using = "//button[contains(@class,'startButton')]")
	WebElement startChatButton;
	
	@FindBy(how = How.XPATH, using = "//button[contains(@class='waitingCancelChat')]")
	WebElement cancelChatRequest; 
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'plaintextContent')]")
	WebElement chattext;
	
	@FindBy(how = How.CSS, using = ".change-language-icon")
	WebElement changeLanguage;
	
	@FindAll(@FindBy(css = ".change-language li"))
	List<WebElement> languages;
		
	@FindBy(how = How.XPATH, using = "//header/h1")
	WebElement header;
	
	/****
	 * Method: Login method logs into the homepage Parameters: String --- this
	 * should same as the element in JSON file Author: Bharathi Bhogavarapu
	 ****/
	public void login(String elementName) {
		JsonReader jr = new JsonReader("jsonFilePathHomePage");
		try {
			Thread.sleep(1000);
			txtUsername.sendKeys(jr.GetElementValue(elementName + ".Username"));
			txtPassword.sendKeys(jr.GetElementValue(elementName + ".Password"));
			buttonLogin.click();
			waitForPageLoaded();
			ExtentReportGeneration.test.log(Status.INFO, "HomePage - Login :Entered username and password. clicked login button");
			logger.info("HomePage - Login :Entered username and password. clicked login button");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void clickContactUsLink(String url) throws InterruptedException {
		isDisplayed(lnkContactUs, "Contact Us Link Displayed");
		// lnkContactUs.click();
		util.jsClick(lnkContactUs);
		Thread.sleep(6000);
		validatePartialURL(url, "Contact Us URL leads to correct page");

	}

	public void clickSignUpLink(String url) throws InterruptedException {
		isDisplayed(lnkSignUp, "Sign Up Link Displayed");
		util.jsClick(lnkSignUp);
		Thread.sleep(3000);
		ExtentReportGeneration.test.log(Status.INFO, "clicked on sign up link");
		logger.info("clicked on sign up link");
		validatePartialURL(url, "Sign Up URL leads to correct page");
	}

	public void clickLinkedinBtn() {
		isDisplayed(btnLinkedin, "Linkedin Button Displayed");
		btnLinkedin.click();
	}

	public void clickGoogleBtn() {
		isDisplayed(btnGoogle, "Linkedin Button Displayed");
		btnGoogle.click();
	}

	public void clickForgotPwdLink(String url) throws InterruptedException {
		isDisplayed(lnkForgotPwd, "Forgot PWd Link Displayed");
//		lnkForgotPwd.click();
		util.jsClick(lnkForgotPwd);
		Thread.sleep(6000);
		validatePartialURL(url, "Forgot Pwd leads to correct page");
	}

	public void SelectRememberMeChkbox() {
		isDisplayed(chkboxRememberMe, "Remember Me checkbox is functional");
		if (chkboxRememberMe.isSelected()) {
			// do nothing();
		} else {
			chkboxRememberMe.click();
		}
		ExtentReportGeneration.test.log(Status.INFO, "Remember Me checkbox is selected");
		logger.info("Remember Me checkbox is selected");
	}

	public void CheckChatNow() throws InterruptedException {
		try {
			chatNow.click();
			Thread.sleep(2000);
			String name = getRandomAlphaNumericString(5);
			chatFirstname.sendKeys(name);
			chatLasttname.sendKeys(getRandomAlphaNumericString(5));
			chatEmailId.sendKeys(getRandomAlphaNumericString(8)+"@gmail.com");
			startChatButton.click();
			Assert.assertTrue(chattext.getText().contains(name));
					
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Start Chaating Button not found in the chat window, error loading chat window");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Start Chaating Button not found in the chat window, error loading chat window");
			logger.fail("exception is " + e);
		}
	}

	public void logout() {
		try {
			do {
				Thread.sleep(2000);
				myProfileMenuItem.click();
				Thread.sleep(2000);
				isDisplayed(logout, "logout button displayed");
				logout.click();
				Thread.sleep(2000);
			} while (driver.getCurrentUrl().contains("verification"));
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Logout failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Logout failed");
			logger.fail("exception is " + e);
		}
	}
	
	
	public void translationsWithDifferentLanguages() {
		try {
			changeLanguage.click();
			for(int i=1; i<14; i++){
				if(i>1)
					changeLanguage.click();
				Thread.sleep(1000);
				String selectlanguage = languages.get(i).getText();
				Writer writer = new PrintWriter("files\\new.txt", "UTF-8");
				writer.write(selectlanguage);
				writer.close();
				BufferedReader bufferedReader = new BufferedReader(new FileReader("files\\new.txt"));
				String lang = bufferedReader.readLine();
				
			ExtentReportGeneration.test.info("language selected is "+lang);
			System.out.println(selectlanguage);
			languages.get(i).click();
			System.out.println(header.getText());
			
			if (selectlanguage.contains("EspaÃ±ol")&&!selectlanguage.contains("EspaÃ±ol (LATAM)"))
				Assert.assertTrue(header.getText().contains("Iniciar sesiÃ³n en Pepperstone"), "Failed for EspaÃ±ol");
			else if (selectlanguage.contains("Ð ÑƒÑ�Ñ�ÐºÐ¸Ð¹"))
				Assert.assertTrue(header.getText().contains("Ð’Ð¾Ð¹Ñ‚Ð¸ Ð² Pepperstone"), "failed for Ð ÑƒÑ�Ñ�ÐºÐ¸Ð¹");
			else if (selectlanguage.contains("ä¸­æ–‡"))
				Assert.assertTrue(header.getText().contains("ç™»å½• Pepperstone"), "Failed for ä¸­æ–‡");
			else if (selectlanguage.contains("Tiáº¿ng Viá»‡t"))
				Assert.assertTrue(header.getText().contains("Ä�Äƒng nháº­p vÃ o Pepperstone"), "Failed for Tiáº¿ng Viá»‡t");
			else if (selectlanguage.contains("Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©"))
				Assert.assertTrue(header.getText().contains("ØªØ³Ø¬ÙŠÙ„ Ø§Ù„Ø¯Ø®ÙˆÙ„ Ø¥Ù„Ù‰ Pepperstone"), "Failed for Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©");
			else if (selectlanguage.contains("à¹„à¸—à¸¢"))
				Assert.assertTrue(header.getText().contains("à¹€à¸‚à¹‰à¸²à¸ªà¸¹à¹ˆà¸£à¸°à¸šà¸š Pepperstone"), "failed for à¹„à¸—à¸¢");
			else if (selectlanguage.contains("Italiano"))
				Assert.assertTrue(header.getText().contains("Accedi a Pepperstone"), "failed for Italiano");
			else if (selectlanguage.contains("FranÃ§ais"))
				Assert.assertTrue(header.getText().contains("Se connecter Ã  Pepperstone"), "Failed for FranÃ§ais");
			else if (selectlanguage.contains("Deutsch"))
				Assert.assertTrue(header.getText().contains("Bei Pepperstone anmelden"), "failed for Deutsch");
			else if (selectlanguage.contains("Polski"))
				Assert.assertTrue(header.getText().contains("Zaloguj się do Pepperstone"),"Failed for Polski");
			else if (selectlanguage.contains("EspaÃ±ol (LATAM)")&&!selectlanguage.contains("EspaÃ±ol"))
				Assert.assertTrue(header.getText().contains("Inicio de sesiÃ³n a Pepperstone"), "Failed for EspaÃ±ol (LATAM)");
			else if (selectlanguage.contains("TÃ¼rkÃ§e"))
				Assert.assertTrue(header.getText().contains("Pepperstone'a GiriÅŸ Yap"), "Failed for TÃ¼rkÃ§e");
			else if (selectlanguage.contains("Portugues do Brasil"))
				Assert.assertTrue(header.getText().contains("Seja bem-vindo(a) à Área Segura para clientes da Pepperstone"),"Failed for Portugues do Brasil");
			else
			Assert.assertTrue(header.getText().contains("Pepperstone"));
			ExtentReportGeneration.test.pass("Translations are properly appearing on Homepage for "+lang);
			}
			changeLanguage.click();
			languages.get(0).click();
			
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Translations are broken for Homepage");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Translations are broken for Homepage");
			logger.fail("exception is " + e);
			changeLanguage.click();
			languages.get(0).click();
			driver.navigate().refresh();
		}
	}
	
	public void ValidateExistingPageIsLoginPage() {
		try {
			waitForPageLoaded();
			if (!driver.getCurrentUrl().contains("login")||driver.getCurrentUrl().contains("individual")) {
				logout();
			}
		} catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
		}
	}
}