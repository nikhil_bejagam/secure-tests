package com.pageDefinitions;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class Onboarding extends UtilityLibrary {

	String fs = File.separator;
	public TestLogger logger = new TestLogger();
	UtilityLibrary util = new UtilityLibrary();

	@FindBy(how = How.NAME, using = "username")
	WebElement email;

	@FindBy(how = How.NAME, using = "password")
	WebElement password;

	@FindBy(how = How.XPATH, using = "//*[text()='Forgot Password?']")
	WebElement forgotPwd;

	@FindBy(how = How.XPATH, using = "//*[text()='Login here']")
	WebElement loginHereLink;

	public WebElement getLoginHereLink() {
		return loginHereLink;
	}

	@FindBy(how = How.XPATH, using = "//button/*[text()='Log In']")
	WebElement loginBtn;

	@FindBy(how = How.LINK_TEXT, using = "here")
	WebElement lnkSignUp;

	@FindBy(how = How.NAME, using = "firstName")
	WebElement firstName;

	@FindBy(how = How.NAME, using = "lastName")
	WebElement lastName;

	@FindBy(how = How.NAME, using = "country")
	WebElement country;

	@FindBy(how = How.XPATH, using = "//*[text()='Register now']")
	WebElement registerNowBtn;

	@FindBy(how = How.XPATH, using = "//*[text()='Affiliate Registration']")
	WebElement registrationPageTitle;

	@FindAll(@FindBy(xpath = "//a[contains(@class,'menuItem')]"))
	List<WebElement> leftmenuItem;

	@FindBy(how = How.XPATH, using = "//*[text()='About your business']")
	WebElement aboutYourBusinessTitle;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'promotional efforts')]")
	WebElement aboutYourBusinessFirstQtn;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'promote pepperstone?')]")
	WebElement aboutYourBusinessSecondQtn;

	@FindBy(how = How.XPATH, using = "//*[text()='Which countries do you offer services?']")
	WebElement aboutYourBusinessThirdQtn;

	@FindAll(@FindBy(xpath = "//input[@type='checkbox']"))
	List<WebElement> aboutYourBusinessFirstAns;

	@FindAll(@FindBy(xpath = "//div[contains(@class,'OriginQuestion_originQuestion__checkBoxWrapper')]"))
	List<WebElement> aboutYourBusinessSecondAns;

	@FindBy(how = How.NAME, using = "licenced")
	WebElement aboutYourBusinessLicencedCheckbox;

	@FindBy(how = How.XPATH, using = "//label/button[@type='button']")
	WebElement aboutYourBusinessAddAnotherCountry;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'completed')]/span[text()='About your business']")
	WebElement aboutYourBusinessCompletionStatus;

	@FindBy(how = How.XPATH, using = "//*[text()='Personal Details']")
	WebElement personalDetailsTitle;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'selected')]/span[text()='Personal Information']")
	WebElement personalDetailsInprogressStatus;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'completed')]/span[text()='Personal Information']")
	WebElement personalDetailsCompletionStatus;

	@FindBy(how = How.XPATH, using = "//*[text()='Identity Verification']")
	WebElement identityVerificationTitle;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'selected')]/span[text()='Identity Verification']")
	WebElement identityVerificationInprogressStatus;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'completed')]/span[text()='Identity Verification']")
	WebElement identityVerificationCompletionStatus;

	@FindBy(how = How.NAME, using = "day")
	public WebElement dayDOB;

	@FindBy(how = How.NAME, using = "month")
	public WebElement monthDOB;

	@FindBy(how = How.NAME, using = "dateOfBirth")
	public WebElement yearDOB;

	@FindBy(how = How.XPATH, using = "//div[@role='tooltip']")
	public WebElement DOBErrorMsg;

	@FindBy(how = How.NAME, using = "address")
	WebElement address;

	@FindBy(how = How.XPATH, using = "//*[text()='Contact Details']")
	WebElement contactDetails;

	@FindBy(how = How.NAME, using = "address2")
	WebElement address2;

	@FindBy(how = How.NAME, using = "state")
	WebElement state;

	@FindBy(how = How.NAME, using = "city")
	WebElement city;

	@FindBy(how = How.NAME, using = "zipCode")
	WebElement postCode;

	@FindBy(how = How.NAME, using = "phone")
	WebElement phone;

	@FindBy(how = How.NAME, using = "id")
	WebElement idType;

	@FindBy(how = How.XPATH, using = "//button/span[text()='Submit']")
	public WebElement submitBtn;

	@FindBy(how = How.NAME, using = "file")
	WebElement chooseFile;

	@FindBy(how = How.XPATH, using = "//img[@alt='Passport']")
	WebElement passportImg;

	@FindBy(how = How.XPATH, using = "//img[@alt='Front']")
	WebElement driverLicenseFrontImg;

	@FindBy(how = How.XPATH, using = "//img[@alt='Back']")
	WebElement driverLicenseBackImg;

	@FindBy(how = How.XPATH, using = "//img[contains(@alt,'Please attach your bank statement or utility bill.')]")
	WebElement proofOfResidencyImg;

	@FindBy(how = How.XPATH, using = "//*[text()='Next']")
	WebElement nxtBtn;

	public WebElement getNxtBtn() {
		return nxtBtn;
	}

	@FindAll(@FindBy(how = How.XPATH, using = "//div[contains(@class,'item')]"))
	List<WebElement> addressList;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'use-this-address')]")
	WebElement useThisAddress;
	
	@FindBy(how = How.XPATH, using = "//*[text()='Menu']")
	WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "//*[name()='svg']")
	WebElement logout;

	public WebElement getAboutYourBusinessFirstAnswer(String option) {
		return driver.findElement(By.xpath("//*[text()='" + option + "']/input"));
	}

	public WebElement getAboutYourBusinessSecondAnswer(String option) {
		return driver.findElement(By.xpath("//*[text()='" + option + "']"));
	}

	public WebElement getAboutYourBusinessSecondAnswerToggleBtn(String option) {
		return driver
				.findElement(By.xpath("//*[text()='" + option + "']/following::div[contains(@class,'btn toggle')][1]"));
	}

	public WebElement getAboutYourBusinessSecondAnswerUrl(String option) {
		return driver.findElement(By.xpath("//div//*[text()='" + option + "']/following::div/input[1]"));
	}

	JsonReader jr = new JsonReader("jsonFilePathOnboardingPage");

	public void login(String elementName) {
		try {
			Thread.sleep(1000);
			email.sendKeys(jr.GetElementValue(elementName + ".Username"));
			password.sendKeys(jr.GetElementValue(elementName + ".Password"));
			loginBtn.click();
			waitForPageLoaded();
			ExtentReportGeneration.test.log(Status.INFO,
					"OnboardingHomePage - Login :Entered username and password. clicked login button");
			logger.info("OnboardingHomePage - Login :Entered username and password. clicked login button");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void clickOnForgotPwd(String url) throws InterruptedException {
		isDisplayed(forgotPwd, "Forgot PWd is Displayed");
		util.jsClick(forgotPwd);
		Thread.sleep(6000);
		validatePartialURL(url, "Forgot Pwd leads to correct page");
	}

	public void onboardingRegistration(String elementName) {
		try {
			firstName.sendKeys(jr.GetElementValue(elementName + ".FirstName"));
			lastName.sendKeys(jr.GetElementValue(elementName + ".LastName"));
			email.sendKeys(jr.GetElementValue(elementName + ".Email") + getRandomNumber(100000)
					+ getRandomAlphaNumericString(3) + jr.GetElementValue(elementName + ".Company"));
			password.sendKeys(jr.GetElementValue(elementName + ".Password"));
			getDropDownByVisibleText(country, jr.GetElementValue(elementName + ".Country"));
			jsClick(registerNowBtn);
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillBusinessInformation(String elementName) {
		try {
			getAboutYourBusinessFirstAnswer(jr.GetElementValue(elementName + ".PromotionalEfforts")).click();
			util.jsClick(nxtBtn);
			Thread.sleep(2000);
			Assert.assertTrue(aboutYourBusinessSecondQtn.isDisplayed(),
					"About your business second quetion is not displayed");
			util.jsClick(nxtBtn);
			Thread.sleep(2000);
			util.getDropDownByVisibleText(country, jr.GetElementValue(elementName + ".Country"));
		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void businessInformationValidation() {
		try {
			Assert.assertTrue(leftmenuItem.size() == 3, "Left menu items are not there");
			Assert.assertTrue(aboutYourBusinessTitle.isDisplayed(), "About your business title is not displayed");
			Assert.assertTrue(aboutYourBusinessFirstQtn.isDisplayed(),
					"About your business first quetion is not displayed");
			Assert.assertTrue(aboutYourBusinessFirstAns.size() == 6,
					"Seven answers for first qtn in About your business is not displayed");
			for (WebElement element : aboutYourBusinessFirstAns) {
				element.click();
			}
			jsClick(nxtBtn);
			ExtentReportGeneration.test.info("Validated First question in service page");
			Assert.assertTrue(aboutYourBusinessSecondQtn.isDisplayed(),
					"About your business second quetion is not displayed");
			for (WebElement element : aboutYourBusinessSecondAns) {
				String text = element.getText();
				int strLen = text.length();
				String finalText = text.substring(0, strLen - 6).trim();
				System.out.println("text::::::::" + finalText);
				if (getAboutYourBusinessSecondAnswer(finalText).isDisplayed()) {
					getAboutYourBusinessSecondAnswerToggleBtn(finalText).click();
					if (getAboutYourBusinessSecondAnswerUrl(finalText).isDisplayed())
						getAboutYourBusinessSecondAnswerUrl(finalText).sendKeys("test");
					else
						logger.fail("URL text box for " + finalText + " is not displayed");
				} else
					logger.fail("Answers for second qtn is not displayed");
			}
			jsClick(nxtBtn);
			ExtentReportGeneration.test.info("Validated Second question in Clients page");
			Assert.assertTrue(aboutYourBusinessThirdQtn.isDisplayed(),
					"About your business Third quetion is not displayed");
			Assert.assertTrue(country.isDisplayed(), "country dropdown is not displayed");
			// if(aboutYourBusinessLicencedCheckbox.isDisplayed())
			// aboutYourBusinessLicencedCheckbox.click();
			// else
			// logger.fail("Licenced checkbox is not displayed");
			if (aboutYourBusinessAddAnotherCountry.isDisplayed()) {
				util.getDropDownByVisibleText(country, "Australia");
				aboutYourBusinessAddAnotherCountry.click();
				util.getDropDownByVisibleText(country, "Australia");
			} else
				logger.fail("Add another country is not displayed");
			jsClick(nxtBtn);
			ExtentReportGeneration.test.info("Validated Third question in regions page");
			Assert.assertTrue(aboutYourBusinessCompletionStatus.isDisplayed(),
					"About your business status is not updated to completed ");
			Assert.assertTrue(personalDetailsTitle.isDisplayed(), "Personal Details title is not displayed");
		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillpersonalDetails(String elementName) {
		try {
			Assert.assertTrue(personalDetailsTitle.isDisplayed(), "Personal Details title is not displayed");
			dayDOB.sendKeys(jr.GetElementValue(elementName + ".Day"));
			monthDOB.sendKeys(jr.GetElementValue(elementName + ".Month"));
			yearDOB.sendKeys(jr.GetElementValue(elementName + ".Year"));
//			Assert.assertTrue(contactDetails.isDisplayed(), "Contact Details title is not displayed");
			fillAddressAndContactDetails(elementName);
		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillAddressAndContactDetails(String elementName) {
		try {
			address.sendKeys(jr.GetElementValue(elementName + ".Address"));
			int size = addressList.size();
			if (size != 0) {
				Thread.sleep(3000);
				addressList.get(0).click();
				Thread.sleep(3000);
			}
			else {
				Thread.sleep(3000);
				useThisAddress.click();
			}
			address2.sendKeys(jr.GetElementValue(elementName + ".Address2"));
			city.sendKeys(jr.GetElementValue(elementName + ".City"));
			postCode.sendKeys(jr.GetElementValue(elementName + ".Postcode"));
			phone.sendKeys(jr.GetElementValue(elementName + ".Phone"));
		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void fillIdentityVerification(String elementName, int numOfDocuments) {
		try {
			Assert.assertTrue(identityVerificationInprogressStatus.isDisplayed(),
					"Identity Verification is not Selected");
			selectIDfromDropdown(elementName);
			Thread.sleep(1000);
			attachFile(numOfDocuments);

		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void attachFile(int numOfDocuments) {
		try {
			for (int i = 1; i <= numOfDocuments; i++) {
				chooseFile.sendKeys(path + fs + "files" + fs + "uploadfile"+i+".png");
				Thread.sleep(1000);
//				attachFile(numOfDocuments);
			}

		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail("Failed in attaching file");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void selectIDfromDropdown(String elementName) {
		try {
			String selectedID = jr.GetElementValue(elementName + ".idType");
			util.getDropDownByVisibleText(idType, selectedID);
			if (selectedID.contains("Passport")) {
				Thread.sleep(500);
				Assert.assertTrue(passportImg.isDisplayed(), "Passport sample image is not displayed");
			} else if (selectedID.contains("Driver's licence")) {
				Assert.assertTrue(driverLicenseFrontImg.isDisplayed(), "Driver's licence front image is not displayed");
				Assert.assertTrue(driverLicenseBackImg.isDisplayed(), "Driver's licence Back image is not displayed");
			} else if (selectedID.contains("Proof of residency"))
				Assert.assertTrue(proofOfResidencyImg.isDisplayed(),
						"Proof of residency sample image is not displayed");
		} catch (Exception e) {
			logout();
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void clickOnboardingSignUpLink(String url) throws InterruptedException {
		try {
			isDisplayed(lnkSignUp, "Sign Up Link Displayed");
			util.jsClick(lnkSignUp);
			Thread.sleep(3000);
			ExtentReportGeneration.test.log(Status.INFO, "clicked on sign up link");
			logger.info("clicked on sign up link");
			validatePartialURL(url, "Sign Up URL leads to correct page");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("clickOnboardingSignUpLink failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("clickOnboardingSignUpLink failed");
			logger.fail("exception is " + e);
		}
	}

	public void logout() {
		try {
			Thread.sleep(2000);
//			util.jsClick(menuBtn);
			isDisplayed(logout, "logout button displayed");
			logout.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Onboarding Logout failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Onboarding Logout failed");
			logger.fail("exception is " + e);
		}
	}
}
