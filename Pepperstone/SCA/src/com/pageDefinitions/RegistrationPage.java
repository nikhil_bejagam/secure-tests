package com.pageDefinitions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.aventstack.extentreports.*;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class RegistrationPage extends UtilityLibrary {
	static ExtentTest test;
	static ExtentReports report;


	public TestLogger logger = new TestLogger();
	
	@FindBy(how = How.NAME, using = "legalEntity")
	WebElement accountType;

	@FindBy(how = How.NAME, using = "country")
	WebElement countryOfResidence;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'username')]")
	WebElement emailAddress;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'password')]")
	WebElement password;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'confirmPassword')]")
	WebElement confirmPassword;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'firstName')]")
	WebElement firstName;

	@FindBy(how = How.XPATH, using = "//input[contains(@name,'lastName')]")
	WebElement laststName;

	@FindBy(how = How.XPATH, using = "//input[contains(@placeholder,'Mobile phone')]")
	WebElement mobileNumber;

	@FindBy(how = How.XPATH, using = "//button/span[text()='register']")
	WebElement btnRegister;

	@FindBy(how = How.XPATH, using = "//input[@name='overEighteen']")
	WebElement chkBoxOverEighteen;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Sign up')]")
	WebElement signUp;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Introducing brokers')]/parent::div//following-sibling::div/span[contains(text(),'Sign up now')]")
	WebElement signUpNowIntroducingBroker;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Joint')]/preceding-sibling::input[@type='radio']")
	WebElement rdbtnJoint;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Company')]/preceding-sibling::input[@type='radio']")
	WebElement rdbtnCompany;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Trust or Superannuation Fund')]/preceding-sibling::input[@type='radio']")
	WebElement rdbtnTrust;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Individual Trustee(s)')]/preceding-sibling::input[@type='radio']")
	WebElement rdbtnIndividualTrustee;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Corporate Trustee')]/preceding-sibling::input[@type='radio']")
	WebElement rdbtnCorporateTrustee;

	@FindBy(how = How.XPATH, using = "//p[text()='Your Pepperstone Demo Account is ready.']")
	WebElement demoAccVldtn;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Next')]")
	WebElement nxtBtn;

	@FindBy(how = How.XPATH, using = "//label[(text()='Primary applicant')]")
	WebElement primaryApplicant;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Accept')]")
	WebElement accept;
	
	@FindAll(@FindBy(xpath = "//div[@id='appPageSectionContainer']//span[text()='Verification']"))
	List<WebElement> VerificationLabel;
	
	@FindBy(how = How.CSS, using = ".change-language-icon")
	WebElement changeLanguage;
	
	@FindAll(@FindBy(css = ".change-language li"))
	List<WebElement> languages;
	
	@FindBy(how = How.XPATH, using = "//header/h1")
	WebElement header;
	
	/****
	 * Method: Registration method Parameters: String --- this should same as
	 * the element in JSON file Author: Dhivya Gunasekaran
	 * @return 
	 ****/
	public String Registration(String elementName) {
		String generatedEmailAddress = null;
		JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
		try {
			Thread.sleep(1000);
			String account = (jr.GetElementValue(elementName + ".AccountType"));
			getDropDownByVisibleText(accountType, account);
			if (account.equalsIgnoreCase("Demo")) {
				chkBoxOverEighteen.click();
				Thread.sleep(1000);
			}
			if (!jr.GetElementValue(elementName + ".AccountType").contains("Demo")){
			getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
			}
			generatedEmailAddress = jr.GetElementValue(elementName + ".Email") + getRandomNumber(100000) + getRandomAlphaNumericString(3)
			+ jr.GetElementValue(elementName + ".Company");
			emailAddress.sendKeys(generatedEmailAddress);
			password.sendKeys(jr.GetElementValue(elementName + ".Password"));
			confirmPassword.sendKeys(jr.GetElementValue(elementName + ".ConfirmPassword"));
			ExtentReportGeneration.test.log(Status.INFO,"Registered with the email -"+emailAddress.getAttribute("value"));
			ExtentReportGeneration.test.log(Status.INFO,"Password -" +password.getAttribute("value"));
			if (!account.equalsIgnoreCase("Individual") && !account.equalsIgnoreCase("Introducing Broker")) {
				firstName.sendKeys(jr.GetElementValue(elementName + ".FirstName"));
				laststName.sendKeys(jr.GetElementValue(elementName + ".LastName"));
				int contactNumber = getRandomNumber(900000000) + 100000000;
				mobileNumber.sendKeys(Integer.toString(contactNumber));

			}
			Thread.sleep(2000);
			btnRegister.click();
			ExtentReportGeneration.test.log(Status.INFO,"RegistrationPage - Registration :Entered details and clicked Register button");
			logger.info("RegistrationPage - Registration :Entered details and clicked Register button");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
		return generatedEmailAddress;
	}

	public void AccountValidation(String account) {
		try {
			switch (account) {
			case "Joint":
				Assert.assertTrue(rdbtnJoint.isSelected(), "Radio button is not checked");
				nxtBtn.click();
				Thread.sleep(5000);
				Assert.assertTrue(primaryApplicant.isDisplayed(), "Primary Applicant is not displayed");
				break;
			case "Company":
				Assert.assertTrue(rdbtnCompany.isSelected(), "Radio button is not checked");
				nxtBtn.click();
				Thread.sleep(5000);
				Assert.assertTrue(primaryApplicant.isDisplayed(), "PrimaryApplicant text is not displayed");
				break;
			case "Trust":
				Assert.assertTrue(rdbtnTrust.isSelected(), "Radio button is not checked");
				Assert.assertTrue(rdbtnIndividualTrustee.isSelected(), "Radio button is not checked");
				nxtBtn.click();
				Thread.sleep(5000);
				Assert.assertTrue(primaryApplicant.isDisplayed(), "Primary Applicant is not displayed");
				break;
			case "Trust Corporate":
				Assert.assertTrue(rdbtnTrust.isSelected(), "Radio button is not checked");
				Assert.assertTrue(rdbtnCorporateTrustee.isSelected(), "Radio button is not checked");
				nxtBtn.click();
				Thread.sleep(5000);
				Assert.assertTrue(primaryApplicant.isDisplayed(), "Primary Applicant is not displayed");
				break;
			case "Demo":
				Assert.assertTrue(demoAccVldtn.isDisplayed(), "Primary Applicant is not displayed");
				break;
			}

		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Registration failed");
			logger.fail("Registration failed");
		}
	}

	public void SignUpAsIntroducingBrokerFromGetStartedPage(String elementName) {
		try {
			driver.get("https://pepperstonepartners.com/get-started");
			if(accept.isDisplayed())
			jsClick(accept);				
			jsClick(signUp);
			jsClick(signUpNowIntroducingBroker);
			String account = getFirstSelectedOption(accountType);
			if (account.equals("Introducing Broker"))
				Registration(elementName);
			else{
				ExtentReportGeneration.test.fail("Account type is not correct");
				logger.fail("Account type is not correct");
			}
		} catch (Exception e) {
			ExtentReportGeneration.test.fail(e);
			logger.fail("exception : " + e.getMessage());
		}
	}
	public void translationsWithDifferentLanguages() {
		try {
			changeLanguage.click();
			for(int i=1; i<14; i++){
				if(i>1)
					changeLanguage.click();
				Thread.sleep(1000);
				String selectlanguage = languages.get(i).getText();
				Writer writer = new PrintWriter("files\\new.txt", "UTF-8");
				writer.write(selectlanguage);
				writer.close();
				BufferedReader bufferedReader = new BufferedReader(new FileReader("files\\new.txt"));
				String lang = bufferedReader.readLine();
				
			ExtentReportGeneration.test.info("language selected is "+lang);
			System.out.println(selectlanguage);
			languages.get(i).click();
			System.out.println(header.getText());
			
			if (selectlanguage.contains("Español")&&!selectlanguage.contains("Español (LATAM)"))
				Assert.assertTrue(header.getText().contains("Registro de cuenta"), "Failed for Español");
			else if (selectlanguage.contains("Русский"))
				Assert.assertTrue(header.getText().contains("Регистрация счета"), "failed for Русский");
			else if (selectlanguage.contains("中文"))
				Assert.assertTrue(header.getText().contains("账户注册"), "Failed for 中文");
			else if (selectlanguage.contains("Tiếng Việt"))
				Assert.assertTrue(header.getText().contains("Đăng ký tài khoản"), "Failed for Tiếng Việt");
			else if (selectlanguage.contains("العربية"))
				Assert.assertTrue(header.getText().contains("تسجيل الحساب"), "Failed for العربية");
			else if (selectlanguage.contains("ไทย"))
				Assert.assertTrue(header.getText().contains("การลงทะเบียนบัญชี"), "failed for ไทย");
			else if (selectlanguage.contains("Italiano"))
				Assert.assertTrue(header.getText().contains("Registrazione conto"), "failed for Italiano");
			else if (selectlanguage.contains("Français"))
				Assert.assertTrue(header.getText().contains("Création d'un compte"), "Failed for Français");
			else if (selectlanguage.contains("Deutsch"))
				Assert.assertTrue(header.getText().contains("Kontoregistrierung"), "failed for Deutsch");
			else if (selectlanguage.contains("Polski"))
				Assert.assertTrue(header.getText().contains("Rejestracja rachunku"),"Failed for Polski");
			else if (selectlanguage.contains("Español (LATAM)")&&!selectlanguage.contains("Español"))
				Assert.assertTrue(header.getText().contains("Registro de cuenta"), "Failed for Español (LATAM)");
			else if (selectlanguage.contains("Türkçe"))
				Assert.assertTrue(header.getText().contains("Hesap Kaydı"), "Failed for Türkçe");
			else if (selectlanguage.contains("Portugues do Brasil"))
				Assert.assertTrue(header.getText().contains("Registro de conta"),"Failed for Portugues do Brasil");
			ExtentReportGeneration.test.pass("Translations are properly appearing on RegistrationPage for "+lang);
			}
			changeLanguage.click();
			languages.get(0).click();
			driver.navigate().refresh();
			
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Translations are broken for Homepage");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Translations are broken for Homepage");
			logger.fail("exception is " + e);
			changeLanguage.click();
			languages.get(0).click();
			driver.navigate().refresh();
		}
	}
}