package com.pageDefinitions;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class CysecPage extends UtilityLibrary {
	static ExtentTest test;
	static ExtentReports report;


	public TestLogger logger = new TestLogger();
	
	@FindBy(how = How.NAME, using = "legalEntity")
	WebElement accountType;

	@FindBy(how = How.NAME, using = "country")
	WebElement countryOfResidence;
	
	@FindAll(@FindBy(xpath = "//ul[@class='register-page__content-container-docs']/li/span"))
	List<WebElement> footerLinks;
	
	@FindBy(how = How.XPATH, using = "//input[@name='overEighteen']")
	WebElement chkBoxOverEighteen;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Trade with our European business']")
	WebElement importatntMessage;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Trade with our European business']")
	WebElement importatntInformationModelDialog;
	
	@FindBy(how = How.XPATH, using = "//button[@class='mdc-button ps-button ps-button--negative']")
	WebElement importatntInformationCloseButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='esma-footer']/p")
	WebElement footerNote;
	
	@FindBy(how = How.XPATH, using = "//section[@class='mdc-dialog__body mdc-dialog__body--scrollable']/a")
	WebElement importantInformationHereLink;
	
	 @FindBy(how = How.XPATH, using = "//span[@class='menu-item__side-text' and text()='Account']")
	 WebElement accountTab;
	     
	 @FindBy(how = How.XPATH, using = "//button[@class='mdc-fab add-account-fab']")
	 WebElement requestAnAccountDemoButton;
	     
	 @FindBy(how = How.XPATH, using = "//a[@class='mdc-tab']")
	 WebElement liveLink;
	     
	 @FindBy(how = How.XPATH, using = "//button[@class='mdc-fab add-account-fab add-account-fab--disabled']")
	 WebElement requestAnAccountLiveButton;
	 
	 @FindBy(how = How.XPATH, using = "//input[contains(@name,'username')]")
	 WebElement emailAddress;
		    
	 @FindBy(how = How.XPATH, using = "//input[contains(@name,'password')]")
	 WebElement password;
	 
	 @FindBy(how = How.XPATH, using = "//input[contains(@name,'confirmPassword')]")
	 WebElement confirmPassword;
	 
	 @FindBy(how = How.XPATH, using = "//button/span[text()='register']")
	 WebElement btnRegister;
	 
	 @FindBy(how = How.XPATH, using = "//div[@class='app-page__section-container-footer']")
	 WebElement verificationPageFooterNote;
	 
	 @FindBy(how = How.XPATH, using = "//button[@type='submit']")
		WebElement next;
	
	
	
	 
	public WebElement getVerificationPageFooterNote() {
		return verificationPageFooterNote;
	}

	public void verifyImportantAndFooterNoteOnRegistrationPage(String elementName) throws InterruptedException {
		JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
		try {
			Thread.sleep(1000);
			String account = (jr.GetElementValue(elementName + ".AccountType"));
			
			getDropDownByVisibleText(accountType, account);
			Thread.sleep(2000);
			getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
			Thread.sleep(2000);	
			String pagesource=driver.getPageSource();
			String actualImportantNote=importatntMessage.getText();
			Assert.assertTrue(pagesource.contains(actualImportantNote), "Failed :- Mismatch of Important Note between actual and expected.");
			importatntMessage.click();
			Thread.sleep(2000);
			
			String modelPopupTitleText=importatntInformationModelDialog.getText();
			Assert.assertTrue(pagesource.contains(modelPopupTitleText), "Failed :- Mismatch of Model Pop up Title Text between actual and expected.");
			importatntInformationCloseButton.click();
			
			String expectedFooter="Risk Warning: CFDs are complex instruments and come with a high risk of losing money rapidly due to leverage. The vast majority of retail client accounts lose money when trading in CFDs. You should consider whether you understand how CFDs work and whether you can afford to take the high risk of losing your money.";
			String actualFooterNote=footerNote.getText();
			System.out.println("Actual Footer Note ::"+actualFooterNote);
			Assert.assertTrue(expectedFooter.contains(actualFooterNote), "Failed :- Mismatch of Footer Note between actual and expected.");
			
			ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Registration :Entered EEA details and checked Important Notes.");
			ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Registration :Entered EEA details and checked Important Information Alert Dialog box.");
			ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Registration :Entered EEA details and checked Footer Notes.");
			logger.info("RegistrationPage - Registration :Entered EEA details and checked Footer Notes.");
		} catch (Exception e) {
		ExtentReportGeneration.test.fail(e.getMessage());
		logger.fail("exception : " + e.getMessage());
	}
}

	public void verifyFooterLinkOnRegistrationPage(String elementName) throws InterruptedException {
		JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
		try {
			Thread.sleep(1000);
			String account = (jr.GetElementValue(elementName + ".AccountType"));
			
			getDropDownByVisibleText(accountType, account);
			Thread.sleep(2000);
			getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
			Thread.sleep(2000);	
			String pagesource=driver.getPageSource();
						
			List<WebElement> listOfFooterLinks=footerLinks;
			assertTrue(footerLinks.size()==3,"Failed:- Mismatch of total number of Links present on Footer.");
			
			for(int i =0;i<listOfFooterLinks.size();i++) {
				String text=listOfFooterLinks.get(i).getText();
				System.out.println("Footer Link "+i+"::"+text);
				assertTrue(pagesource.contains(text),"Failed:- Link is not displayed.");
			}
			ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Registration :Entered EEA details and checked Footer Links.");
			logger.info("RegistrationPage - Registration :Entered EEA details and checked Footer Links.");
		} catch (Exception e) {
		ExtentReportGeneration.test.fail(e.getMessage());
		logger.fail("exception : " + e.getMessage());
	}
}
	
	public void verifyCountryURLOnRegistrationPage(String elementName) throws InterruptedException {
		JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
		try {
			Thread.sleep(1000);
			String account = (jr.GetElementValue(elementName + ".AccountType"));
			
			getDropDownByVisibleText(accountType, account);
			Thread.sleep(2000);
			getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
			Thread.sleep(2000);	
			
			importatntMessage.click();
			Thread.sleep(2000);
			String initialURL=driver.getCurrentUrl();
			importantInformationHereLink.click();
			Thread.sleep(5000);
			
			String actualURL=driver.getCurrentUrl();
			Assert.assertFalse(actualURL.equals(initialURL), "Failed :-Actual URL matches with Initial URL");
			
			ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Verify Cyprus country URL by clicking 'here' link on model popup .");
			logger.info("RegistrationPage - Registration :Verify Cyprus country URL by clicking 'here' link on model popup .");
		} catch (Exception e) {
		ExtentReportGeneration.test.fail(e.getMessage());
		logger.fail("exception : " + e.getMessage());
		}
	}
	
    public void registrationWithEEACountry(String elementName) throws InterruptedException {
        JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
         try {
             Thread.sleep(1000);
            String account = (jr.GetElementValue(elementName + ".AccountType"));
            getDropDownByVisibleText(accountType, account);
            Thread.sleep(2000);
            getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
            Thread.sleep(2000);    
            emailAddress.sendKeys(jr.GetElementValue(elementName + ".Email") + getRandomNumber(100000) + getRandomAlphaNumericString(3)+ jr.GetElementValue(elementName + ".Company"));
            password.sendKeys(jr.GetElementValue(elementName + ".Password"));
            confirmPassword.sendKeys(jr.GetElementValue(elementName + ".ConfirmPassword"));
            ExtentReportGeneration.test.log(Status.PASS,"Registered with the email -"+emailAddress.getAttribute("value"));
            ExtentReportGeneration.test.log(Status.PASS,"Password -" +password.getAttribute("value"));
            Thread.sleep(2000);
            btnRegister.click();
            ExtentReportGeneration.test.log(Status.INFO,"RegistrationPage - Registration :Entered details and clicked Register button");
           logger.info("RegistrationPage - Registration ::::Entered details and clicked Register button");
            
            ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Verify Registration with EEA country .");
           logger.info("RegistrationPage - Registration :::Verify Registration with EEA country.");
       } catch (Exception e) {
       ExtentReportGeneration.test.fail(e.getMessage());
       logger.fail("exception : " + e.getMessage());
       }
   }
   public void verifyRequestAccountButtonOnAccountTabForLiveAndDemoIsDisplayed() throws InterruptedException {
       accountTab.click();
       Thread.sleep(2000);
       Assert.assertTrue(isElementPresent(requestAnAccountDemoButton), "Failed:- Demo Request an Account button is not displayed.");
       liveLink.click();
       Assert.assertTrue(isElementPresent(requestAnAccountLiveButton), "Failed:- Live Request an Account button is not displayed.");
       ExtentReportGeneration.test.log(Status.PASS,"AccountTab - Request an Account button isdisplayed on both Live and Demo Link.");
    }
   
   public void veifyDeclarationLinks()  {
		try {
			waitForPageLoaded();
			scrollIntoView(next);
			List<WebElement> listOfLink=driver.findElements(By.xpath("//div[@class='legal-documents__form-fields']/div//span/a"));
			for(int i=0;i<listOfLink.size();i++) {
				String linkName=listOfLink.get(i).getText();
				logger.info("Link Name:::"+linkName);
				Assert.assertTrue(isElementPresent(listOfLink.get(i)),"Failed:- Link is not displayed.");
				
				listOfLink.get(i).click();
				waitForPageLoaded();
				ArrayList<String> tabs=new ArrayList<>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				
				String pagesource=driver.getPageSource();
				String actualurl=driver.getCurrentUrl();
				logger.info(actualurl);
				Assert.assertTrue(pagesource.contains(actualurl),"Failed:-Actual URL of document is not displayed.");
				
				driver.close();
				Thread.sleep(2000);
				driver.switchTo().window(tabs.get(0));
			}
			ExtentReportGeneration.test.log(Status.PASS, "Declaration Links Validation is Successful.");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to Validate Declaration Links.");
		}
	}
	
	
   


}
