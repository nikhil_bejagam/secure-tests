package com.pageDefinitions;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class SVGPage extends UtilityLibrary {
	public TestLogger logger = new TestLogger();
	UtilityLibrary util = new UtilityLibrary();
	JsonReader jr = new JsonReader("jsonFilePathverificationPage");
	private final String chineseHeading="账户注册";
	private final String expectedChineseRiskWarningMessage="风险警告: 交易衍生品具有风险。它并不适合所有人，您的损失可能远远超过您的初始投资。您并不拥有标的资产或对其拥有任何权利。过往表现并不能代表未来表现，并且税法可能会发生变化。此网站上的信息属于通用信息，并未考虑您或您客户的个人目标、财务状况或需求。在您做出任何交易决策之前，请阅读我们的法律文件并确保您完全理解风险我们鼓励您寻求独立建议. 本网站上的信息并不针对英国、欧洲、美国、澳大利亚的居民。并且如果此类分发或使用违反当地法律或法规，则本网站上的信息不得由此类国家或地区的任何人使用。";

	@FindBy(how = How.CSS, using = "[name='location']")
	WebElement Iblocation;
	
	@FindBy(how = How.CSS, using = "[name='clientsLocation']")
	WebElement clientsLocation;
	
	@FindBy(how = How.CSS, using = "[name='agreeSvgTermsAndConditions']")
	WebElement agreeSvgTermsAndConditionsChkbox;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@href,'SV-Partner-Agreement-CN.pdf')]")
	WebElement svgTermsAndConditionsLink;
	
	@FindBy(how = How.CSS, using = "[name='agreeAccuracyVerification']")
	WebElement agreeAccuracyVerification;
	
	@FindBy(how = How.CSS, using = "[name='chineseName']")
	WebElement chineseName;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'next')]")
	WebElement next;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'submit')]")
	WebElement submit;
	
	@FindBy(how = How.XPATH, using = "//p[contains(text(),'Thank you for completing')]")
	WebElement ibSubmissionMessage;
	
	@FindBy(how = How.CSS, using = "[data-name='cn_bank_svg_select']")
	WebElement cnbank;
	
	@FindBy(how = How.CSS, using = "[data-name='rpn_svg_upop']")
	WebElement unionPay;
	
	@FindBy(how = How.CSS, using = "[data-licence='asic']  .licence-card__more-info")
	WebElement withdrawalAU;
	
	@FindBy(how = How.CSS, using = "[data-licence='svg']  .licence-card__more-info")
	WebElement withdrawalSV;
	
	@FindBy(xpath = "//span[@class='menu-item__side-text' and text()='Contact Us']")
	WebElement contactUsTab;
	
	@FindAll(@FindBy(xpath = "//div[@class='contact-card mdc-card']"))
	List<WebElement> addressCardList;
	
	@FindBy(xpath = "//div[@class='contact-card mdc-card']//span[text()='St. Vincent & the Grenadines']")
	WebElement shanghaiAddressCard;
	
	@FindBy(xpath = "//span[text()='Account']")
	WebElement accountTab;
	
	@FindBy(xpath = "//a[@class='mdc-tab mdc-ripple-upgraded']")
	WebElement accountDemoTab;
	
	@FindBy(xpath = "//button[@id='addAccBtn']")
	WebElement accountDemoPlusButton;
	
	@FindBy(xpath = "//button[@id='addAccBtn']//span[text()='Request an account']")
	WebElement requestAnAccountButton;
	
	@FindAll(@FindBy(xpath = "//div[@class='create-account-modal-content__form']/div//p"))
	List<WebElement> requestAnIndividualDemoAccountList;
	
	@FindAll(@FindBy(xpath = "//div[@class='create-account-modal-content__form']/div//span"))
	List<WebElement> requestAnIndividualDemoLabelList;
	
	@FindBy(xpath = "//button[@class='mdc-button ps-button ps-button--negative']/span[text()='cancel']")
	WebElement requestAnAccountCancelButton;
	
	@FindBy(how = How.NAME, using = "legalEntity")
	WebElement accountType;
	
	@FindBy(how = How.NAME, using = "country")
	WebElement countryOfResidence;
	
	@FindBy(how = How.XPATH, using = "//div[@class='esma-footer']/p")
	WebElement riskWarningNote;
	
	@FindBy(how = How.XPATH, using = "//span[text()='中文']")
	WebElement selectchineseLanguage;
	
	@FindBy(how = How.CSS, using = ".change-language-icon")
	WebElement selectLanguage;
	
	@FindBy(how = How.XPATH, using = "//div[@class='change-language']//span[text()='English']")
	WebElement selectEnglishLanguage;
	
	@FindAll(@FindBy(xpath = "//div[@class='mdc-layout-grid__cell mdc-layout-grid__cell--span-4 mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-mobile']/span"))
	List<WebElement> contactLinks;
	
	@FindBy(how = How.XPATH, using = "//h1[text()='Account Registration']")
	WebElement englishAccountRegistration;
	
	@FindBy(how = How.CSS, using = "header h1")
	WebElement chineseAccountRegistration;
	
	@FindAll(@FindBy(xpath = "//ul[@class='register-page__content-container-docs']/li/span/span/a"))
	List<WebElement> footerLinks;
	
	@FindBy(how = How.XPATH, using = "//div[@class='app-page__section-container-footer']/p")
	WebElement riskWarningMessageVerificationPage;
	
	@FindAll(@FindBy(xpath = "//div[@class='register-page__form-footer']//a"))
	List<WebElement> footerListOfLinks;
	
	@FindBy(how = How.XPATH, using = "//div[@class='register-page__form-footer']//p//span/a")
	WebElement privacyPolicyLink;
	
	@FindBy(how = How.XPATH, using = "//div[@class='register-page__form-footer']//a/span")
	WebElement loginHereLink;
	
	@FindBy(how = How.XPATH, using = "//div[@class='login-page__dialog-footer']/div/a")
	WebElement lnkSignUp;
	
	@FindBy(how = How.XPATH, using = "//h1[@class='mdc-dialog__header__title']/span[text()='Onboarding Disclaimer']")
	WebElement onBoardingDisclaimerTitle;
	
	@FindBy(how = How.XPATH, using = "//button[@class='mdc-button ps-button ps-button--negative']")
	WebElement onBoardingDisclaimerCloseButton;
	
	
	public WebElement getOnBoardingDisclaimerCloseButton() {
		return onBoardingDisclaimerCloseButton;
	}

	public WebElement getLnkSignUp() {
		return lnkSignUp;
	}

	public WebElement getLoginHereLink() {
		return loginHereLink;
	}

	public WebElement getPrivacyPolicyLink() {
		return privacyPolicyLink;
	}
	
	public WebElement getRiskWarningMessageVerificationPage() {
		return riskWarningMessageVerificationPage;
	}

	public WebElement getChineseAccountRegistration() {
		return chineseAccountRegistration;
	}


	public WebElement getEnglishAccountRegistration() {
		return englishAccountRegistration;
	}

	public WebElement getSelectEnglishLanguage() {
		return selectEnglishLanguage;
	}
	public WebElement getRiskWarningNote() {
		return riskWarningNote;
	}
	public WebElement getRequestAnAccountCancelButton() {
		return requestAnAccountCancelButton;
	}
	public WebElement getRequestAnAccountButton() {
		return requestAnAccountButton;
	}
	public WebElement getAccountDemoPlusButton() {
		return accountDemoPlusButton;
	}
	public WebElement getAccountDemoTab() {
		return accountDemoTab;
	}
	public WebElement getAccountTab() {
		return accountTab;
	}
	public WebElement getShanghaiAddressCard() {
		return shanghaiAddressCard;
	}
	public WebElement getContactUsTab() {
		return contactUsTab;
	}
	public void fillChineseName(String elementName) {
		try {
			chineseName.sendKeys(jr.GetElementValue(elementName + ".ChineseName"));
			ExtentReportGeneration.test.log(Status.INFO,"Provided Chinese Name");
			
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to fill chinese Name");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	public void fillIBDetails(String elementName) {
		try {
			getDropDownByVisibleText(Iblocation, jr.GetElementValue(elementName + ".IbLocation"));
			getDropDownByVisibleText(clientsLocation, jr.GetElementValue(elementName + ".ClientLocation"));
			Thread.sleep(2000);
			util.scrollIntoView(next);
			util.jsClick(next);
			//next.click();
			
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to fill IB details");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	public void fillprogramInfo() {
		try {
			agreeSvgTermsAndConditionsChkbox.click();
			agreeAccuracyVerification.click();
			Thread.sleep(2000);
			next.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to fill program Info");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	public void validateSVGtermsandConditionNavigation() {
		try {
			waitForPageLoaded();
			Thread.sleep(1000);
			svgTermsAndConditionsLink.click();
			Thread.sleep(2000);
			SwitchAndCloseWindow("ST_VINCENTS/SV-Partner-Agreement");	
			ExtentReportGeneration.test.pass("Navigating to SVG terms and condition page sucessfully");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to Navigate to SVG terms and condition page.");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	public void submitPage() {
		try {
			submit.click();
			ExtentReportGeneration.test.info("Able to submit sucessfully");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Unable to submit");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	public void validateIBSubmission() {
		try {
			Assert.assertEquals(true, isElementPresent(ibSubmissionMessage));
			ExtentReportGeneration.test.pass("IB application Completed");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to complete IB application");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	public void validateAddfundsoption() throws InterruptedException {
		try {
			Thread.sleep(2000);
			getDynamicFundNow("AccountChina").click();
			Assert.assertEquals(true, isElementPresent(unionPay));
			Assert.assertEquals(true, isElementPresent(cnbank));
		} catch (AssertionError e) {
			ExtentReportGeneration.test.fail("Failed to see unionPay and ChineseBank");
		}
	}
	
	 public void verifyAddressCard() throws InterruptedException {
		   Thread.sleep(3000);	
		   JavascriptExecutor js = (JavascriptExecutor) driver;  
		   js.executeScript("arguments[0].click();",getContactUsTab());
		  // getContactUsTab().click();
		   List<WebElement> listOfAddress=addressCardList;
		   String address=getShanghaiAddressCard().getText();
		   for(int i=0;i<listOfAddress.size();i++) {
			   if(listOfAddress.get(i).getText().contains(address)) {
				   String actualAddress=listOfAddress.get(i).getText();
				   logger.info(actualAddress);
				   boolean expectedAddressCard=expectedAddress(actualAddress);
				   Assert.assertTrue(expectedAddressCard,"Failed :- Mismatch of Address with pagesource.");
			   }
		   }
		  ExtentReportGeneration.test.log(Status.PASS, "Validating  SVG Address card in contact us page.");
	   }
	   
	   public boolean expectedAddress(String actualAddress) {
		   boolean flag=false;
		   ArrayList<String> list=new ArrayList<String>();
		   list.add("St. Vincent & the Grenadines");
		   list.add("Pepperstone Group Limited");
		   
		   list.add("Phone");
		   list.add("+86 21 8024 6133");
		   list.add("Email");
		   list.add("support.cn@pepperstone.com");
		   
		   for (int i=0;i<list.size();i++) {
				 if(actualAddress.contains(list.get(i))) {
					flag= true;
				}
			  }
		   return flag;
		   
	   }
	   
	   public void verifyRequestAnIndividualDemoAccount() throws InterruptedException {
		   Thread.sleep(3000);
		   JavascriptExecutor js = (JavascriptExecutor) driver;  
		   js.executeScript("arguments[0].click();",getAccountTab());
		   getAccountDemoTab().click();
		   Actions action=new Actions(driver);
		   action.moveToElement(getAccountDemoPlusButton()).build().perform();
		   Thread.sleep(2000);
		   util.jsClick(getRequestAnAccountButton());
		   Thread.sleep(2000);
		   List<WebElement> listOfDemoAccountFields=requestAnIndividualDemoAccountList;
		   List<WebElement> listOfDemoAccountLabel=requestAnIndividualDemoLabelList;
		   int actualListSize=listOfDemoAccountFields.size();
		   
		   assertFalse(actualListSize==6,"Failed:- Mismatch of list of Demo Account Fields.");
		   
		   for(int i=0;i<listOfDemoAccountLabel.size();i++) {
			 String actualField=listOfDemoAccountLabel.get(i).getText();
			 logger.info(actualField);
		   }
		   Assert.assertFalse(listOfDemoAccountLabel.contains("Leverage"),"Failed :- Leverage Field is present on Request an Individual Demo Account.");
		   getRequestAnAccountCancelButton().click();
		   ExtentReportGeneration.test.log(Status.PASS, "Validating  Leverage option is removed from Request an Individual Demo Account model popup..");
	   }
	   
	   public void verifyRiskWarningMessageInBothLanguage(String elementName) throws InterruptedException {
			JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
			try {
				Thread.sleep(1000);
				String account = (jr.GetElementValue(elementName + ".AccountType"));
				
				getDropDownByVisibleText(accountType, account);
				Thread.sleep(2000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);	
								
				String expectedFooter="Risk Warning: Trading derivatives is risky. It isn't suitable for everyone and you could lose substantially more than your initial investment. You don't own or have rights in the underlying assets. Past performance is no indication of future performance and tax laws are subject to change. The information on this website is general in nature and doesn't take into account you or your client's personal objectives, financial circumstances, or needs. Please read our legal documents and ensure you fully understand the risks before you make any trading decisions. We encourage you to seek independent advice. The information on this site is not intended for residents of the United Kingdom, Europe, the United States, Australia, or use by any person in any country or jurisdiction where such distribution or use would be contrary to local law or regulation.";
				String actualFooterNote=getRiskWarningNote().getText();
				System.out.println("Actual Footer Note ::"+actualFooterNote);
				System.out.println(expectedFooter);
				Assert.assertTrue(actualFooterNote.equals(expectedFooter), "Failed :- Mismatch of warning message between actual and expected.");
				
				selectLanguage.click();
				selectchineseLanguage.click();
				Thread.sleep(3000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);	
								
				//String expectedChineseRiskWarningMessage="风险警告: 交易衍生品具有风险。它并不适合所有人，您的损失可能远远超过您的初始投资。您并不拥有标的资产或对其拥有任何权利。过往表现并不能代表未来表现，并且税法可能会发生变化。此网站上的信息属于通用信息，并未考虑您或您客户的个人目标、财务状况或需求。在您做出任何交易决策之前，请阅读我们的法律文件并确保您完全理解风险我们鼓励您寻求独立建议. 本网站上的信息并不针对英国、欧洲、美国、澳大利亚的居民。并且如果此类分发或使用违反当地法律或法规，则本网站上的信息不得由此类国家或地区的任何人使用。";
				String actualChineseRiskWarningMessage=getRiskWarningNote().getText();
				System.out.println("Actual Footer Note ::"+actualChineseRiskWarningMessage);
				Assert.assertTrue(expectedChineseRiskWarningMessage.contains(actualChineseRiskWarningMessage), "Failed :- Mismatch of Risk warning message between actual and expected.");
				
				selectLanguage.click();
				Actions action=new Actions(driver);
				action.moveToElement(getSelectEnglishLanguage()).build().perform();
				getSelectEnglishLanguage().click();
				Thread.sleep(2000);
				driver.navigate().refresh();
				Thread.sleep(4000);
				
				ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Registration :Entered SVG details and checked Risk warning message in English and Chinese Language.");
				logger.info("RegistrationPage - Registration :Entered SVG details and checked Risk warning message in both Language.");
			} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}

	   
	   public void verifyAddressAndPhoneNumberOnRegistrationPage(String elementName) throws InterruptedException {
			JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
			try {
				Thread.sleep(1000);
				String account = (jr.GetElementValue(elementName + ".AccountType"));
				
				getDropDownByVisibleText(accountType, account);
				Thread.sleep(2000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);	
				
							
				List<WebElement> listOfContactLinks=contactLinks;
				assertTrue(contactLinks.size()==2,"Failed:- Mismatch of total number of Links present on Footer.");
				
				for(int i =0;i<listOfContactLinks.size();i++) {
					String text=listOfContactLinks.get(i).getText();
					System.out.println("Contact Link "+i+"::"+text);
					boolean verifyContactUsOnRegistrationPage=verifyContact(text);
					assertTrue(verifyContactUsOnRegistrationPage,"Failed:- Contacts is not displayed.");
				}
				ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Address on Account Registration Page with SVG Country.");
				logger.info("RegistrationPage - Registration :Verify Address on Account Registration Page with SVG Country.");
			} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	   public boolean verifyContact(String actualContact) {
		   boolean flag=false;
		   ArrayList<String> list=new ArrayList<String>();
		   list.add("First Floor, First St Vincent Bank Ltd");
		   list.add("James Street, P.O Box 1574");
		   list.add("Kingstown, VC0100");
		   list.add("Saint Vincent and Grenadines");
		   list.add("+86 21 8024 6133");
		   for (int i=0;i<list.size();i++) {
				 if(actualContact.contains(list.get(i))) {
					flag= true;
				}
			  }
		   return flag;
	   }
	   
	   public void verifyFooterLink(String elementName) {
		   JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
			try {
				
				ExtentReportGeneration.test.log(Status.INFO,"Verify Footer Links...");
				Thread.sleep(2000);
				
				String account = (jr.GetElementValue(elementName + ".AccountType"));
				getDropDownByVisibleText(accountType, account);
				Thread.sleep(2000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);
				String english=getEnglishAccountRegistration().getText();
				clickFooterLinksAndVerifyUrl(english);
				scrollIntoView(selectLanguage);
				
				selectLanguage.click();
				selectEnglishLanguage.click();
				ExtentReportGeneration.test.log(Status.PASS,"Select Language is changed to Chinese language.");
				Thread.sleep(3000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);	
				String chinese=getChineseAccountRegistration().getText();
				clickFooterLinksAndVerifyUrl(chinese);
				
				util.scrollIntoView(selectLanguage);
				util.jsClick(selectLanguage);
				Actions action=new Actions(driver);
				action.moveToElement(getSelectEnglishLanguage()).build().perform();
				getSelectEnglishLanguage().click();
				ExtentReportGeneration.test.log(Status.PASS,"Select Language is changed back to English language.");
				Thread.sleep(2000);
				driver.navigate().refresh();
				Thread.sleep(4000);
				ExtentReportGeneration.test.log(Status.PASS,"RegistrationPage - Footer links(Legal Documentations,Privacy policy and Website terms & conditions) on Registration Page with SVG Country.");
				logger.info("RegistrationPage - Registration :Verify Footer links(Legal Documentations,Privacy policy and Website terms & conditions) on Registration Page with SVG Country.");
			} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to click Footer links in both the language.");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	   }
	   
	   public void clickFooterLinksAndVerifyUrl(String accountRegistrationHeading) throws InterruptedException {
		   try {
				   util.scrollIntoView(getPrivacyPolicyLink());
				   List<WebElement> listOfFooterLinks=footerLinks;
				   assertTrue(footerLinks.size()==2,"Failed:- Mismatch of total number of Links present on Footer.");
				   logger.info("Success::Footer Link size== 2.");
				   ExtentReportGeneration.test.log(Status.PASS,"Success :: Total Footer Link is count is 3.");
				   Thread.sleep(2000);
				   if(accountRegistrationHeading.contains("Account Registration")) {
				        for(int i =0;i<listOfFooterLinks.size();i++) {
					    String text=listOfFooterLinks.get(i).getText();
						logger.info("Footer Link "+i+"::"+text);
						Thread.sleep(2000);				
						WebElement elementToHover=listOfFooterLinks.get(i);
						Actions action=new Actions(driver);
						action.moveToElement(elementToHover).build().perform();
						util.jsClick(listOfFooterLinks.get(i));
						waitForPageLoaded();
						Thread.sleep(5000);
						logger.info("Pass :: Link is clicked .");
						ArrayList<String> tabs=new ArrayList<>(driver.getWindowHandles());
						driver.switchTo().window(tabs.get(1));
						Thread.sleep(5000);
						waitForPageLoaded();
						logger.info("Pass :: Navigate to next tab.");
						
						String actualurl=driver.getCurrentUrl();
						System.out.println(actualurl);
						boolean expectedPartialUrl=expectedEnglishPartialUrl(actualurl);
						assertTrue(expectedPartialUrl, "Failed:-Mismatch of URL between actual and expected.");
						ExtentReportGeneration.test.log(Status.PASS,"Success :: Actual URL contains Expected Partial URL.");
						driver.close();
						Thread.sleep(2000);
						driver.switchTo().window(tabs.get(0));
						logger.info("Pass :: Footer link is Clicked and navigate to next Tab tab and tab is closed.");
						ExtentReportGeneration.test.log(Status.PASS,"Success :: Footer Link is Clicked and navigate to next tab and tab is closed");
				      }
				   }
				      else if(accountRegistrationHeading.contains(chineseHeading)) {
					        for(int i =0;i<listOfFooterLinks.size();i++) {
						    String text=listOfFooterLinks.get(i).getText();
							logger.info("Footer Link "+i+"::"+text);
							logger.info("Pass ::Chinese Footer Link is displayed.");
							
							Actions action=new Actions(driver);
							action.moveToElement(listOfFooterLinks.get(i)).build().perform();
							util.jsClick(listOfFooterLinks.get(i));
							waitForPageLoaded();
							Thread.sleep(5000);
							logger.info("Pass ::Chinese:: Link is clicked .");
							ArrayList<String> tabs=new ArrayList<>(driver.getWindowHandles());
							driver.switchTo().window(tabs.get(1));
							Thread.sleep(5000);
							waitForPageLoaded();
							logger.info("Pass ::Chinese:: Navigate to next tab.");
							String actualurl=driver.getCurrentUrl();
							System.out.println(actualurl);
							boolean expectedUrl=expectedChinesePartialUrl(actualurl);
							assertTrue(expectedUrl, "Failed:-Mismatch of URL between actual and expected.");
							logger.info("Pass::Chinese::: Actual url matches with expected url.");
							driver.close();
							Thread.sleep(2000);
							driver.switchTo().window(tabs.get(0));
														
							logger.info("Pass ::Chinese::: Footer link is Clicked and navigate to next Tab tab and tab is closed.");
							ExtentReportGeneration.test.log(Status.PASS,"Success ::Chinese::: Footer Link is Clicked and navigate to next tab and tab is closed");
					      }
				   }
				   }catch (Exception e){
					   ExtentReportGeneration.test.fail("Failed to click to Footer links.");
						ExtentReportGeneration.test.fail(e.getMessage());
						logger.fail("exception : " + e.getMessage());
				   }				   
	   }
	   
	   public boolean expectedEnglishPartialUrl(String actualUrl) { 
		   boolean flag=false;
		   ArrayList<String> list=new ArrayList<String>();
			list.add("legal/ST_VINCENTS/SV-Terms.pdf");
			list.add("legal/ST_VINCENTS/SV-Privacy-Policy.pdf");
			list.add("legal/ST_VINCENTS/SV-Website-Terms.pdf");
			
			for (int i=0;i<list.size();i++) {
				 if(actualUrl.contains(list.get(i))) {
					flag= true;
				}
			  }
		   return flag;
	   }
	   
	   public boolean expectedChinesePartialUrl(String actualUrl) { 
		   boolean flag=false;
		   ArrayList<String> list=new ArrayList<String>();
			list.add("legal/ST_VINCENTS/SV-Terms-CN.pdf");
			list.add("legal/ST_VINCENTS/SV-Privacy-Policy-CN.pdf");
			list.add("legal/ST_VINCENTS/SV-Website-Terms-CN.pdf");
			
			for (int i=0;i<list.size();i++) {
				 if(actualUrl.contains(list.get(i))) {
					flag= true;
				}
			  }
		   return flag;
	   }
	   
	   public void verifyRiskWarningMessage(String elementName) throws InterruptedException {
			JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
			try {
				Thread.sleep(3000);
				scrollIntoView(getRiskWarningMessageVerificationPage());
				String expectedFooter="Risk Warning: For more information, please see our full legal documents here. Pepperstone Markets SV Limited is an international business company registered in St Vincent & the Grenadines (International Business Company Number 23485 IBC 2016). Pepperstone Markets SV Limited isn’t subject to the same regulatory oversight as other Pepperstone entities, so you won’t get the protections that you’d get if you traded with our other licensed businesses.";
				String actualFooterNote=getRiskWarningMessageVerificationPage().getText();
				logger.info("Actual Footer Note ::"+actualFooterNote);
				logger.info(expectedFooter);
				Assert.assertTrue(actualFooterNote.startsWith(expectedFooter), "Failed :- Mismatch of warning message between actual and expected.");
				
				ExtentReportGeneration.test.log(Status.PASS,"VerificationPage -  :Login with SVG user details and checke Risk warning message.");
				logger.info("VerificationPage - Registration :Login with SVG user details and checke Risk warning message.");
			} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	   
	   public void verifyLinksInBothLanguage(String elementName) throws InterruptedException {
			JsonReader jr = new JsonReader("jsonFilePathRegistrationPage");
			try {
				Thread.sleep(2000);
				String account = (jr.GetElementValue(elementName + ".AccountType"));
				getDropDownByVisibleText(accountType, account);
				Thread.sleep(2000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);	
				
				verifyLinks("english");
				selectLanguage.click();
				selectchineseLanguage.click();
				Thread.sleep(3000);
				getDropDownByVisibleText(countryOfResidence, jr.GetElementValue(elementName + ".Country"));
				Thread.sleep(2000);	
				verifyLinks("chinese");
				
				selectLanguage.click();
				Actions action=new Actions(driver);
				action.moveToElement(getSelectEnglishLanguage()).build().perform();
				getSelectEnglishLanguage().click();
				Thread.sleep(2000);
				driver.navigate().refresh();
				Thread.sleep(4000);
				
				ExtentReportGeneration.test.log(Status.PASS,"Privacy policy Links and Login here Links are working.");
				logger.info("Privacy policy Links and Login here Links are working");
			} catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
		}
	}
	   
	   public void verifyLinks(String language) throws InterruptedException {
		   try {
			   	List<WebElement> links=footerListOfLinks;
				Assert.assertTrue(links.size()==2,"Failed:-Mismatch of Link size.");
				Assert.assertTrue(getPrivacyPolicyLink().isDisplayed(), "Failed:-Privacy policy Link is not displayed");
				Assert.assertTrue(getLoginHereLink().isDisplayed(), "Failed:-Login hereLink is not displayed");
				scrollIntoView(getPrivacyPolicyLink());
				getPrivacyPolicyLink().click();
				waitForPageLoaded();
				ArrayList<String> tabs=new ArrayList<>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				
				String actualurl=driver.getCurrentUrl();
				System.out.println(actualurl);
				
				if(language.equalsIgnoreCase("english")) {
				boolean expectedPartialUrl=expectedEnglishPartialUrl(actualurl);
				assertTrue(expectedPartialUrl, "Failed:-Mismatch of URL between actual and expected.");
				ExtentReportGeneration.test.log(Status.PASS,"English::Privacy policy Link is successfully clicked.");
				}
				else if(language.equalsIgnoreCase("chinese")){
					boolean expectedPartialUrl=expectedChinesePartialUrl(actualurl);
					assertTrue(expectedPartialUrl, "Failed:-Mismatch of URL between actual and expected.");
					ExtentReportGeneration.test.log(Status.PASS,"Chinese::Privacy policy Link is successfully clicked.");
				}
				driver.close();
				Thread.sleep(2000);
				driver.switchTo().window(tabs.get(0));
				Thread.sleep(2000);
				util.scrollIntoView(loginHereLink);
				Actions action=new Actions(driver);
				action.moveToElement(loginHereLink).build().perform();
				util.waitForElementToBeClickable(loginHereLink);
				util.jsClick(loginHereLink);
				String actualURL=driver.getCurrentUrl();
				String expectedLogiURL="https://secure-beta.pepperstone.com/login";
				Assert.assertTrue(expectedLogiURL.contains(actualURL),"Failed:-Mismatch of URL.");
				ExtentReportGeneration.test.log(Status.PASS,"Login here Link is successfully clicked.");
				util.jsClick(getLnkSignUp());
		   }catch (Exception e){
			   ExtentReportGeneration.test.fail("Failed to click to Footer links.");
				ExtentReportGeneration.test.fail(e.getMessage());
				logger.fail("exception : " + e.getMessage());
		   }
	   }
	   
	   public void veifyDeclarationLinks()  {
			try {
				waitForPageLoaded();
				scrollIntoView(next);
				List<WebElement> listOfLink=driver.findElements(By.xpath("//div[@class='legal-documents__form-fields']/div//span/a"));
				Assert.assertEquals(listOfLink.size(), 4,"Failed:- Mismatch of Total number of Links under Declaration Section.");
				logger.info("Four Links are displayed.");
				ExtentReportGeneration.test.log(Status.PASS, "Four Links are displayed..");
				Thread.sleep(3000);
				listOfLink.get(0).click();
				Assert.assertTrue(onBoardingDisclaimerTitle.isDisplayed(), "Failed :- onBoarding Disclaimer Title is not displayed.");
				logger.pass("Success:- onBoarding Disclaimer Title is displayed");
				ExtentReportGeneration.test.log(Status.PASS, "onBoarding Disclaimer Link is clicked and modal box is displayed with Onbarding Disclaimer Title.");
				getOnBoardingDisclaimerCloseButton().click();
				for(int i=1;i<listOfLink.size();i++) {
					String linkName=listOfLink.get(i).getText();
					logger.info(linkName);
					Assert.assertTrue(isElementPresent(listOfLink.get(i)),"Failed:- Link is not displayed.");
					listOfLink.get(i).click();
					waitForPageLoaded();
					logger.pass("Links clicked and navigated to new tab.");
					
					 String mainWindow=driver.getWindowHandle();
					 Set<String> s1=driver.getWindowHandles();	
					 Iterator<String> iterator=s1.iterator();
					 while(iterator.hasNext())			
				        {		
				            String ChildWindow=iterator.next();		
				            		
				            if(!mainWindow.equalsIgnoreCase(ChildWindow))			
				            {    		
				            	driver.switchTo().window(ChildWindow);	
				            	Thread.sleep(3000);					
				            	String actualurl=driver.getCurrentUrl();
				            	System.out.println(actualurl);
				            	boolean partialUrl=expectedPartialUrl(actualurl);
				            	Assert.assertTrue(partialUrl,"Failed:-Link Name is not present on page source.");
				            	driver.close();
				            }
				        }
					driver.switchTo().window(mainWindow);
					logger.pass("Successfully Validated Links .");
					Thread.sleep(3000);
				}
				ExtentReportGeneration.test.log(Status.PASS, "Declaration Links Validation is Successful.");
			} catch (Exception e) {
				ExtentReportGeneration.test.fail("Failed to Validate Links.");
			}
		}
	   
	   public boolean expectedPartialUrl(String actualUrl) { 
		   boolean flag=false;
		   ArrayList<String> list=new ArrayList<String>();
			list.add("legal/ST_VINCENTS/SV-Terms.pdf");
			list.add("legal/ST_VINCENTS/SV-Risk-Disclosure.pdf");
			list.add("legal/ST_VINCENTS/SV-Privacy-Policy.pdf");
			
			for (int i=0;i<list.size();i++) {
				 if(actualUrl.contains(list.get(i))) {
					flag= true;
				}
			  }
		   return flag;
	   }
}