package com.pageDefinitions;

import java.text.DecimalFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.Status;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestConfig;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

import java_cup.runtime.virtual_parse_stack;

public class Payments extends UtilityLibrary {
	public TestLogger logger = new TestLogger();
	UtilityLibrary util = new UtilityLibrary();
	protected VerificationPage verificationpage = new VerificationPage();
	JsonReader jr = new JsonReader("jsonFilePathpaymentsPage");

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'fund now')]/..")
	WebElement fundNow;

	@FindBy(how = How.XPATH, using = "//div[@data-name='poli']")
	WebElement poli;

	@FindBy(how = How.XPATH, using = "//div[@data-name='commweb3Dvisa']")
	WebElement visa;

	@FindBy(how = How.XPATH, using = "//div[@data-name='neteller']")
	WebElement neteller;

	@FindBy(how = How.XPATH, using = "//div[@data-name='neteller_fca']")
	WebElement netellerFCA;
	
	@FindBy(how = How.XPATH, using = "//div[@data-name='skrill']")
	WebElement skrill;

	@FindBy(how = How.XPATH, using = "//div[@data-name='paypal']")
	WebElement paypal;

	@FindBy(how = How.NAME, using = "amount")
	WebElement amount;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'fund now')]/parent::button")
	WebElement FundNow;
	
	@FindBy(how = How.XPATH, using = "//button[@variant='negative']/following-sibling::button")
	WebElement demoFundNow;
	
	@FindBy(how = How.XPATH, using = "//button[@variant='negative']")
	WebElement close;

	@FindBy(how = How.ID, using = "proceed-button")
	WebElement continuebttn;

	@FindBy(how = How.NAME, using = "Username")
	WebElement username;

	@FindBy(how = How.NAME, using = "Password")
	WebElement password;
	
	@FindBy(how = How.NAME, using = "password")
	WebElement newpassword;

	@FindBy(how = How.NAME, using = "confirmPassword")
	WebElement confirmPassword;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Account')]/../../..//span[contains(text(),'balance')]/../../div[1]")
	WebElement balanceAmount;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Login')]/parent::button")
	WebElement login;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Continue')]/parent::button")
	WebElement continueButton;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Confirm')]/parent::button")
	WebElement confirm;

	@FindBy(how = How.ID, using = "returnToMerchantButton")
	WebElement returnTopepperstoneUAT;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Return to deposit')]")
	WebElement returnToDeposit;

	@FindBy(how = How.NAME, using = "cardNumber")
	WebElement cardOption;

	@FindBy(how = How.NAME, using = "cardHolderName")
	WebElement cardHolderName;

	@FindBy(how = How.NAME, using = "cardNumber")
	WebElement cardNumber;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Valid to')]/following-sibling::input")
	WebElement validTo;

	@FindBy(how = How.NAME, using = "cvc")
	WebElement CVV;

	@FindBy(how = How.NAME, using = "netAccount")
	WebElement netellerID;

	@FindBy(how = How.NAME, using = "secureId")
	WebElement netellersecureId;

	@FindBy(how = How.XPATH, using = "//a[@href='/accounts']")
	WebElement accounts;

	@FindBy(how = How.XPATH, using = "//a[@href='/wallet']")
	WebElement funds;

	@FindBy(how = How.XPATH, using = "//a[@href='/wallet/transfer']")
	WebElement transferFunds;
	
	@FindBy(how = How.XPATH, using = "//a[@href='/accounts/demo']")
	WebElement demo;
	
	@FindBy(how = How.XPATH, using = "//span[@id='myProfileMenuItem']//span[text()='log out']")
	WebElement logout;
	
	@FindBy(how = How.ID, using = "myProfileMenuItem")
	WebElement myProfileMenuItem;

	@FindBy(how = How.CSS, using = ".flash-message__action button")
	WebElement paymentError;

	@FindBy(how = How.CSS, using = "select[name='fromAccount']")
	WebElement fromAccount;

	@FindBy(how = How.CSS, using = "input[name='fromAmount']")
	WebElement fromAmount;

	@FindBy(how = How.CSS, using = "select[name='toAccount']")
	WebElement toAccount;

	@FindBy(how = How.CSS, using = "button[type='submit']")
	WebElement submit;

	@FindBy(how = How.CSS, using = "input[id='email']")
	WebElement paypalEmail;

	@FindBy(how = How.ID, using = "btnNext")
	WebElement paypalbtnNext;

	@FindBy(how = How.ID, using = "password")
	WebElement paypalpassword;

	@FindBy(how = How.ID, using = "btnLogin")
	WebElement paypalbtnLogin;

	@FindBy(how = How.ID, using = "confirmButtonTop")
	WebElement paypalpayNowBttn;
	
	@FindBy(how = How.XPATH, using = "//select")
	WebElement selectAmount;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Total amount']/following-sibling::span")
	WebElement currencyExchangeAmount;
	
	@FindAll(@FindBy(css = ".account-card__edit-toggle a"))
	List<WebElement> editBttn;
	
	@FindBy(how = How.CSS, using = ".change-password__email-confirmation button")
	WebElement resetPasswordBttn;
		
	@FindAll(@FindBy(xpath = "//span[contains(text(),'An email has been sent.')]"))
	List<WebElement> emailSentMessage;
	
	@FindAll(@FindBy(xpath = "//span[contains(text(),'Password changed successfully')]"))
	List<WebElement> passwordChangeSucessfullMsg;
	
	@FindAll(@FindBy(xpath = "//div[@class='ps-card-list']/div"))
	List<WebElement> liveAccount;
	
	@FindBy(how = How.XPATH, using = "//li[@class='account-details-modal-content__menu-item']//span[text()='Change Nickname']")
	WebElement changeNickNameLink;
	
	@FindBy(how = How.XPATH, using = "//div[@class='mdc-text-field mdc-text-field--upgraded']/input")
	WebElement changeNickNameInput;
	
	@FindBy(how = How.XPATH, using = "//button[@class='mdc-button ps-button ps-button--primary']")
	WebElement changeNickNameSubmitButton;
	
	

	/****
	 * Method: Payment Parameters: String --- this should same as the element in
	 * JSON file Author: Avi Shah
	 ****/
	public void depositeWithAccount(String elementName) {

		try {
			float beforeBalance = getBalanceOfAccount(jr.GetElementValue(elementName + ".AccountName"));
			ExtentReportGeneration.test.log(Status.INFO, "Current balance Amount is " + beforeBalance);
			getDynamicFundNow(jr.GetElementValue(elementName + ".AccountName")).click();
			pamentMethod(jr.GetElementValue(elementName + ".AccountType"));
			amount.sendKeys(jr.GetElementValue(elementName + ".FundsAmount"));
			ExtentReportGeneration.test.log(Status.INFO,
					"Adding fund amount " + jr.GetElementValue(elementName + ".FundsAmount"));
			scrollIntoView(fundNow);
			Thread.sleep(3000);
			waitForElementToBeClickable(fundNow);
			fundNow.click();
			waitForPageWithUrl("wallet/deposit/confirm", 6);
			if (elementName.equalsIgnoreCase("Visa")) {
				paymentWithVisa(elementName);
			}
			String AccountType = jr.GetElementValue(elementName + ".AccountType");
			if (AccountType.equalsIgnoreCase(("Neteller")) || AccountType.equalsIgnoreCase(("NetellerFCA"))) {
				paymentWithNetteler(elementName);
			}
			scrollIntoView(fundNow);
			waitForElementToBeClickable(fundNow);
			fundNow.click();
			if (AccountType.equalsIgnoreCase(("Paypal"))) {
				paymentWithpaypal(elementName);
			}
			if (elementName.equalsIgnoreCase("POLi"))
				paymentWithPoli();
			waitForPageWithUrl("wallet/deposit/result", 6);
			returnToDeposit.click();
			accounts.click();
			waitForPageWithUrl("accounts/live", 6);
			float afterBalance = getBalanceOfAccount(jr.GetElementValue(elementName + ".AccountName"));
			ExtentReportGeneration.test.log(Status.INFO, "Total amount after fund added is " + afterBalance);
			Assert.assertEquals(afterBalance, beforeBalance + 10);
			ExtentReportGeneration.test.log(Status.INFO, "Successfully validated adding of funds with " + elementName);
			logger.info("Successfully validated adding of funds with " + elementName);

		} catch (AssertionError | Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
			if (paymentError.isDisplayed()) {
				paymentError.click();
			}
			logout();
		}
	}

	public void pamentMethod(String paymenttype) {

		switch (paymenttype) {

		case "POLi":
			scrollIntoView(neteller);
			poli.click();
			break;
		case "Visa":
			scrollIntoView(neteller);
			visa.click();
			break;
		case "Paypal":
			scrollIntoView(paypal);
			paypal.click();
			break;
		case "Neteller":
			scrollIntoView(neteller);
			neteller.click();
			break;
		case "NetellerFCA":
			scrollIntoView(netellerFCA);
			netellerFCA.click();
			break;
		default:
			poli.click();
		}
		ExtentReportGeneration.test.log(Status.INFO, "payment method selected " + paymenttype);
	}

	public void paymentWithPoli() {
		try {
			waitForPageWithUrl("paywithpoli.com", 8);
			util.validatePartialURL("paywithpoli.com", "Moved to POLi payment section");
			continuebttn.click();
			waitForPageWithUrl("paywithpoli.com/stp/transaction", 6);
			username.sendKeys(jr.GetElementValue("POLi.Username"));
			password.sendKeys(jr.GetElementValue("POLi.Password"));
			ExtentReportGeneration.test.log(Status.INFO, "Sucessfully entered POLi username and Password");
			Thread.sleep(2000);
			waitForElementToBeClickable(login);
			login.click();
			Thread.sleep(7000);
			continueButton.click();
			Thread.sleep(7000);
			confirm.click();
			waitForPageWithUrl("paywithpoli.com/Receipt?", 6);
			returnTopepperstoneUAT.click();
		}

		catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
			logout();

		}
	}
	
	

	public void paymentWithVisa(String elementName) {
		getDropDownByVisibleText(cardOption, jr.GetElementValue(elementName + ".CardOption"));
		cardHolderName.sendKeys(jr.GetElementValue(elementName + ".CardHolderName"));
		cardNumber.sendKeys(jr.GetElementValue(elementName + ".CardNumber"));
		validTo.sendKeys(jr.GetElementValue(elementName + ".Expiry"));
		CVV.sendKeys(jr.GetElementValue(elementName + ".CVV"));
	}

	public void paymentWithNetteler(String elementName) {
		try {
			netellerID.sendKeys(jr.GetElementValue(elementName + ".AccountID"));
			netellersecureId.sendKeys(jr.GetElementValue(elementName + ".SecureID"));
			ExtentReportGeneration.test.log(Status.INFO, "Sucessfully entered Neteller ID and secureID");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to fill Neteller ID and secure ID");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void paymentWithpaypal(String elementName) {
		try {
			waitForPageWithUrl("sandbox.paypal.com", 8);
			paypalEmail.sendKeys(jr.GetElementValue(elementName + ".EmaiId"));
			paypalbtnNext.click();
			waitForPageLoaded();
			paypalpassword.sendKeys(jr.GetElementValue(elementName + ".Password"));
			paypalbtnLogin.click();
			waitForPageLoaded();
			paypalpayNowBttn.click();
			ExtentReportGeneration.test.log(Status.INFO, "Sucessfully made payment with paypal");
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to make payment with paypal");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}

	public void transferfunds(String elementName) {
		try {
			float ExchangeAmount=0;
			float expectedAfterBalanceTo=0;
			float expectedfloat=0;
			float beforeBalancefrom = getBalanceOfAccount(jr.GetElementValue(elementName + ".BalanceAccountFrom"));
			float beforeBalanceto = getBalanceOfAccount(jr.GetElementValue(elementName + ".BalanceAccountTo"));
			ExtentReportGeneration.test.log(Status.INFO, "Transfering From Account having balance "+beforeBalancefrom+" to account having balance "+beforeBalanceto);
			funds.click();
			Thread.sleep(4000);
			transferFunds.click();
			Thread.sleep(3000);
			getDropDownByValue(fromAccount, jr.GetElementValue(elementName + ".FromAccount"));
			fromAmount.sendKeys(jr.GetElementValue(elementName + ".FromAmount"));
			ExtentReportGeneration.test.log(Status.INFO, "Amount transferred is "+jr.GetElementValue(elementName + ".FromAmount"));
			ExtentReportGeneration.test.log(Status.INFO, "Sucessfully entered transfer from and to account");
			getDropDownByValue(toAccount, jr.GetElementValue(elementName + ".ToAccount"));
			if(elementName.contains("DifferentCurrency")){
				Thread.sleep(2000);
				 ExchangeAmount = stringToFloat(currencyExchangeAmount.getText(), "space");
				 ExtentReportGeneration.test.log(Status.INFO, "Amount transferd to new account after currency exchange rate "+ExchangeAmount);
			}
			confirmtransfer();
			Thread.sleep(4000);
			float afterBalancefrom = getBalanceOfAccount(jr.GetElementValue(elementName + ".BalanceAccountFrom"));
			float afterBalanceto = getBalanceOfAccount(jr.GetElementValue(elementName + ".BalanceAccountTo"));
			ExtentReportGeneration.test.log(Status.INFO, "Account balance after Transfer is : From Account "+afterBalancefrom+" and To account "+afterBalanceto);
			Assert.assertEquals(afterBalancefrom, beforeBalancefrom - Float.parseFloat(jr.GetElementValue(elementName + ".FromAmount")));
			if(elementName.contains("DifferentCurrency")){
				expectedAfterBalanceTo = beforeBalanceto + ExchangeAmount;
			expectedfloat = twoDecimalFloatNumber(expectedAfterBalanceTo);
				Assert.assertEquals(afterBalanceto, expectedfloat);
			}
			else
			Assert.assertEquals(afterBalanceto, beforeBalanceto + Float.parseFloat(jr.GetElementValue(elementName + ".FromAmount")));
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to select accounts to transfer funds");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	
	public void logout() {
		try {
			do {
				Thread.sleep(2000);
				myProfileMenuItem.click();
				Thread.sleep(2000);
				isDisplayed(logout, "logout button displayed");
				logout.click();
				Thread.sleep(3000);
			} while (driver.getCurrentUrl().contains("verification"));
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Logout failed");
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("Logout failed");
			logger.fail("exception is " + e);
		}
	}

	public void changepassword(String elementName ){
		String passwordString = null;
		try{
			if (elementName.equalsIgnoreCase("demo"))
				demo.click();
		editBttn.get(0).click();
		if (elementName.equalsIgnoreCase("demo")){
		 passwordString = getRandomAlphaNumericString(8);
		 waitForPageLoaded();
		newpassword.sendKeys(passwordString);
		confirmPassword.sendKeys(passwordString);
		submit.click();
		Assert.assertEquals(passwordChangeSucessfullMsg.size(), 1);
		close.click();
		Thread.sleep(1000);
		}
		else{
		resetPasswordBttn.click();
		Assert.assertEquals(emailSentMessage.size(), 1);
		ExtentReportGeneration.test.info("Sucessfully found message 'AN EMAIL HAS BEEN SENT.'");
		}
		close.click();
	} catch (Exception e) {
		ExtentReportGeneration.test.fail("Failed to validate change password link for Live account");
		logger.fail("exception : " + e.getMessage());
		logout();
	}
	}
	public void verifyEditNickName(String elementName) {
		try {
			if (elementName.equalsIgnoreCase("demo"))
				demo.click();
			String beforeEditingNickName=liveAccount.get(0).getText();
			editBttn.get(0).click();
			changeNickNameLink.click();
			String editNickName="Test"+getRandomAlphaNumericString(5);
			System.out.println("editNickName"+editNickName);
				
			JavascriptExecutor js=((JavascriptExecutor)driver);
			js.executeScript("arguments[0].value ='';", changeNickNameInput);
				
			changeNickNameInput.sendKeys(editNickName);
			submit.click();
			Thread.sleep(5000);
			Assert.assertFalse(editNickName.equals(beforeEditingNickName), "Failed:-Actual Edit Name is same with expected Edit Name.");
			ExtentReportGeneration.test.log(Status.PASS, "Sucessfully edited Account Nick Name.");
			
		}catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to edit Account Nick Name.");
			logout();
			//verificationpage.logout();
			logger.fail("exception : " + e.getMessage());
			
		}
	
	}
	
	public void addfundsDemo(){
		try{
		demo.click();
		float beforeBalance = getBalanceOfAccount("Demo #1");
		ExtentReportGeneration.test.info("Existing funds in Account is "+beforeBalance);
		getDynamicFundNow("Demo #1").click();
		getDropDownByVisibleText(selectAmount, "200");
		ExtentReportGeneration.test.info("Funds added is 200");
		demoFundNow.click();
		Thread.sleep(2000);
		float afterBalance = getBalanceOfAccount("Demo #1");
		Assert.assertEquals(afterBalance, beforeBalance+200);
		Assert.assertEquals(afterBalance, beforeBalance+200, 0, "Account balance is sucessfully updated from "+ beforeBalance +" to "+ afterBalance);
		ExtentReportGeneration.test.info("Account balance is sucessfully updated from "+ beforeBalance +" to "+ afterBalance);
		close.click();
		Thread.sleep(3000);
		}
		catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to add funds in Demo account");
			verificationpage.logout();
			logger.fail("exception : " + e.getMessage());
		}
	}
	
	public void confirmtransfer() {
		try {
			scrollIntoView(submit);
			Thread.sleep(3000);
			submit.click();
		} catch (Exception e) {
			ExtentReportGeneration.test.fail("Failed to click on confirm transfer");
			logger.fail("exception : " + e.getMessage());
			logout();
		}
	}
	public void noNetellerAndSkrill() {
		try {
			fundNow.click();
			Assert.assertFalse(isElementPresent(neteller), "neteller is present for FCA account");
			Assert.assertFalse(isElementPresent(skrill), "skrill is present for FCA account");
		}
		catch (Exception e) {
			ExtentReportGeneration.test.fail(e.getMessage());
			logger.fail("exception : " + e.getMessage());
			logout();

		}
	}

	public float getBalanceOfAccount(String account) {
		String balance = getDynamicbalance(account).getText();
		float balanceAmount = stringToFloat(balance, "line");
		return balanceAmount;
	}
	
	public float stringToFloat(String string, String splitby ) {
		String[] elements;
		if (splitby.contains("line")){
		 elements = string.split("\\n");
		}
		else{
		 elements = string.split(" ");	
		}
		String stringFloat = elements[0];
		String numberOnly= stringFloat.replaceAll("[,]", "");
		float balanceAmount = Float.parseFloat(numberOnly);
		return balanceAmount;
	}

		public float twoDecimalFloatNumber(float input){
		DecimalFormat df = new DecimalFormat("0.00");
		float f = Float.parseFloat(df.format(input));
		return f;
	}
	public boolean isElementPresent(WebElement locatorKey) {
	    try {
	        locatorKey.isDisplayed();
	        return true;
	    } catch (org.openqa.selenium.NoSuchElementException e) {
	        return false;
	    }
	}
}