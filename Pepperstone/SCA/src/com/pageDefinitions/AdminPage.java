package com.pageDefinitions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tests.VerificationPageTest;
import com.utils.ExtentReportGeneration;
import com.utils.JsonReader;
import com.utils.TestLogger;
import com.utils.UtilityLibrary;

public class AdminPage extends UtilityLibrary {
	public TestLogger logger = new TestLogger();
	UtilityLibrary util = new UtilityLibrary();
	JsonReader jr = new JsonReader("jsonFilePathAdminPage");
	
	
	@FindBy(how = How.ID, using = "username")
	WebElement id;
	
	@FindBy(how = How.ID, using = "password")
	WebElement password;
	
	@FindBy(how = How.CSS, using = "[type='submit']")
	WebElement submit;

	@FindBy(how = How.CSS, using = "[href='#menu-verify']")
	WebElement verify;
	
	@FindBy(how = How.CSS, using = "[href='/account/check-id-list']")
	WebElement checkID;
	
	@FindBy(how = How.CSS, using = "#new-accounts_filter input")
	WebElement search;
	
	@FindBy(how = How.XPATH, using = "//td/a/button")
	WebElement show;
	
	@FindBy(how = How.XPATH, using = "//th[contains(text(),'User Id')]/../td")
	WebElement userID;
	
	//th[contains(text(),'User Id')]/../td
	
	
	public void loginToadmin(String elementName) {
		
		try {
			driver.navigate().to("https://admin-beta.pepperstone.com/dev_mode.php/login");
			id.sendKeys(jr.GetElementValue(elementName + ".Username"));
			password.sendKeys(jr.GetElementValue(elementName + ".Password"));
			submit.click();
			
			}
		 catch (Exception e) {
			logger.fail("exception : " + e.getMessage());
		}
	}
}
